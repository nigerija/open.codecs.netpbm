﻿using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace open.codecs.netpbm
{


  /// <summary>
  /// <para>Encapsulates Portable Any Type file image containers reader and writer.</para>
  /// <para>Supported extension: .PAM (Portable Arbitrary Bitmap), .PNM (Portable Any Map), .PBM (Portable Bit Map), .PFM (Portable Float Map), .PGM (Portable Grey Map), .PPM (Portable Pix Map).</para>
  /// </summary>
  public class PmFile : System.IDisposable
  {
    // NOTE: NETPBM image container standard
    // DOCS: https://en.wikipedia.org/wiki/Netpbm
    // NOTE: In NETPBM, the de facto standard implementation of the PNM formats, the most significant byte is first.[10]

    /*
        9. A raster of Height rows, in order from top to bottom. Each row consists of Width gray values,
           in order from left to right. Each gray value is a number from 0 through MAXVAL, with 0 being 
           black and MAXVAL being white. Each gray value is represented in pure binary by either 1 or 2 
           bytes. If the MAXVAL is less than 256, it is 1 byte. Otherwise, it is 2 bytes. 
           The most significant byte is first.
           A row of an image is horizontal. A column is vertical. 
           The pixels in the image are square and contiguous.

        10. Each gray value is a number proportional to the intensity of the pixel, adjusted by the ITU-R 
            Recommendation BT.709 gamma transfer function. (That transfer function specifies a gamma number
            of 2.2 and has a linear section for small intensities). A value of zero is therefore black. 
            A value of MAXVAL represents CIE D65 white and the most intense value in the image and any 
            other image to which the image might be compared.

        11. Note that a common variation on the PGM format is to have the gray value be "linear," i.e. as 
            specified above except without the gamma adjustment. PNM gamma takes such a PGM variant as input
            and produces a true PGM as output.

        12. In the transparency mask variation on PGM, the value represents opaqueness. It is proportional 
            to the fraction of intensity of a pixel that would show in place of an underlying pixel. 
            So what normally means white represents total opaqueness and what normally means black 
            represents total transparency. In between, you would compute the intensity of a composite pixel
            of an "under" and "over" pixel as under * (1-(alpha/alpha_maxval)) + over * (alpha/alpha_maxval).
            Note that there is no gamma transfer function in the transparency mask.

        13. Characters from a "#" to the next end-of-line, before the MAXVAL line, are comments and are ignored.
    */

    /// <summary>
    /// Provides information about this codec.
    /// </summary>
    public static readonly BitmapCodecInfo CodecInfo = new PmCodecInfo();

    private PmReader _reader;
    private PmWriter _writer;
    private PmImage _image;

    /// <summary>
    /// Gets or set <see cref="PmImage"/> assigned to the this instance of <see cref="PmFile"/>.
    /// </summary>
    public PmImage Image
    {
      get { return _image; }
      set { _image = value; }
    }

    #region ### read ###

    /// <summary>
    /// Reads image pixel data from specified <paramref name="filename"/>.
    /// </summary>
    /// <param name="filename">A filename from which to read image pixel data.</param>
    /// <param name="options">A optional reader options to be used while reading.</param>
    /// <returns>Returns the status of read operation, for more details see <see cref="EPmReadStatus"/>.</returns>
    public EPmReadStatus Read(string filename, PmReaderOptions options = null)
    {
      if (!File.Exists(filename))
      {
        return EPmReadStatus.ERROR_FILE_NOT_FOUND;
      }
      using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
      {
        return Read(fs, options);
      }
    }

    /// <summary>
    /// Reads image pixel data from specified <paramref name="stream"/>.
    /// </summary>
    /// <param name="stream">A stream from which to read image pixel data.</param>
    /// <param name="options">A optional reader options to be used while reading.</param>
    /// <returns>Returns the status of read operation, for more details see <see cref="EPmReadStatus"/>.</returns>
    public EPmReadStatus Read(Stream stream, PmReaderOptions options = null)
    {
      EPmReadStatus status = EPmReadStatus.ERROR_BASE;
      if (null == options)
      {
        options = new PmReaderOptions();
      }
      _image = new PmImage();
      try
      {
        // NOTE: using memory stream is eleven times faster, but puts memory pressure
        // read ~ 0.390 : when using direct FileStream, no memory pressure
        // read ~ 0.033 : when using MemoryStream, but produces memory pressure
        byte[] buffer = new byte[stream.Length];
        stream.Read(buffer, 0, buffer.Length);
        if (!options.LeaveStreamOpen)
        {
          stream.Close();
        }
        using (_reader = new PmReader(new MemoryStream(buffer), false, false))
        {
          // add delegate to add comments to the read image
          _reader.OnComment += delegate (string comment) { _image.Comments.Add(comment); };
          // validate needed size to read file type
          if (_reader.Length < 2)
          {
            return status = EPmReadStatus.ERROR_EOF;
          }
          // read header magic file type
          if (EPmReadStatus.OK != (status = _reader.ReadFileType(out _image.FileType)))
          {
            return status;
          }
          // read header data, headers based on file format
          // NOTE: P7 (PAM) has different header rules from other file types
          switch (_image.FileType)
          {
            default:
              return status = EPmReadStatus.ERROR_FILE_TYPE;
            case EPmFileType.P1_BITMAP_ASCII:
            case EPmFileType.P2_GRAYMAP_ASCII:
            case EPmFileType.P3_PIXMAP_ASCII:
            case EPmFileType.P4_BITMAP_BINARY:
            case EPmFileType.P5_GRAYMAP_BINARY:
            case EPmFileType.P6_PIXMAP_BINARY:
            case EPmFileType.PF_FLOATMAP_GRAY_BINARY:
            case EPmFileType.PF_FLOATMAP_PIX_BINARY:
              if (EPmReadStatus.OK != (status = ReadHeaderPNM()))
              {
                return status;
              }
              break;
            case EPmFileType.P7_ARBITRARY_MAP_BINARY:
              if (EPmReadStatus.OK != (status = ReadHeaderPAM()))
              {
                return status;
              }
              break;
          }
          // if read header only, return read status
          if (options.ReadHeaderOnly)
          {
            return status;
          }
          // set reader endian
          _reader.ReadLittleEndian = _image.ImageDataLittleEndian;
          // read pixel data as ASCII or BINARY type
          if (_image.IsFileTypeAscii)
          {
            return status = ReadPixelDataAsAscii();
          }
          else
          {
            return status = ReadPixelDataAsBinary();
          }
        }
      }
      finally
      {
        _reader = null;
        if (status != EPmReadStatus.OK && !options.OnErrorLeaveImageData)
        {
          _image = null;
        }
      }
    }

    #endregion

    #region ### read header data ###

    /// <summary>
    /// Reads header as old format (.PBM, .PFM, .PGM, .PNM, .PPM), and sets position of the read stream to the first pixel data.
    /// </summary>
    /// <returns>Returns the status of read operation, for more details see <see cref="EPmReadStatus"/>.</returns>
    private EPmReadStatus ReadHeaderPNM()
    {
      // dumb ass IDE==M$, there are more than one use of value!
#pragma warning disable IDE0018 // Inline variable declaration
      string value;

      // skip separator, and read implicit (by position) image width
      if (!_reader.ReadAsciiWord(out value))
      {
        return EPmReadStatus.ERROR_READING_IMAGE_WIDTH;
      }
      if (!int.TryParse(value, out _image.ImageWidth))
      {
        return EPmReadStatus.ERROR_PARSING_IMAGE_WIDTH;
      }
      if (_image.ImageWidth <= 0)
      {
        return EPmReadStatus.ERROR_INVALID_IMAGE_WIDTH;
      }
      // skip separator, and read implicit (by position) image height
      if (!_reader.ReadAsciiWord(out value) || null == value)
      {
        return EPmReadStatus.ERROR_READING_IMAGE_HEIGHT;
      }
      if (!int.TryParse(value, out _image.ImageHeight))
      {
        return EPmReadStatus.ERROR_PARSING_IMAGE_HEIGHT;
      }
      if (_image.ImageHeight <= 0)
      {
        return EPmReadStatus.ERROR_INVALID_IMAGE_HEIGHT;
      }
      // parse implicit (by position) MAXVAL for specific file format
      switch (_image.FileType)
      {
        case EPmFileType.P1_BITMAP_ASCII:
        case EPmFileType.P4_BITMAP_BINARY:
          {
            // 1 bit, implicit 1
            _image.ImageMaxVal = 1;
            // TODO: we should add error fall-back in case of erroneous implementations (added MAXVAL).
          }
          break;
        case EPmFileType.P2_GRAYMAP_ASCII:
        case EPmFileType.P5_GRAYMAP_BINARY:
        case EPmFileType.P3_PIXMAP_ASCII:
        case EPmFileType.P6_PIXMAP_BINARY:
          // skip ASCII separators and read image MAXVAL 
          if (!_reader.ReadAsciiWord(out value))
          {
            return EPmReadStatus.ERROR_READING_IMAGE_MAXVAL;
          }
          if (!ulong.TryParse(value, out _image.ImageMaxVal))
          {
            return EPmReadStatus.ERROR_PARSING_IMAGE_MAXVAL;
          }
          if (_image.ImageMaxVal < 0)
          {
            return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
          }
          // skip ASCII separators
          if (!_reader.SkipAsciiSeparators())
          {
            return EPmReadStatus.ERROR_EOF;
          }
          break;
        case EPmFileType.PF_FLOATMAP_GRAY_BINARY:
        case EPmFileType.PF_FLOATMAP_PIX_BINARY:
          // read image MAXVAL (FLOAT)
          if (!_reader.ReadAsciiWord(out value))
          {
            return EPmReadStatus.ERROR_READING_IMAGE_WIDTH;
          }
          if (!float.TryParse(value, out _image.ImageMaxValFloat))
          {
            return EPmReadStatus.ERROR_PARSING_IMAGE_MAXVAL;
          }
          // NOTE: do not set, as it may produce invalid execution paths that may not break
          //       it should break on invalid execution path.
          //_image.ImageMaxVal = (ulong)_image.ImageMaxValFloat;

          // NOTE: if (value == -1.0) pixel values are defines as [0..1] floats in little endian format
          //       if (value == 1.0) pixel value are defined as [0..1] floats in big endian format
          //       So positive number is Big Endian
          _image.ImageDataLittleEndian = (_image.ImageMaxValFloat < 0);
          // skip white space
          if (!_reader.SkipAsciiSeparators())
          {
            return EPmReadStatus.ERROR_EOF;
          }
          break;
      }
      // we are at the beginning of data or comment line
      return EPmReadStatus.OK;
    }

    /// <summary>
    /// Maximum allowed UINT value for PAM files MaxVal header value.
    /// </summary>
    private const ulong PAM_TUPLTYPE_MAXVAL_MAXUINT = 0x000000000000FFFFul;

    /// <summary>
    /// Reads header as new format (.PAM), and sets position of the read stream to the first pixel data.
    /// </summary>
    /// <returns>Returns the status of read operation, for more details see <see cref="EPmReadStatus"/>.</returns>
    private EPmReadStatus ReadHeaderPAM()
    {
      string word;
      string value;
      bool fEndHeader = false;
      bool fFloatType = false;
      // skip whitespace and read word, or more precisely while not EOF.
      while (_reader.ReadAsciiWord(out word))
      {
        #region ### read header entries ###
        switch (word)
        {
          // entries with value (Separated with separation character(s))
          case "WIDTH":
          case "HEIGHT":
          case "DEPTH":
          case "MAXVAL":
          case "TUPLTYPE":
            // skips whitespace and read value
            if (!_reader.ReadAsciiWord(out value))
            {
              return EPmReadStatus.ERROR_EOF;
            }
            switch (word)
            {
              case "WIDTH":
                if (!int.TryParse(word, out _image.ImageWidth))
                  return EPmReadStatus.ERROR_PARSING_IMAGE_WIDTH;
                break;
              case "HEIGHT":
                if (!int.TryParse(word, out _image.ImageHeight))
                  return EPmReadStatus.ERROR_PARSING_IMAGE_HEIGHT;
                break;
              case "DEPTH":
                if (!int.TryParse(word, out _image.ImageDepth))
                  return EPmReadStatus.ERROR_PARSING_IMAGE_DEPTH;
                break;
              case "MAXVAL":
                // it may be negative:little-endian or positive:big-endian for FLOAT (and what about UINT?)
                // ulong.TryParse() shall fail for negative UINT's or any FLOAT's
                // but float.TryParse() should parse it if valid FLOAT (positive or negative).
                // NOTE: pixel endian shall be decided later on from pixel format and MaxVal.
                if (!ulong.TryParse(word, out _image.ImageMaxVal))
                {
                  if (!float.TryParse(word, out _image.ImageMaxValFloat))
                  {
                    return EPmReadStatus.ERROR_PARSING_IMAGE_MAXVAL;
                  }
                }
                break;
              case "TUPLTYPE":
                switch (value)
                {
                  case "BLACKANDWHITE":
                    _image.ImageTuplType = EPamTuplType.BLACKANDWHITE;
                    break;
                  case "GRAYSCALE":
                    _image.ImageTuplType = EPamTuplType.GRAYSCALE;
                    break;
                  case "RGB":
                    _image.ImageTuplType = EPamTuplType.RGB;
                    break;
                  case "BLACKANDWHITE_ALPHA":
                    _image.ImageTuplType = EPamTuplType.BLACKANDWHITE_ALPHA;
                    break;
                  case "GRAYSCALE_ALPHA":
                    _image.ImageTuplType = EPamTuplType.GRAYSCALE_ALPHA;
                    break;
                  case "RGB_ALPHA":
                    _image.ImageTuplType = EPamTuplType.RGB_ALPHA;
                    break;
                  // TuplType float extensions
                  case "FLOAT_GRAYSCALE":
                    _image.ImageTuplType = EPamTuplType.FLOAT_GRAYSCALE;
                    break;
                  case "FLOAT_RGB":
                    _image.ImageTuplType = EPamTuplType.FLOAT_RGB;
                    break;
                  case "FLOAT_GRAYSCALE_ALPHA":
                    _image.ImageTuplType = EPamTuplType.FLOAT_GRAYSCALE_ALPHA;
                    break;
                  case "FLOAT_RGB_ALPHA":
                    _image.ImageTuplType = EPamTuplType.FLOAT_RGB_ALPHA;
                    break;
                  default:
                    return EPmReadStatus.ERROR_INVALID_IMAGE_TUPLTYPE;
                }
                break;
              default:
                // UPS, unknown entry (unknown/undocumented extension?)
                break;
            }
            break;
          // entries without value
          case "ENDHDR":
            fEndHeader = true;
            break;
        }
        #endregion

        // if end of header not encountered continue reading header entries
        if (!fEndHeader)
        {
          continue;
        }
        // make sure we are at the beginning of pixel data.
        if (!_reader.SkipAsciiSeparators())
        {
          return EPmReadStatus.ERROR_EOF;
        }
        // mark float types and test for endian
        switch (_image.ImageTuplType)
        {
          case EPamTuplType.FLOAT_GRAYSCALE:
          case EPamTuplType.FLOAT_GRAYSCALE_ALPHA:
          case EPamTuplType.FLOAT_RGB:
          case EPamTuplType.FLOAT_RGB_ALPHA:
            fFloatType = true;
            break;
        }
        // Test for pixel data endian for formats having multiple bytes for pixel
        if (fFloatType)
        {
          // if binary format for pixel data:
          // MAXVAL containing -1.0 indicates little-endian format in range ZERO (0) to ONE (1), 
          // otherwise we extended standard to support ZERO (0) to -MAXVAL range .
          // Otherwise, float numbers are in big endian format from ZERO (0) to MAXVAL.
          _image.ImageDataLittleEndian = _image.ImageMaxValFloat < 0f;
          _image.ImageMaxValFloat = System.Math.Abs(_image.ImageMaxValFloat);

          // NOTE: do not set, as it may produce invalid execution paths that may not break
          //       it should break on invalid execution path.
          //_image.ImageMaxVal = (ulong)_image.ImageMaxValFloat;
        }
        else
        {
          // NetPbm de-facto standard says all UINT's are unsigned int's in little endian
          _image.ImageDataLittleEndian = true;
          // NOTE: do not set, as it may produce invalid execution paths that may not break
          //       it should break on invalid execution path.
          //_image.ImageMaxValFloat = (float)_image.ImageMaxVal;
        }
        // validate image pixel format information
        switch (_image.ImageTuplType)
        {
          case EPamTuplType.BLACKANDWHITE:
            if (!(_image.ImageMaxVal == 1))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 1))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.BLACKANDWHITE_ALPHA:
            if (!(_image.ImageMaxVal == 1))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 2))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.GRAYSCALE:
            if (!(_image.ImageMaxVal >= 2 && _image.ImageMaxVal <= PAM_TUPLTYPE_MAXVAL_MAXUINT))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 1))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.GRAYSCALE_ALPHA:
            if (!(_image.ImageMaxVal >= 2 && _image.ImageMaxVal <= PAM_TUPLTYPE_MAXVAL_MAXUINT))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 2))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.RGB:
            if (!(_image.ImageMaxVal >= 2 && _image.ImageMaxVal <= PAM_TUPLTYPE_MAXVAL_MAXUINT))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 3))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.RGB_ALPHA:
            if (!(_image.ImageMaxVal >= 2 && _image.ImageMaxVal <= PAM_TUPLTYPE_MAXVAL_MAXUINT))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 4))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.FLOAT_GRAYSCALE:
            fFloatType = true;
            if (!(_image.ImageMaxValFloat > 0 && _image.ImageMaxVal <= float.MaxValue))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 1))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.FLOAT_GRAYSCALE_ALPHA:
            fFloatType = true;
            if (!(_image.ImageMaxValFloat > 0 && _image.ImageMaxVal <= float.MaxValue))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 2))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.FLOAT_RGB:
            fFloatType = true;
            if (!(_image.ImageMaxValFloat > 0 && _image.ImageMaxVal <= float.MaxValue))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 3))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          case EPamTuplType.FLOAT_RGB_ALPHA:
            fFloatType = true;
            if (!(_image.ImageMaxValFloat > 0 && _image.ImageMaxVal <= float.MaxValue))
              return EPmReadStatus.ERROR_INVALID_IMAGE_MAXVAL;
            if (!(_image.ImageDepth == 4))
              return EPmReadStatus.ERROR_INVALID_IMAGE_DEPTH;
            break;
          default:
            return EPmReadStatus.ERROR_INVALID_IMAGE_TUPLTYPE;
        }
        // and if validation passes return OK
        return EPmReadStatus.OK;
      }// while (word)
      // otherwise nothing to read, it's EOF
      return EPmReadStatus.ERROR_EOF;
    }

    #endregion

    #region ### read image pixel data ###

    /// <summary>
    /// Reads image data as ASCII text pixel values
    /// </summary>
    /// <returns>Returns the status of read operation, for more details see <see cref="EPmReadStatus"/>.</returns>
    private EPmReadStatus ReadPixelDataAsAscii()
    {
      _image.BitsPerPixel = _image.GetPixelBitsSize();
      int elementCount = _image.GetElementsCount();

      switch (_image.PixelFormat)
      {
        case EPmPixelFormat.W_1bit_1u1:
          // are bits
          if (!_reader.ReadAsciiBitsArray(elementCount, _image))
          {
            return EPmReadStatus.ERROR_EOF;
          }
          return 0;
        case EPmPixelFormat.RGBA_32bits_4u8:
        case EPmPixelFormat.RGB_24bits_3u8:
        case EPmPixelFormat.WA_16bits_2u8:
        case EPmPixelFormat.YA_16bits_2u8:
        case EPmPixelFormat.Y_8bits_1u8:
          // array of uint8
          if (!_reader.ReadAsciiByteArray(elementCount, out _image.ImagePixelData))
          {
            return EPmReadStatus.ERROR_EOF;
          }
          return 0;
        case EPmPixelFormat.RGBA_64bits_4u16:
        case EPmPixelFormat.RGB_48bits_3u16:
        case EPmPixelFormat.YA_32bits_2u16:
        case EPmPixelFormat.Y_16bits_1u16:
          // array of uint16          
          if (!_reader.ReadAsciiUInt16Array(elementCount, out _image.ImagePixelData))
          {
            return EPmReadStatus.ERROR_EOF;
          }
          return 0;
        case EPmPixelFormat.RGB_96bits_3u32:
        case EPmPixelFormat.Y_32bits_1u32:
          // array of uint16
          if (!_reader.ReadAsciiUInt32Array(elementCount, out _image.ImagePixelData))
          {
            return EPmReadStatus.ERROR_EOF;
          }
          return 0;
        case EPmPixelFormat.Y_48bits_1u48:
          // array of uint48
          if (!_reader.ReadAsciiUInt48Array(elementCount, out _image.ImagePixelData))
          {
            return EPmReadStatus.ERROR_EOF;
          }
          return 0;
        case EPmPixelFormat.RGB_192bits_3u64:
        case EPmPixelFormat.Y_64bits_1u64:
          // array of uint64
          if (!_reader.ReadAsciiUInt64Array(elementCount, out _image.ImagePixelData))
          {
            return EPmReadStatus.ERROR_EOF;
          }
          return 0;
        case EPmPixelFormat.RGBA_128bits_4f32:
        case EPmPixelFormat.RGB_96bits_3f32:
          if (!_reader.ReadAsciiFloat32Array(elementCount, out _image.ImagePixelData))
          {
            return EPmReadStatus.ERROR_EOF;
          }
          return 0;
        default:
          // array of float32, or otherwise : not supported
          break;
      }

      return EPmReadStatus.ERROR_NOT_SUPPORTED;
    }

    /// <summary>
    /// Reads image data as binary pixel values
    /// </summary>
    /// <returns>Returns the status of read operation, for more details see <see cref="EPmReadStatus"/>.</returns>
    private EPmReadStatus ReadPixelDataAsBinary()
    {
      unchecked
      {
        // validate read image information
        _image.BitsPerPixel = _image.GetPixelBitsSize();
        if (_image.BitsPerPixel == -1)
        {
          return EPmReadStatus.ERROR_INVALID_BINARY_PIXEL_SIZE;
        }
        // instantiate and read image data
        int bufferSizeInBytes = _image.GetPixelDataSize();
        int elementSizeInBytes = (_image.BitsPerPixel + 7) / 8;
        _image.ImagePixelData = _reader.ReadBytes(bufferSizeInBytes, elementSizeInBytes);
      }
      return EPmReadStatus.OK;
    }

    #endregion

    #region ### write ###

    /// <summary>
    /// Writes image data to he specified <paramref name="filename"/>.
    /// </summary>
    /// <param name="filename">A filename to be written with image data.</param>
    /// <param name="options">An optional write options.</param>
    /// <returns>Returns the status of write operation.</returns>
    public EPmWriteStatus Write(string filename, PmWriterOptions options = null)
    {
      if (null == options)
      {
        options = new PmWriterOptions()
        {
          // NOTE: for ASCII FILE default line break is at image width
          AsciiOptions = new PmWriterAsciiOptions()
          {
            WidthBreak = _image.ImageWidth
          }
        };
      }
      if (File.Exists(filename))
      {
        if (!options.OverwriteExistingFile)
        {
          return EPmWriteStatus.ERROR_FILE_ALREADY_EXSTS;
        }
        File.Delete(filename);
      }
      using (FileStream stream = new FileStream(filename, FileMode.CreateNew, FileAccess.Write, FileShare.ReadWrite))
      {
        return Write(stream, options);
      }
    }

    /// <summary>
    /// Writes image data to he specified <paramref name="stream"/>.
    /// </summary>
    /// <param name="stream">A stream to be written with image data.</param>
    /// <param name="options">An optional write options.</param>
    /// <returns>Returns the status of write operation.</returns>
    public EPmWriteStatus Write(Stream stream, PmWriterOptions options = null)
    {
      EPmWriteStatus status = EPmWriteStatus.OK;
      if (null == options)
      {
        options = new PmWriterOptions()
        {
          // NOTE: for ASCII FILE default line break is at image width
          AsciiOptions = new PmWriterAsciiOptions()
          {
            WidthBreak = _image.ImageWidth
          }
        };
      }
      try
      {
        // NOTE: using memory stream is ten times faster, but puts memory pressure
        using (_writer = new PmWriter(new MemoryStream(), false, true))
        {
          // write header magic file type        
          if (EPmWriteStatus.OK != (status = _writer.WriteFileType(_image.FileType)))
          {
            return status;
          }
          // write comments
          if (_image.Comments.Count > 0)
          {
            foreach (string comment in _image.Comments)
            {
              _writer.WriteComment(comment);
            }
          }
          // write header data
          if (_image.IsFileTypeP7ArbitratyMap)
          {
            if (EPmWriteStatus.OK != (status = WriteHeaderPAM(options)))
            {
              return status;
            }
          }
          else
          {
            if (EPmWriteStatus.OK != (status = WriteHeaderPNM(options)))
            {
              return status;
            }
          }
          // set writer endian
          _writer.WriteLittleEndian = _image.ImageDataLittleEndian;
          // write pixel data 
          if (_image.IsFileTypeAscii)
          {
            // ASCII format
            return (status = WritePixelDataAsAscii(options));
          }
          else
          {
            // BINARY format
            return (status = WritePixelDataAsBinary());
          }
        }
      }
      finally
      {
        // _writter.BaseStream == MemoryStream
        if (status == EPmWriteStatus.OK)
        {
          long count = _writer.Position;
          byte[] buffer = new byte[_writer.Position];
          _writer.Position = 0;
          _writer.BaseStream.Read(buffer, 0, buffer.Length);
          stream.Write(buffer, 0, buffer.Length);
        }
        // error or not
        if (null != _writer)
        {
          _writer.Close();
        }
        _writer = null;
        if (!options.LeaveStreamOpen)
        {
          stream.Close();
        }
      }
    }

    #endregion

    #region ### write header data ###

    /// <summary>
    /// Writes header as old format (PBM, PFM, PGM, PNM, PPM)
    /// </summary>
    /// <returns></returns>
    private EPmWriteStatus WriteHeaderPNM(PmWriterOptions options)
    {
      // '<WIDTH><space><HEIGHT><lineend>'
      _writer.WriteText(_image.ImageWidth.ToString());
      _writer.WriteSpace();
      _writer.WriteText(_image.ImageHeight.ToString());
      _writer.WriteLineEnd();
      // '<MAXVAL><lineend>'
      if (_image.IsPixelFormatFloat)
      {
        // NOTE: original PFM contains '-1.000000', while our code writes '-1'
        string sMaxVal = _image.ImageMaxValFloat.ToString(options.FloatMaxValueFormat, System.Globalization.CultureInfo.InvariantCulture);
        _writer.WriteText(sMaxVal);
        _writer.WriteLineEnd();
      }
      else
      {
        switch(_image.FileType)
        {
          case EPmFileType.P1_BITMAP_ASCII:
          case EPmFileType.P4_BITMAP_BINARY:
            // must not write MAXVAL
            break;
          default:
            _writer.WriteText(_image.ImageMaxVal.ToString());
            _writer.WriteLineEnd();
            break;
        }
      }
      return EPmWriteStatus.OK;
    }

    /// <summary>
    /// Writes header as new format (PAM)
    /// </summary>
    /// <returns>Returns the status of write operation.</returns>
    private EPmWriteStatus WriteHeaderPAM(PmWriterOptions options)
    {
      _writer.WriteParamLine("WIDTH", _image.ImageWidth.ToString());
      _writer.WriteParamLine("HEIGHT", _image.ImageHeight.ToString());

      int nComponents = _image.GetPixelComponents();
      _writer.WriteParamLine("DEPTH", nComponents.ToString());

      if (_image.IsPixelFormatFloat)
      {
        // if image float format is little endian negative MAXVAL is written
        if (_image.ImageDataLittleEndian)
        {
          _writer.WriteParamLine("MAXVAL", (-_image.ImageMaxValFloat).ToString(options.FloatMaxValueFormat, System.Globalization.CultureInfo.InvariantCulture));
        }
        else
        {
          _writer.WriteParamLine("MAXVAL", _image.ImageMaxValFloat.ToString(options.FloatMaxValueFormat, System.Globalization.CultureInfo.InvariantCulture));
        }
      }
      else
      {
        _writer.WriteParamLine("MAXVAL", _image.ImageMaxVal.ToString());
      }
      switch (_image.ImageTuplType)
      {
        case EPamTuplType.BLACKANDWHITE:
          _writer.WriteParamLine("TUPLTYPE", "BLACKANDWHITE");
          break;
        case EPamTuplType.BLACKANDWHITE_ALPHA:
          _writer.WriteParamLine("TUPLTYPE", "BLACKANDWHITE_ALPHA");
          break;
        case EPamTuplType.GRAYSCALE:
          _writer.WriteParamLine("TUPLTYPE", "GRAYSCALE");
          break;
        case EPamTuplType.GRAYSCALE_ALPHA:
          _writer.WriteParamLine("TUPLTYPE", "GRAYSCALE_ALPHA");
          break;
        case EPamTuplType.RGB:
          _writer.WriteParamLine("TUPLTYPE", "RGB");
          break;
        case EPamTuplType.RGB_ALPHA:
          _writer.WriteParamLine("TUPLTYPE", "RGB_ALPHA");
          break;
        case EPamTuplType.FLOAT_GRAYSCALE:
          _writer.WriteParamLine("TUPLTYPE", "FLOAT_GRAYSCALE");
          break;
        case EPamTuplType.FLOAT_GRAYSCALE_ALPHA:
          _writer.WriteParamLine("TUPLTYPE", "FLOAT_GRAYSCALE_ALPHA");
          break;
        case EPamTuplType.FLOAT_RGB:
          _writer.WriteParamLine("TUPLTYPE", "FLOAT_RGB");
          break;
        case EPamTuplType.FLOAT_RGB_ALPHA:
          _writer.WriteParamLine("TUPLTYPE", "FLOAT_RGB_ALPHA");
          break;
        default:
          return EPmWriteStatus.ERROR_INVALID_IMAGE_TUPLTYPE;
      }
      _writer.WriteTextLine("ENDHDR");
      return EPmWriteStatus.OK;
    }

    #endregion

    #region ### write image pixel data ###

    /// <summary>
    /// Writes image data as ASCII text pixel values
    /// </summary>
    /// <returns>Returns the status of write operation.</returns>
    private EPmWriteStatus WritePixelDataAsAscii(PmWriterOptions options)
    {
      EPmWriteStatus ok = EPmWriteStatus.OK;
      _image.BitsPerPixel = _image.GetPixelBitsSize();
      int elementCount = _image.GetElementsCount();

      switch (_image.PixelFormat)
      {
        case EPmPixelFormat.W_1bit_1u1:
          // are bits
          _writer.WriteAsciiBitsArray(_image, options.AsciiOptions);
          return ok;
        case EPmPixelFormat.RGBA_32bits_4u8:
        case EPmPixelFormat.RGB_24bits_3u8:
        case EPmPixelFormat.WA_16bits_2u8:
        case EPmPixelFormat.YA_16bits_2u8:
        case EPmPixelFormat.Y_8bits_1u8:
          // array of uint8
          _writer.WriteAsciiByteArray(_image.ImagePixelData, elementCount, options.AsciiOptions);
          return ok;
        case EPmPixelFormat.RGBA_64bits_4u16:
        case EPmPixelFormat.RGB_48bits_3u16:
        case EPmPixelFormat.YA_32bits_2u16:
        case EPmPixelFormat.Y_16bits_1u16:
          // array of uint16
          _writer.WriteAsciiUInt16Array(_image.ImagePixelData, elementCount, options.AsciiOptions);
          return ok;
        case EPmPixelFormat.RGB_96bits_3u32:
        case EPmPixelFormat.Y_32bits_1u32:
          // array of uint16
          _writer.WriteAsciiUInt32Array(_image.ImagePixelData, elementCount);
          return ok;
        case EPmPixelFormat.Y_48bits_1u48:
          // array of uint48
          _writer.WriteAsciiUInt48Array(_image.ImagePixelData, elementCount);
          return ok;
        case EPmPixelFormat.RGB_192bits_3u64:
        case EPmPixelFormat.Y_64bits_1u64:
          // array of uint64
          _writer.WriteAsciiUInt64Array(_image.ImagePixelData, elementCount);
          return ok;
        case EPmPixelFormat.RGBA_128bits_4f32:
        case EPmPixelFormat.RGB_96bits_3f32:
          _writer.WriteAsciiFloat32Array(_image.ImagePixelData, elementCount);
          return ok;
        default:
          // array of float32, or otherwise : not supported
          break;
      }

      return EPmWriteStatus.ERROR_NOT_SUPPORTED;
    }

    /// <summary>
    /// Reads image data as binary pixel values
    /// </summary>
    /// <returns>Returns the status of write operation.</returns>
    private EPmWriteStatus WritePixelDataAsBinary()
    {
      unchecked
      {
        // get the pixel size from header
        _image.BitsPerPixel = _image.GetPixelBitsSize();
        if (_image.BitsPerPixel == -1)
        {
          return EPmWriteStatus.ERROR_INVALID_BINARY_PIXEL_SIZE;
        }
        int elementSize = (_image.BitsPerPixel + 7) / 8;
        byte[] byteArray = _image.ImagePixelData;
        switch (_image.PixelFormat)
        {
          case EPmPixelFormat.W_1bit_1u1: 
            {
              // NOTE: must reverse all bits (PBM : 1 is black, 0 is white )
              byte[] data = new byte[byteArray.Length];
              for (int i = 0; i < data.Length; i++)
              {
                data[i] = (byte)(~byteArray[i]);
              }
              byteArray = data;
            }
            break;
        }
        _writer.WriteBytes(byteArray, elementSize);
      }
      return EPmWriteStatus.OK;
    }

    #endregion

    /// <summary>
    /// Reads image data from specified filename.
    /// </summary>
    /// <param name="filename">A filename to be read.</param>
    /// <param name="options">A read options to be used.</param>
    /// <param name="image">An output image data.</param>
    /// <returns>Returns the status of read operation.</returns>
    public static EPmReadStatus Read(string filename, PmReaderOptions options, out PmImage image)
    {
      using (PmFile file = new PmFile())
      {
        EPmReadStatus status = file.Read(filename, options);
        image = file._image;
        return status;
      }
    }

    /// <summary>
    /// Reads image data from specified stream.
    /// </summary>
    /// <param name="stream">A stream to be read.</param>
    /// <param name="options">A read options to be used.</param>
    /// <param name="image">An output image data.</param>
    /// <returns>Returns the status of read operation.</returns>
    public static EPmReadStatus Read(Stream stream, PmReaderOptions options, out PmImage image)
    {
      using (PmFile file = new PmFile())
      {
        EPmReadStatus status = file.Read(stream, options);
        image = file._image;
        return status;
      }
    }

    /// <summary>
    /// Writes image data to specified filename.
    /// </summary>
    /// <param name="filename">A filename to be written.</param>
    /// <param name="options">A write options to be used.</param>
    /// <param name="image">An input image data.</param>
    /// <returns>Returns the status of write operation.</returns>
    public static EPmWriteStatus Write(string filename, PmImage image, PmWriterOptions options = null)
    {
      using (PmFile file = new PmFile())
      {
        file.Image = image;
        EPmWriteStatus status = file.Write(filename, options);
        return status;
      }
    }

    /// <summary>
    /// Writes image data to specified stream.
    /// </summary>
    /// <param name="stream">A stream to be written.</param>
    /// <param name="options">A write options to be used.</param>
    /// <param name="image">An input image data.</param>
    /// <returns>Returns the status of write operation.</returns>
    public static EPmWriteStatus Write(Stream stream, PmImage image, PmWriterOptions options = null)
    {
      using (PmFile file = new PmFile())
      {
        file.Image = image;
        EPmWriteStatus status = file.Write(stream, options);
        return status;
      }
    }

    #region IDisposable Support

    /// <summary>
    /// To detect redundant calls
    /// </summary>
    private bool _disposedValue = false; 

    /// <summary>
    /// Frees unmanaged resources.
    /// </summary>
    /// <param name="disposing">A flag to indicate if to free also and managed resources</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!_disposedValue)
      {
        if (disposing)
        {
          // dispose managed state (managed objects).
          if (null != _reader)
          {
            _reader.Dispose();
            _reader = null;
          }
          if (null != _writer)
          {
            _writer.Dispose();
            _writer = null;
          }
        }
        // free unmanaged resources (unmanaged objects) and override a finalizer below.
        // -- none --
        // set large fields to null.
        if (null != _image)
        {
          _image = null;
        }
        _disposedValue = true;
      }
    }

    // override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
    // ~PmFile() {
    //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
    //   Dispose(false);
    // }

    /// <summary>
    /// Frees internal resources
    /// </summary>
    public void Dispose()
    {
      // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
      Dispose(true);
    }
    #endregion

  }
}
