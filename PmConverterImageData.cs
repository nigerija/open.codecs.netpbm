﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Media;

namespace open.codecs.netpbm
{

  static class ExtensionPixelFormat
  {
    public static PixelFormat CopyCloneDivergence(this PixelFormat self)
    {
      // NOTE: divergence from common practice : PixelFormat.BitsPerPixel shall fail
      //       if we do not instantiate first the PixelFormat pixelFormat = new PixelFormat();
      //
      // NOTE: we are hacking PixelFormat and default setting of PixelFormat
      //       actually throws tons of exception cause it's not initialized.
      //       To avoid such errors, if we, and when we instantiate first the
      //       PixelFormat structure and then copy clone it, everything works
      //       as advertised.
      //       Actually we are using with reflection to invoke constructor 
      //       which is hidden from us, guess with some reason behind it.
      //       But never the less this works.
      //
      PixelFormat pixelFormat = new PixelFormat();
      pixelFormat = self;
      return pixelFormat;
    }
  }

  /// <summary>
  /// Represents converted PbmImage data in unmanaged memory (see <see cref="Data"/> member).
  /// This memory then can be directly assigned to BitmapSource or similar objects.
  /// <para>
  /// ■ NOTE: must dispose to release unmanaged memory.
  /// Or otherwise assign variable to null, and call GC.Collect(int) with 0 and 2 to make sure memory is freed at once.
  /// Failure to do so will leak memory when not needed.
  /// </para>
  /// </summary>
  public class PmConverterImageData : IDisposable
  {
    /// <summary>
    /// Unmanaged pointer to pixel data.
    /// </summary>
    public IntPtr Data
    {
      [MethodImpl(256)]
      get;
      [MethodImpl(256)]
      protected set;
    }
    /// <summary>
    /// Number of bytes allocated for <see cref="Data"/> member in unmanaged memory.
    /// </summary>
    public int DataSize
    {
      [MethodImpl(256)]
      get;
      [MethodImpl(256)]
      protected set;
    }

    /// <summary>
    /// Width in pixels.
    /// </summary>
    public readonly int ImageWidth;
    /// <summary>
    /// Height in pixels.
    /// </summary>
    public readonly int ImageHeight;
    /// <summary>
    /// Number of bits per single pixel.
    /// </summary>
    public readonly int BitsPerPixel;
    /// <summary>
    /// Number of bytes per one line.
    /// </summary>
    public readonly int Stride;
    /// <summary>
    /// Total number of pixels.
    /// </summary>
    public int PixelCount
    {
      [MethodImpl(256)]
      get;
      [MethodImpl(256)]
      protected set;
    }

    /// <summary>
    /// Defines the pixel format.
    /// </summary>
    public readonly PixelFormat PixelFormat;

    /// <summary>
    /// Creates new instance of <see cref="PmConverterImageData"/> class.
    /// </summary>
    /// <param name="width">A width of image data to be created.</param>
    /// <param name="height">A height of image data to be created.</param>
    /// <param name="pixelFormat">A pixel format to be used when creating pixel data buffer.</param>
    [MethodImpl(256)]
    public PmConverterImageData(int width, int height, PixelFormat pixelFormat)
    {
      ImageWidth = width;
      ImageHeight = height;
      // NOTE: divergence from common practice : PixelFormat.BitsPerPixel shall fail, 
      //       if direct copy clone struct which was constructed with hidden constructor.
      PixelFormat = pixelFormat.CopyCloneDivergence();
      BitsPerPixel = PixelFormat.BitsPerPixel;
      PixelCount = ImageWidth * ImageHeight;
      Stride = ((BitsPerPixel * ImageWidth) + 7) >> 3;
      DataSize = ImageHeight * Stride;
      Data = Marshal.AllocHGlobal(DataSize);
    }
    /// <summary>
    /// Returns the <see cref="IntPtr"/> pointer from unmanaged pixel <see cref="Data"/> buffer 
    /// to the beginning of the last line (<see cref="Stride"/>).
    /// </summary>
    public IntPtr DataLastLine
    {
      [MethodImpl(256)]
      get
      {
        return (Data + DataSize - Stride);
      }
    }

    #region IDisposable Support

    // NOTE: Override to free unmanaged resources.

    /// <summary>
    /// Frees unmanaged resources : a <see cref="Data"/> member field.
    /// </summary>
    /// <param name="disposing">A flag indicating if we should free managed resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (IntPtr.Zero != Data)
      {
        Marshal.FreeHGlobal(Data);
        Data = IntPtr.Zero;
        DataSize = 0;
      }
    }

    // NOTE: must override finalizer to free unmanaged resources when GC finalizes this instance

    /// <summary>
    /// Finalizer override in case user forgot to call Dispose method.
    /// Calls Dispose(false) to free unmanaged resources.
    /// </summary>
    ~PmConverterImageData()
    {
      // NOTE: shall not be executed, if Dispose() method called 
      Dispose(false);
    }
    
    /// <summary>
    /// Frees unmanaged resources : a <see cref="Data"/> member field.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      // native memory is released, call to finalizer is no longer necessary
      GC.SuppressFinalize(this);
    }

    #endregion
  }
}
