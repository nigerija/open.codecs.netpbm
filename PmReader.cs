﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm
{
  // NOTE: de-facto standard uses for new line only LINEFEED character (0x0A)
  //       but for bad implementations we shall also include support of CARRY-RETURN character (0x0D).

  /// <summary>
  /// Portable Map image container reader
  /// </summary>
  unsafe class PmReader : BinaryReader
  {
    /// <summary>
    /// Indicates the byte order ("endianness") in which data is stored in this computer architecture.
    /// Used in testing hardware endian against required reader endian type.
    /// </summary>
    public static readonly bool HWIsLittleEndian = BitConverter.IsLittleEndian;

    /// <summary>
    /// A reader endian type.
    /// PNM binary format is most significant byte (MSB) first, so little endian by default.
    /// But some formats like FLOAT binary (PF4) support also big endian and little endian formats in same time.
    /// </summary>
    public bool ReadLittleEndian = true;

    /// <summary>
    /// Creates new instance of <see cref="PmReader"/> class with specified parameters.
    /// </summary>
    /// <param name="input">A input stream to read from.</param>
    /// <param name="readLittleEndian">A read endian to be used during the binary read of data.</param>
    /// <param name="leaveOpen">True to leave the stream open after <see cref="PmReader"/> object is disposed; Otherwise, false to close it.</param>
    public PmReader(Stream input, bool readLittleEndian = true, bool leaveOpen = false)
        : base(input, new UTF8Encoding(false, true), leaveOpen)
    {
      ReadLittleEndian = readLittleEndian;
    }

    /// <summary>
    /// Returns true if Hardware and Reader endian match, false otherwise.
    /// </summary>
    public bool HWAndReaderEndianMatch
    {
      [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
      get
      {
        return (HWIsLittleEndian != ReadLittleEndian);
      }
    }

    #region ### binary read methods ###

    /// <summary>
    /// <para>SHORT (16 bit) in reader endian set by <see cref="ReadLittleEndian"/> member.</para>
    /// <para>Reads reader endian Int16 from the stream and advances the stream position by two (2) bytes.</para>
    /// </summary>
    /// <returns>Returns Int16 value read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override short ReadInt16()
    {
      unchecked
      {
        ushort value = base.ReadUInt16();
        if (HWIsLittleEndian != ReadLittleEndian)
          value = (ushort)((value >> 8) | (value << 8));
        return (short)value;
      }
    }

    /// <summary>
    /// <para>USHORT (16 bit) in reader endian set by <see cref="ReadLittleEndian"/> member.</para>
    /// <para>Reads reader endian UInt16 from the stream and advances the stream position by two (2) bytes.</para>
    /// </summary>
    /// <returns>Returns UInt16 value read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override ushort ReadUInt16()
    {
      unchecked
      {
        ushort value = base.ReadUInt16();
        if (HWIsLittleEndian != ReadLittleEndian)
          value = (ushort)((value >> 8) | (value << 8));
        return value;
      }
    }

    /// <summary>
    /// <para>INT (32 bit) in reader endian set by <see cref="ReadLittleEndian"/> member.</para>
    /// <para>Reads reader endian Int32 from the stream and advances the stream position by four (4) bytes.</para>
    /// </summary>
    /// <returns>Returns Int32 value.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override int ReadInt32()
    {
      unchecked
      {
        uint value = base.ReadUInt32();
        if (HWIsLittleEndian != ReadLittleEndian)
          value = (uint)(((value /*& 0xFF000000*/) >> 24) |
                         ((value & 0x00FF0000) >> 8) |
                         ((value & 0x0000FF00) << 8) |
                         ((value /*& 0x000000FF*/) << 24));
        return (int)value;
      }
    }
    /// <summary>
    /// <para>UINT (32 bit) in reader endian set by <see cref="ReadLittleEndian"/> member.</para>
    /// <para>Reads reader endian UInt32 from the stream and advances the stream position by four (4) bytes.</para>
    /// </summary>
    /// <returns>Returns UInt32 value.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override uint ReadUInt32()
    {
      unchecked
      {
        uint value = base.ReadUInt32();
        if (HWIsLittleEndian != ReadLittleEndian)
          value = (uint)(((value /*& 0xFF000000*/) >> 24) |
                         ((value & 0x00FF0000) >> 8) |
                         ((value & 0x0000FF00) << 8) |
                         ((value /*& 0x000000FF*/) << 24));
        return value;
      }
    }

    /// <summary>
    /// <para>ULONG (64 bit) in big endian.</para>
    /// <para>Reads reader endian UInt64 from the stream and advances the stream position by eight (8) bytes.</para>
    /// </summary>
    /// <returns>Returns UInt64 read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override long ReadInt64()
    {
      unchecked
      {
        ulong value = base.ReadUInt64();
        if (HWIsLittleEndian != ReadLittleEndian)
          value = ((value /*& 0xFF00000000000000*/) >> 56) |
                  ((value & 0x00FF000000000000) >> 40) |
                  ((value & 0x0000FF0000000000) >> 24) |
                  ((value & 0x000000FF00000000) >> 08) |
                  ((value & 0x00000000FF000000) << 08) |
                  ((value & 0x0000000000FF0000) << 24) |
                  ((value & 0x000000000000FF00) << 40) |
                  ((value /*& 0x00000000000000FF*/) << 56);
        return (long)value;
      }
    }

    /// <summary>
    /// <para>ULONG (64 bit) in big endian.</para>
    /// <para>Reads reader endian UInt64 from the stream and advances the stream position by eight (8) bytes.</para>
    /// </summary>
    /// <returns>Returns UInt64 read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override ulong ReadUInt64()
    {
      unchecked
      {
        ulong value = base.ReadUInt64();
        if (HWIsLittleEndian != ReadLittleEndian)
          value = ((value /*& 0xFF00000000000000*/) >> 56) |
                  ((value & 0x00FF000000000000) >> 40) |
                  ((value & 0x0000FF0000000000) >> 24) |
                  ((value & 0x000000FF00000000) >> 08) |
                  ((value & 0x00000000FF000000) << 08) |
                  ((value & 0x0000000000FF0000) << 24) |
                  ((value & 0x000000000000FF00) << 40) |
                  ((value /*& 0x00000000000000FF*/) << 56);
        return value;
      }
    }

    [StructLayout(LayoutKind.Explicit)]
    struct t_FloatAndUIntUnion
    {
      [FieldOffset(0)]
      public uint U4;
      [FieldOffset(0)]
      public float F4;
    }

    /// <summary>
    /// <para>FLOAT (32 bit) in reader endian set by <see cref="ReadLittleEndian"/> member.</para>
    /// <para>Reads reader endian Single (4-byte float) from the stream and advances the stream position by four (4) bytes.</para>
    /// </summary>
    /// <returns>Returns Single (4-byte float) read from this stream.</returns>
    public override float ReadSingle()
    {
      unchecked
      {
        // ■ NOTE: base.ReadUInt32() already accounts for endian
        t_FloatAndUIntUnion t = new t_FloatAndUIntUnion() { U4 = base.ReadUInt32() };
        return t.F4;
      }
    }

    [StructLayout(LayoutKind.Explicit)]
    struct t_DoubleAndULongUnion
    {
      [FieldOffset(0)]
      public ulong U8;
      [FieldOffset(0)]
      public double F8;
    }

    /// <summary>
    /// <para>DOUBLE (64 bit) in reader endian set by <see cref="ReadLittleEndian"/> member.</para>
    /// <para>Reads reader endian Double (8-byte float) from the stream and advances the stream position by eight (8) bytes.</para>
    /// </summary>
    /// <returns>Returns Double (8-byte float) read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override double ReadDouble()
    {
      unchecked
      {
        // ■ NOTE: base.ReadUInt32() already accounts for endian
        t_DoubleAndULongUnion t = new t_DoubleAndULongUnion() { U8 = ReadUInt64() };
        return t.F8;
      }
    }

    /// <summary>
    /// Gets or sets position within the stream.
    /// </summary>
    public long Position
    {
      [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
      get { return BaseStream.Position; }
      [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
      set { BaseStream.Position = value; }
    }

    /// <summary>
    /// Gets the length in bytes of the stream.
    /// </summary>
    public long Length
    {
      [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
      get { return BaseStream.Length; }
    }

    #endregion




    #region ### ASCII read methods ###

    // used in ASCII reading to avoid conversion to char when read as byte

    /// <summary>
    /// 0x20 - ' '
    /// </summary>
    const byte BYTE_SPACE = 0x20;
    /// <summary>
    /// 0x0D - '\r'
    /// </summary>
    const byte BYTE_CR = 0x0D;
    /// <summary>
    /// 0x0A - '\n'
    /// </summary>
    const byte BYTE_LF = 0x0A;
    /// <summary>
    /// 0x09 - '\t'
    /// </summary>
    const byte BYTE_TAB = 0x09;
    /// <summary>
    /// 0x23 - '#'
    /// </summary>
    const byte BYTE_SHARP = 0x23;
    /// <summary>
    /// 0x30 - '0'
    /// </summary>
    const byte BYTE_NUM_0 = 0x30;
    /// <summary>
    /// 0x31 - '1'
    /// </summary>
    const byte BYTE_NUM_1 = 0x31;
    /// <summary>
    /// 0x39 - '9'
    /// </summary>
    const byte BYTE_NUM_9 = 0x39;

    /// <summary>
    /// Triggered once comment is encountered and read.
    /// </summary>
    internal event Action<string> OnComment = null;

    /// <summary>
    /// Skips reminder of text until CR '\r' or LF '\n' character encountered.
    /// </summary>
    /// <returns>Returns true if not end of stream; Otherwise returns false.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public bool ReadAsciiComment()
    {
      List<char> characters = new List<char>();
      byte b;
      while (Position < Length)
      {
        b = ReadByte();
        if (b == BYTE_CR)
        {
          continue;
        }
        else if (b == BYTE_LF)
        {
          break;
        }
        characters.Add((char)b);
      }
      OnComment?.Invoke(new string(characters.ToArray()));
      return Position < Length;
    }

    /// <summary>
    /// Skips white space characters: ' ', '\t', '\n', '\r', and reminder of line if '#' character encountered.
    /// </summary>
    /// <returns>Returns true if not end of stream; Otherwise, if end of stream returns false.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public bool SkipAsciiSeparators()
    {
      byte b;
      while (Position < Length)
      {
        b = ReadByte();
        switch (b)
        {
          case BYTE_SPACE:
          case BYTE_TAB:
          case BYTE_CR:
          case BYTE_LF:
            continue;
          case BYTE_SHARP:
            ReadAsciiComment();
            continue;
        }
        Position--;
        break;
      }
      return (Position < Length);
    }

    /// <summary>
    /// Returns byte array of ASCII characters right until first Whitespace character (Space ' ', Tab '\t', Carry-Return '\r', Line-Feed '\n')
    /// </summary>
    /// <returns>Returns string expression of read bytes from stream right until first separator character.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    string ReadAsciiWordString()
    {
      byte b;
      List<byte> word = new List<byte>();
      while (Position < Length)
      {
        b = ReadByte();
        if (b == BYTE_SPACE ||
            b == BYTE_TAB ||
            b == BYTE_CR ||
            b == BYTE_LF)
        {
          break;
        }
        else if (b == BYTE_SHARP)
        {
          ReadAsciiComment();
          break;
        }
        word.Add(b);
      }
      return Encoding.UTF8.GetString(word.ToArray());
    }

    /// <summary>
    /// Skips ASCII separators, and reads the ASCII word until next separator.
    /// </summary>
    /// <param name="expression"></param>
    /// <returns>Returns true if not end of stream; Otherwise returns false.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public bool ReadAsciiWord(out string expression)
    {
      if (SkipAsciiSeparators())
      {
        expression = ReadAsciiWordString();
        return true;
      }
      expression = null;
      return false;
    }

    /// <summary>
    /// Reads header magic code and return read status of <paramref name="fileType"/> value.
    /// </summary>
    /// <param name="fileType"></param>
    /// <returns>Returns read status.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public EPmReadStatus ReadFileType(out EPmFileType fileType)
    {
      fileType = EPmFileType.UNKNOWN;
      if (!ReadAsciiWord(out string sz))
      {
        return EPmReadStatus.ERROR_EOF;
      }
      switch (sz.Length)
      {
        default:
          return EPmReadStatus.ERROR_FILE_TYPE;
        case 2: // 'P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'PF', 'Pf'
          fileType = (EPmFileType)(sz[0] << 8 | sz[1]);
          return EPmReadStatus.OK;
        case 3: // 'PF4', 'Pf4'
          fileType = (EPmFileType)(sz[0] << 16 | sz[1] << 8 | sz[2]);
          return EPmReadStatus.OK;
      }
    }

    //if (!ulong.TryParse(Encoding.UTF8.GetString(byteArray.ToArray()), out number))
    private static bool ULongParse(List<byte> list, out ulong result)
    {
      result = 0;
      int ipos = list.Count;
      int imul = 1;
      while(ipos > 0)
      {
        ipos--;
        result += (ulong)((list[ipos] - BYTE_NUM_0) * imul);
        imul *= 10;
      }
      return true;
    }

    /// <summary>
    /// Skips any non-numeric characters (if any), reads numeric characters until first non-numeric.
    /// And returns converted string numeric expression as <see cref="ulong"/> value.
    /// </summary>
    /// <param name="number">A number to be returned.</param>
    /// <returns>Returns true if number value is read; Otherwise false.</returns>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public bool ReadAsciiNumber(out ulong number)
    {
      // maybe too complicated !?
      if (Position >= Length)
      {
        number = 0;
        return false;
      }
      byte b = 0;
      // using list is maybe an overkill, max is 64-bit, max characters 20
      List<byte> byteArray = new List<byte>(32);
      // skip any non numeric characters and read first numeric character
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= BYTE_NUM_0 && b <= BYTE_NUM_9)
        {
          // encountered numeric
          byteArray.Add(b);
          break;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
      }
      // read other numeric characters, if any
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= BYTE_NUM_0 && b <= BYTE_NUM_9)
        {
          byteArray.Add(b);
          continue;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
        break;
      }
      // convert to unsigned int value
      //if (!ulong.TryParse(Encoding.UTF8.GetString(byteArray.ToArray()), out number))
      if (!ULongParse(byteArray, out number))
      {
        // error in conversion !?
        return false;
      }
      // success
      return true;
    }

    /// <summary>
    /// Skips any non-numeric characters (if any), reads numeric characters until first non-numeric.
    /// And returns converted string numeric expression as <see cref="float"/> value.
    /// </summary>
    /// <param name="number">A number to be returned.</param>
    /// <returns>Returns true if number value is read; Otherwise false.</returns>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public bool ReadAsciiNumberFloat32(out float number)
    {
      // maybe too complicated !?
      if (Position >= Length)
      {
        number = 0;
        return false;
      }
      byte b = 0;
      // using list is maybe an overkill, max is 64-bit, max characters 20
      List<byte> byteArray = new List<byte>(32);
      // skip any non numeric characters
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          // encountered numeric
          byteArray.Add(b);
          break;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
      }
      // read other numeric characters, if any
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          byteArray.Add(b);
          continue;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
        break;
      }
      // convert to Float32 value
      string value = Encoding.UTF8.GetString(byteArray.ToArray());
      if (!float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out number))
      {
        // error in conversion !?
        return false;
      }
      // success
      return true;
    }


    /// <summary>
    /// Skips any non-numeric characters (if any), reads numeric characters until first non-numeric.
    /// And returns converted string numeric expression as <see cref="ulong"/> value.
    /// </summary>
    /// <param name="number">A number to be returned.</param>
    /// <returns>Returns true if number value is read; Otherwise false.</returns>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public bool ReadAsciiNumber(out UInt48 number)
    {
      // maybe too complicated !?
      if (Position >= Length)
      {
        number = 0;
        return false;
      }
      byte b = 0;
      // using list is maybe an overkill, max is 64-bit, max characters 20
      List<byte> byteArray = new List<byte>(32);
      // skip any non numeric characters
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          // encountered numeric
          byteArray.Add(b);
          break;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
      }
      // read other numeric characters, if any
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          byteArray.Add(b);
          continue;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
        break;
      }
      // convert to unsigned int value
      if (!UInt48.TryParse(Encoding.UTF8.GetString(byteArray.ToArray()), out number))
      {
        // error in conversion !?
        return false;
      }
      // success
      return true;
    }

    /// <summary>
    /// Skips any non-numeric characters (if any), reads numeric characters until first non-numeric.
    /// And returns converted string numeric expression as <see cref="uint"/> value.
    /// </summary>
    /// <param name="number">A number to be returned.</param>
    /// <returns>Returns true if number value is read; Otherwise false.</returns>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public bool ReadAsciiNumber(out uint number)
    {
      // maybe too complicated !?
      if (Position >= Length)
      {
        number = 0;
        return false;
      }
      byte b = 0;
      // using list is maybe an overkill, max is 32-bit, max characters 10
      List<byte> byteArray = new List<byte>(16);
      // skip any non numeric characters
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          // encountered numeric
          byteArray.Add(b);
          break;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
      }
      // read other numeric characters, if any
      while (Position < Length)
      { 
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          byteArray.Add(b);
          continue;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
        break;
      }
      // convert to unsigned int value
      if (!uint.TryParse(Encoding.UTF8.GetString(byteArray.ToArray()), out number))
      {
        // error in conversion !?
        return false;
      }
      // success
      return true;
    }

    /// <summary>
    /// Skips any non-numeric characters (if any), reads numeric characters until first non-numeric.
    /// And returns converted string numeric expression as <see cref="ushort"/> value.
    /// </summary>
    /// <param name="number">A number to be returned.</param>
    /// <returns>Returns true if number value is read; Otherwise false.</returns>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public bool ReadAsciiNumber(out ushort number)
    {
      // maybe too complicated !?
      if (Position >= Length)
      {
        number = 0;
        return false;
      }
      byte b = 0;
      // using list is maybe an overkill, max is 16-bit, max characters 10
      List<byte> byteArray = new List<byte>(16);
      // skip any non numeric characters
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          // encountered numeric
          byteArray.Add(b);
          break;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
      }
      // read other numeric characters, if any
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          byteArray.Add(b);
          continue;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
        break;
      }
      // convert to unsigned int value
      if (!ushort.TryParse(Encoding.UTF8.GetString(byteArray.ToArray()), out number))
      {
        // error in conversion !?
        return false;
      }
      // success
      return true;
    }

    /// <summary>
    /// Skips any non-numeric characters (if any), reads numeric characters until first non-numeric.
    /// And returns converted string numeric expression as <see cref="byte"/> value.
    /// </summary>
    /// <param name="number">A number to be returned.</param>
    /// <returns>Returns true if number value is read; Otherwise false.</returns>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public bool ReadAsciiNumber(out byte number)
    {
      // maybe too complicated !?
      if (Position >= Length)
      {
        number = 0;
        return false;
      }
      byte b = 0;
      // using list is maybe an overkill, max is 8-bit, max characters 3
      List<byte> byteArray = new List<byte>(4);
      // skip any non numeric characters
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          // encountered numeric
          byteArray.Add(b);
          break;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
      }
      // read other numeric characters, if any
      while (Position < Length)
      {
        b = ReadByte();
        if (b >= 48 && b <= 57)
        {
          byteArray.Add(b);
          continue;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          ReadAsciiComment();
        }
        break;
      }
      // convert to unsigned int value
      if (!byte.TryParse(Encoding.UTF8.GetString(byteArray.ToArray()), out number))
      {
        // error in conversion !?
        return false;
      }
      // success
      return true;
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as <see cref="ulong"/> type.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">A output array of elements, represented as byte array of <see cref="ulong"/> elements.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public bool ReadAsciiUInt64Array(int elementsCount, out byte[] byteArray)
    {
      byteArray = new byte[elementsCount << 3];
      fixed (byte* pByteArray = byteArray)
      {
        var pUInt64Array = (ulong*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          if (!ReadAsciiNumber(out pUInt64Array[i]))
          {
            return false;
          }
        }
      }
      return true;
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as <see cref="float"/> type.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">A output array of elements, represented as byte array of <see cref="float"/> elements.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public bool ReadAsciiFloat32Array(int elementsCount, out byte[] byteArray)
    {
      byteArray = new byte[elementsCount << 3];
      fixed (byte* pByteArray = byteArray)
      {
        var pFloat32Array = (float*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          if (!ReadAsciiNumberFloat32(out pFloat32Array[i]))
          {
            return false;
          }
        }
      }
      return true;
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as <see cref="UInt48"/> type.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">A output array of elements, represented as byte array of <see cref="UInt48"/> elements.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public bool ReadAsciiUInt48Array(int elementsCount, out byte[] byteArray)
    {
      byteArray = new byte[elementsCount << 3];
      fixed (byte* pByteArray = byteArray)
      {
        var pUInt48Array = (UInt48*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          if (!ReadAsciiNumber(out pUInt48Array[i]))
          {
            return false;
          }
        }
      }
      return true;
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as <see cref="uint"/> type.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">A output array of elements, represented as byte array of <see cref="uint"/> elements.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public bool ReadAsciiUInt32Array(int elementsCount, out byte[] byteArray)
    {
      byteArray = new byte[elementsCount << 2];
      fixed (byte* pByteArray = byteArray)
      {
        var pUInt32Array = (uint*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          if (!ReadAsciiNumber(out pUInt32Array[i]))
          {
            return false;
          }
        }
      }
      return true;
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as <see cref="ushort"/> type.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">A output array of elements, represented as byte array of <see cref="ushort"/> elements.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public bool ReadAsciiUInt16Array(int elementsCount, out byte[] byteArray)
    {
      byteArray = new byte[elementsCount << 1];
      fixed (byte* pByteArray = byteArray)
      {
        var pUInt16Array = (ushort*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          if (!ReadAsciiNumber(out pUInt16Array[i]))
          {
            return false;
          }
        }
      }
      return true;
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as <see cref="byte"/> type.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">A output array of elements.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public bool ReadAsciiByteArray(int elementsCount, out byte[] byteArray)
    {
      byteArray = new byte[elementsCount];
      for (int i = 0; i < elementsCount; i++)
      {
        if (!ReadAsciiNumber(out byteArray[i]))
        {
          return false;
        }
      }
      return true;
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as bit type, and stores them in byte array.
    /// Skips any non-numeric characters (if any), reads numeric characters until first non-numeric.
    /// And returns converted string numeric expression as array of bytes bits.
    /// </summary>
    /// <returns>Returns true if array is read; Otherwise false.</returns>
    //[DebuggerHidden()]
    [MethodImpl(256)]
    public bool ReadAsciiBitsArray(int elementsCount, PmImage image)
    {
      if (Position >= Length)
      {
        return false;
      }
      // NOTE: in WIC first bit (highest bit) is first pixel      
      const int BITS_READ_COUNT = 8;
      int bitReminderShift = BITS_READ_COUNT;
      int bytePosition = 0;      
      int bufferSize = image.ImageHeight * image.Stride;
      var byteArray = image.ImagePixelData = new byte[bufferSize];
      int bitsRead = 0;
      int bitsStride = image.StrideBits;
      byte b;
      byte value = 0;
      while (Position < Length)
      {
        b = ReadByte();
        if (b == BYTE_NUM_0 || b == BYTE_NUM_1) // '0' or '1'
        {
          // NOTE: 1 is black, 0 is white
          bitReminderShift--;
          if (b == BYTE_NUM_0)
          {
            // set value bit for '1' characters
            value |= (byte)(1 << bitReminderShift);
          }
          bitsRead++;
          if (bitsRead == image.ImageWidth)
          {
            bitReminderShift = bitsRead = 0;
          }
          if (bitReminderShift == 0)
          {
            byteArray[bytePosition] = value;
            bytePosition++;
            bitReminderShift = BITS_READ_COUNT;
            value = 0;
          }
        }
        else if (b == BYTE_SHARP)
        {
          // by standard document there should be no comments in pixel data
          // but we are fault tolerant reader and we shall allow parsing of comments in pixel data
          ReadAsciiComment();
        }
      }
      // commit last value if anything reminds
      if (bitReminderShift < BITS_READ_COUNT)
      {
        byteArray[bytePosition] = value;
      }
      // success
      return true;
    }

    #endregion

    #region ### Binary read methods ###

    #region ### Swap Endianness Fast ###

    /* 
     * test run on 100 * 8Mb byte[] buffers
    HPC[1001]::ShowStatistic() called from : [JITTrack,E&C] JpegLSTest.Program::TestEndinnessSwapping()
    ┌───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    │ meter       : sumtime  / cnt ~ average  | times * time.min ~ time.max : max-min  ( max-avrg , avrg-min ) over time improving  │
    ├───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
    │ SwapU2Bytes : 0.761279 / 100 ~ 0.007613 |     1 * 0.007188 ~ 0.011845 : 0.004657 ( 0.004232 , 0.000425 ) ▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫ │
    │ SwapU4Bytes : 0.755487 / 100 ~ 0.007555 |     1 * 0.007137 ~ 0.011128 : 0.003991 ( 0.003573 , 0.000418 ) ▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫ │
    │ SwapU8Bytes : 0.757489 / 100 ~ 0.007575 |     1 * 0.007199 ~ 0.010217 : 0.003018 ( 0.002642 , 0.000376 ) ▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫ │
    │ SwapX2      : 1.204579 / 100 ~ 0.012046 |     1 * 0.011515 ~ 0.013712 : 0.002197 ( 0.001667 , 0.000531 ) ▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫ │
    │ SwapX4      : 0.817181 / 100 ~ 0.008172 |     1 * 0.007798 ~ 0.013059 : 0.005261 ( 0.004888 , 0.000373 ) ▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫▫ │
    └───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    HPC[1001]::ShowStatistic() called from : [JITOpt] JpegLSTest.Program::TestEndinnessSwapping()
    ┌───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    │ meter       : sumtime  / cnt ~ average  | times * time.min ~ time.max : max-min  ( max-avrg , avrg-min ) over time improving  │
    ├───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
    │ SwapU2Bytes : 0.311244 / 100 ~ 0.003112 |     2 * 0.002738 ~ 0.006813 : 0.004075 ( 0.003700 , 0.000374 ) ???????????????????? │
    │ SwapU4Bytes : 0.328578 / 100 ~ 0.003286 |     3 * 0.002733 ~ 0.008743 : 0.006009 ( 0.005457 , 0.000552 ) ???????????????????? │
    │ SwapU8Bytes : 0.358203 / 100 ~ 0.003582 |     2 * 0.003168 ~ 0.006926 : 0.003758 ( 0.003344 , 0.000414 ) ???????????????????? │
    │ SwapX2      : 0.310273 / 100 ~ 0.003103 |     2 * 0.002758 ~ 0.007843 : 0.005085 ( 0.004741 , 0.000345 ) ???????????????????? │
    │ SwapX4      : 0.318257 / 100 ~ 0.003183 |     2 * 0.002745 ~ 0.008107 : 0.005362 ( 0.004924 , 0.000438 ) ???????????????????? │
    └───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
     * test run on 100000 * 8Kb byte[] buffers
    HPC[1001]::ShowStatistic() called from : [JITTrack,E&C] JpegLSTest.Program::TestEndinnessSwapping()
    ┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    │ meter       : sumtime  /    cnt ~ average  | times * time.min ~ time.max : max-min  ( max-avrg , avrg-min ) over time improving  │
    ├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
    │ SwapU2Bytes : 0.757011 / 100000 ~ 0.000008 |   455 * 0.000007 ~ 0.003110 : 0.003103 ( 0.003103 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapU4Bytes : 0.749213 / 100000 ~ 0.000007 | 1 178 * 0.000007 ~ 0.008045 : 0.008039 ( 0.008038 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapU8Bytes : 0.749137 / 100000 ~ 0.000007 |   857 * 0.000007 ~ 0.005851 : 0.005844 ( 0.005843 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapX2      : 1.216467 / 100000 ~ 0.000012 |   353 * 0.000011 ~ 0.003947 : 0.003936 ( 0.003935 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapX4      : 0.808326 / 100000 ~ 0.000008 |   126 * 0.000007 ~ 0.000941 : 0.000933 ( 0.000933 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    └──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    HPC[1001]::ShowStatistic() called from : [JITOpt] JpegLSTest.Program::TestEndinnessSwapping()
    ┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    │ meter       : sumtime  /    cnt ~ average  | times * time.min ~ time.max : max-min  ( max-avrg , avrg-min ) over time improving  │
    ├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
    │ SwapU2Bytes : 0.274493 / 100000 ~ 0.000003 |   637 * 0.000002 ~ 0.001582 : 0.001579 ( 0.001579 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapU4Bytes : 0.272241 / 100000 ~ 0.000003 |   355 * 0.000002 ~ 0.000883 : 0.000881 ( 0.000881 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapU8Bytes : 0.325498 / 100000 ~ 0.000003 |   315 * 0.000003 ~ 0.000880 : 0.000877 ( 0.000876 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapX2      : 0.285102 / 100000 ~ 0.000003 | 3 516 * 0.000002 ~ 0.008729 : 0.008726 ( 0.008726 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapX4      : 0.273668 / 100000 ~ 0.000003 |   412 * 0.000002 ~ 0.001024 : 0.001021 ( 0.001021 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    └──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    Note: these variants clearly win DEBUG condition due var length optimization, and match RELEASE speed optimization.
    Note: MethodImpl plays no role in tests, but left it on purpose if someone wants to play with it.
    */

    /// <summary>
    /// Swaps INT16 or UINT16 bytes in <paramref name="bytes"/> stream.
    /// Optimized for debug run.
    /// </summary>
    /// <param name="bytes">The stream of INT16 or UINT16 bytes to be swapped.</param>
    [DebuggerNonUserCode]
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    static unsafe void SwapU2Bytes(byte[] bytes)
    {
      unchecked
      {
        fixed (byte* lp = bytes)
        {
          byte* p = lp;
          byte* pend = lp + bytes.Length - (bytes.Length % 2);
          // swap up to reminder
          byte* pend2rem = pend - (bytes.Length % 8);
          while (p < pend2rem)
          {
            *(ulong*)p = (
                ((uint)(*p) << 8) |
                (*(p + 1)) |
                ((uint)(*(p + 2)) << 24) |
                ((uint)(*(p + 3)) << 16) |
                ((ulong)(*(p + 4)) << 40) |
                ((ulong)(*(p + 5)) << 32) |
                ((ulong)(*(p + 6)) << 56) |
                ((ulong)(*(p + 7)) << 48)
                );
            p += 8;
          }
          // swap reminder
          while (p < pend)
          {
            *(ushort*)p = (ushort)(
                (*p << 8) |
                (*(p + 1)));
            p += 2;
          }
        }
      }
    }

    /// <summary>
    /// Swaps INT32 or UINT32 bytes in <paramref name="bytes"/> stream.
    /// Optimized for debug run.        
    /// </summary>
    /// <param name="bytes">The stream of INT32 or UINT32 bytes to be swapped.</param>
    [DebuggerNonUserCode]
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    static unsafe void SwapU4Bytes(byte[] bytes)
    {
      unchecked
      {
        fixed (byte* lp = bytes)
        {
          byte* p = lp;
          byte* pend = lp + bytes.Length - (bytes.Length % 4);

          // swap up to reminder
          byte* pend2rem = pend - (bytes.Length % 8);
          while (p < pend2rem)
          {
            *(ulong*)p = (
                ((uint)(*p) << 24) |
                ((uint)(*(p + 1)) << 16) |
                ((uint)(*(p + 2)) << 8) |
                (*(p + 3)) |
                ((ulong)(*(p + 4)) << 56) |
                ((ulong)(*(p + 5)) << 48) |
                ((ulong)(*(p + 6)) << 40) |
                ((ulong)(*(p + 7)) << 32)
                );
            p += 8;
          }
          // swap reminder

          while (p < pend)
          {
            *(uint*)p = (uint)(
                (*p << 24) |
                (*(p + 1) << 16) |
                (*(p + 2) << 8) |
                (*(p + 3))
                );
            p += 4;
          }
        }
      }
    }

    /// <summary>
    /// Swaps INT64 or UINT64 bytes in <paramref name="bytes"/> stream.
    /// </summary>
    /// <param name="bytes">The stream of INT64 or UINT64 bytes to be swapped.</param>
    [DebuggerNonUserCode]
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    static unsafe void SwapU8Bytes(byte[] bytes)
    {
      fixed (byte* lp = bytes)
      {
        byte* p = lp;
        byte* pend = p + bytes.Length - (bytes.Length % 8);
        while (p < pend)
        {
          *(ulong*)p = (
              (*(p + 7))
               | ((uint)(*(p + 6)) << 8)
               | ((uint)(*(p + 4)) << 24)
               | ((uint)(*(p + 5)) << 16)
               | ((ulong)(*(p + 3)) << 32)
               | ((ulong)(*(p + 2)) << 40)
               | ((ulong)(*(p + 1)) << 48)
               | ((ulong)(*(p)) << 56)
               );
          p += 8;
        }
      }
    }

    #endregion

    /// <summary>
    /// Reads number of <paramref name="count"/> bytes in reader endian for <paramref name="elementSize"/>.
    /// </summary>
    /// <param name="count">A number of elements (pixels) to be read.</param>
    /// <param name="elementSize">An element (pixel) size in bytes.</param>
    /// <returns>Returns read elements (pixel) bytes.</returns>
    public unsafe byte[] ReadBytes(int count, int elementSize)
    {
      byte[] bytes = base.ReadBytes(count);
      if (HWIsLittleEndian == ReadLittleEndian)
        return bytes;
      
      // ■ Note: we could have chosen to read with endian swapped
      //         but this is 20 times or more faster
      switch (elementSize)
      {
        case 1:
          // do nothing
          break;
        case 2: 
          // must swap UINT16
          SwapU2Bytes(bytes);
          break;
        case 4: 
          // must swap UINT32
          SwapU4Bytes(bytes);
          break;
        case 8: 
          // must swap UINT64
          SwapU8Bytes(bytes);
          break;
      }
      return bytes;
    }

    #endregion

  }
}
