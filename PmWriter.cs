﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm
{
  /// <summary>
  /// Portable Map image container writer
  /// </summary>
  unsafe class PmWriter : BinaryWriter
  {
    /// <summary>
    /// This flag determines hardware endian used in testing hardware endian against required endian type.
    /// </summary>
    public static readonly bool HWIsLittleEndian = BitConverter.IsLittleEndian;

    /// <summary>
    /// PNM binary format is most significant byte (MSB) first
    /// </summary>
    public bool WriteLittleEndian = true;

    /// <summary>
    /// Creates new instance of <see cref="PmWriter"/> class with specified parameters.
    /// </summary>
    /// <param name="stream">An output stream to read from.</param>
    /// <param name="writeLittleEndian">A write endian to be used during the binary read of data.</param>
    /// <param name="leaveOpen">True to leave the stream open after <see cref="PmReader"/> object is disposed; Otherwise, false to close it.</param>
    public PmWriter(Stream stream, bool writeLittleEndian = true, bool leaveOpen = false)
        : base(stream, new UTF8Encoding(false, true), leaveOpen)
    {
      WriteLittleEndian = writeLittleEndian;
    }

    /// <summary>
    /// Returns true if Hardware and Reader endian match, false otherwise.
    /// </summary>
    public bool HWAndReaderEndianMatch
    {
      [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
      get
      {
        return (HWIsLittleEndian != WriteLittleEndian);
      }
    }

    #region ### binary read methods ###

    /// <summary>
    /// <para>SHORT (16 bit) in endian set by <see cref="WriteLittleEndian"/> member.</para>
    /// <para>Writes writer endian <see cref="short"/>  to the stream and advances the stream position by two (2) bytes.</para>
    /// </summary>
    /// <returns>Returns Int16 value read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override void Write(short value)
    {
      unchecked
      {
        if (HWIsLittleEndian != WriteLittleEndian)
          value = (short)((value >> 8) | (value << 8));
        base.Write(value);
      }
    }

    /// <summary>
    /// <para>USHORT (16 bit) in endian set by <see cref="WriteLittleEndian"/> member.</para>
    /// <para>Writes writer endian <see cref="ushort"/> to the stream and advances the stream position by two (2) bytes.</para>
    /// </summary>
    /// <returns>Returns UInt16 value read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override void Write(ushort value)
    {
      unchecked
      {
        if (HWIsLittleEndian != WriteLittleEndian)
          value = (ushort)((value >> 8) | (value << 8));
        base.Write(value);
      }
    }

    /// <summary>
    /// <para>INT (32 bit) in endian set by <see cref="WriteLittleEndian"/> member.</para>
    /// <para>Writes in writer endian Int32 from the stream and advances the stream position by four (4) bytes.</para>
    /// </summary>
    /// <returns>Returns Int32 value.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override void Write(int value)
    {
      unchecked
      {
        if (HWIsLittleEndian != WriteLittleEndian)
          value = (int)(((value /*& 0xFF000000*/) >> 24) |
                        ((value & 0x00FF0000) >> 8) |
                        ((value & 0x0000FF00) << 8) |
                        ((value /*& 0x000000FF*/) << 24));
        base.Write(value);
      }
    }
    /// <summary>
    /// <para>UINT (32 bit) in endian set by <see cref="WriteLittleEndian"/> member.</para>
    /// <para>Writes in writer endian UInt32 from the stream and advances the stream position by four (4) bytes.</para>
    /// </summary>
    /// <returns>Returns UInt32 value.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override void Write(uint value)
    {
      unchecked
      {
        if (HWIsLittleEndian != WriteLittleEndian)
          value = (uint)(((value /*& 0xFF000000*/) >> 24) |
                         ((value & 0x00FF0000) >> 8) |
                         ((value & 0x0000FF00) << 8) |
                         ((value /*& 0x000000FF*/) << 24));
        base.Write(value);
      }
    }

    /// <summary>
    /// <para>ULONG (64 bit) in endian set by <see cref="WriteLittleEndian"/> member.</para>
    /// <para>Writes in writer endian UInt64 from the stream and advances the stream position by eight (8) bytes.</para>
    /// </summary>
    /// <returns>Returns UInt64 read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override void Write(long value)
    {
      unchecked
      {

        if (HWIsLittleEndian != WriteLittleEndian)
          value = ((value /*& 0xFF00000000000000*/) >> 56) |
                  ((value & 0x00FF000000000000) >> 40) |
                  ((value & 0x0000FF0000000000) >> 24) |
                  ((value & 0x000000FF00000000) >> 08) |
                  ((value & 0x00000000FF000000) << 08) |
                  ((value & 0x0000000000FF0000) << 24) |
                  ((value & 0x000000000000FF00) << 40) |
                  ((value /*& 0x00000000000000FF*/) << 56);
        base.Write(value);
      }
    }

    /// <summary>
    /// <para>ULONG (64 bit) in endian set by <see cref="WriteLittleEndian"/> member.</para>
    /// <para>Writes in writer endian UInt64 from the stream and advances the stream position by eight (8) bytes.</para>
    /// </summary>
    /// <returns>Returns UInt64 read from this stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override void Write(ulong value)
    {
      unchecked
      {
        if (HWIsLittleEndian != WriteLittleEndian)
          value = ((value /*& 0xFF00000000000000*/) >> 56) |
                  ((value & 0x00FF000000000000) >> 40) |
                  ((value & 0x0000FF0000000000) >> 24) |
                  ((value & 0x000000FF00000000) >> 08) |
                  ((value & 0x00000000FF000000) << 08) |
                  ((value & 0x0000000000FF0000) << 24) |
                  ((value & 0x000000000000FF00) << 40) |
                  ((value /*& 0x00000000000000FF*/) << 56);
        base.Write(value);
      }
    }

    [StructLayout(LayoutKind.Explicit)]
    struct t_FloatAndUIntUnion
    {
      [FieldOffset(0)]
      public uint U4;
      [FieldOffset(0)]
      public float F4;
    }

    t_FloatAndUIntUnion _u4f4;
    /// <summary>
    /// <para>FLOAT (32 bit) in endian set by <see cref="WriteLittleEndian"/> member.</para>
    /// <para>Writes in writer endian Single (4-byte float) from the stream and advances the stream position by four (4) bytes.</para>
    /// </summary>
    /// <returns>Returns Single (4-byte float) read from this stream.</returns>
    public override void Write(float value)
    {
      unchecked
      {
        // ■ NOTE: base.Write(UInt32) already accounts for endian
        _u4f4.F4 = value;
        Write(_u4f4.U4);
      }
    }

    [StructLayout(LayoutKind.Explicit)]
    struct t_DoubleAndULongUnion
    {
      [FieldOffset(0)]
      public ulong U8;
      [FieldOffset(0)]
      public double F8;
    }
    
    /// <summary>
    /// <para>DOUBLE (64 bit) in endian set by <see cref="WriteLittleEndian"/> member.</para>
    /// <para>Writes in writer endian Double (8-byte float) to the stream and advances the stream position by eight (8) bytes.</para>
    /// </summary>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public override void Write(double value)
    {
      unchecked
      {
        // ■ NOTE: Write(ulong) already accounts for endian
        t_DoubleAndULongUnion _u8f8 = new t_DoubleAndULongUnion() { F8 = value };
        Write(_u8f8.U8);
      }
    }

    /// <summary>
    /// Gets or sets position within the stream.
    /// </summary>
    public long Position
    {
      [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
      get { return BaseStream.Position; }
      [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
      set { BaseStream.Position = value; }
    }

    /// <summary>
    /// Gets the length in bytes of the stream.
    /// </summary>
    public long Length
    {
      [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
      get { return BaseStream.Length; }
    }

    #endregion

    #region ### ASCII write methods ###

    // used in ASCII reading to avoid conversion to char when read as byte

    /// <summary>
    /// 0x20 - ' '
    /// </summary>
    const byte BYTE_SPACE = 0x20;
    /// <summary>
    /// 0x0D - '\r'
    /// </summary>
    const byte BYTE_CR = 0x0D;
    /// <summary>
    /// 0x0A - '\n'
    /// </summary>
    const byte BYTE_LF = 0x0A;
    /// <summary>
    /// 0x09 - '\t'
    /// </summary>
    const byte BYTE_TAB = 0x09;
    /// <summary>
    /// 0x23 - '#'
    /// </summary>
    const byte BYTE_SHARP = 0x23;
    /// <summary>
    /// 0x30 - '0'
    /// </summary>
    const byte BYTE_NUM_0 = 0x30;
    /// <summary>
    /// 0x31 - '1'
    /// </summary>
    const byte BYTE_NUM_1 = 0x31;
    /// <summary>
    /// 0x39 - '9'
    /// </summary>
    const byte BYTE_NUM_9 = 0x39;

    /// <summary>
    /// Writes LINE FEED ('\n', 10, x0A) character to the stream.
    /// </summary>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public int WriteLineEnd()
    {
      // TODO: LINE ENDINGS STANDARD 
      Write(new byte[] { BYTE_LF });
      return 1;
    }

    /// <summary>
    /// Writes SPACE (' ', 32, x20) character to the stream.
    /// </summary>
    /// <returns></returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public int WriteSpace()
    {
      Write(new byte[] { BYTE_SPACE });
      return 1;
    }

    /// <summary>
    /// Writes string as the ANSII stream of characters to the stream.
    /// </summary>
    /// <param name="ascii">A string to be written to the stream.</param>
    /// <returns>Returns the number of bytes written to the stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public int WriteText(string ascii)
    {
      byte[] asciiBuffer = Encoding.ASCII.GetBytes(ascii);
      Write(asciiBuffer);
      return asciiBuffer.Length;
    }

    /// <summary>
    /// Writes string as the ANSII stream of characters and line ending to the stream.
    /// </summary>
    /// <param name="ascii">A string to be written to the stream.</param>
    /// <returns>Returns the number of bytes written to the stream.</returns>
    public int WriteTextLine(string ascii)
    {
      int count = WriteText(ascii);
      return count + WriteLineEnd();
    }

    /// <summary>
    /// Writes ASCII comment string as following sequence of characters: [SHARP][comment][LINEEND] 
    /// <para><code>Example: If <paramref name="comment"/> value is 'some comment text', then written string is '# some comment text\n'.</code></para>
    /// </summary>
    /// <param name="comment">A string to be written as the comment to the stream.</param>
    /// <returns>Returns the number of bytes written to the stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public int WriteComment(string comment)
    {
      Write(new byte[] { BYTE_SHARP });
      return 1 + WriteTextLine(comment);
    }

    /// <summary>
    /// Writes ASCII string as following sequence of characters: [paramName][SPACE][paramValue][LINEEND].
    /// <para><code>Example: If <paramref name="paramName"/> value is 'MAXVAL', and <paramref name="paramValue"/> value is '255', then written string is 'MAXVAL 255\n'.</code></para>
    /// </summary>
    /// <param name="paramName">A first string expression to be written to the stream.</param>
    /// <param name="paramValue">A second string expression to be written to the stream.</param>
    /// <returns>Returns the number of bytes written to the stream.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public int WriteParamLine(string paramName, string paramValue)
    {
      int count = 0;
      count += WriteText(paramName);
      count += WriteSpace();
      count += WriteText(paramValue);
      count += WriteLineEnd();
      return count;
    }
  
    /// <summary>
    /// Writes header magic code.
    /// </summary>
    /// <param name="fileType"></param>
    /// <returns>Returns write status.</returns>
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    public EPmWriteStatus WriteFileType(EPmFileType fileType)
    {
      EPmWriteStatus ok = EPmWriteStatus.OK;
      switch (fileType)
      {
        case EPmFileType.P1_BITMAP_ASCII:
          WriteTextLine("P1");
          return ok;
        case EPmFileType.P2_GRAYMAP_ASCII:
          WriteTextLine("P2");
          return ok;
        case EPmFileType.P3_PIXMAP_ASCII:
          WriteTextLine("P3");
          return ok;
        case EPmFileType.P4_BITMAP_BINARY:
          WriteTextLine("P4");
          return ok;
        case EPmFileType.P5_GRAYMAP_BINARY:
          WriteTextLine("P5");
          return ok;
        case EPmFileType.P6_PIXMAP_BINARY:
          WriteTextLine("P6");
          return ok;
        case EPmFileType.P7_ARBITRARY_MAP_BINARY:
          WriteTextLine("P7");
          return ok;
        case EPmFileType.PF_FLOATMAP_GRAY_BINARY:
          WriteTextLine("Pf");
          return ok;
        case EPmFileType.PF_FLOATMAP_PIX_BINARY:
          WriteTextLine("PF");
          return ok;
        case EPmFileType.PF4_FLOATMAP_GRAY_ALPHA_BINARY:
          WriteTextLine("Pf4");
          return ok;
        case EPmFileType.PF4_FLOATMAP_PIX_ALPHA_BINARY:
          WriteTextLine("PF4");
          return ok;
        case EPmFileType.UNKNOWN:
        default:
          return EPmWriteStatus.ERROR_UNKNOWN_FILE_TYPE;
      }
    }

    /// <summary>
    /// Writes <paramref name="number"/> as ASCII expression.
    /// </summary>
    /// <param name="number">A number to be written.</param>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public void WriteAsciiNumber(ulong number)
    {
      WriteText(number.ToString());
    }

    /// <summary>
    /// Writes <paramref name="number"/> as ASCII expression.
    /// </summary>
    /// <param name="number">A number to be written.</param>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public void WriteAsciiNumberFloat32(float number)
    {
      WriteText(number.ToString(CultureInfo.InvariantCulture));
    }

    /// <summary>
    /// Writes <paramref name="number"/> as ASCII expression.
    /// </summary>
    /// <param name="number">A number to be written.</param>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public void WriteAsciiNumber(UInt48 number)
    {
      WriteText(number.ToString());
    }

    /// <summary>
    /// Writes <paramref name="number"/> as ASCII expression.
    /// </summary>
    /// <param name="number">A number to be written.</param>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public void WriteAsciiNumber(uint number)
    {
      WriteText(number.ToString());
    }

    /// <summary>
    /// Writes <paramref name="number"/> as ASCII expression.
    /// </summary>
    /// <param name="number">A number to be written.</param>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public void WriteAsciiNumber(ushort number)
    {
      WriteText(number.ToString());
    }

    /// <summary>
    /// Writes <paramref name="number"/> as ASCII expression.
    /// </summary>
    /// <param name="number">A number to be written.</param>    
    [DebuggerHidden()]
    [MethodImpl(256)]
    public void WriteAsciiNumber(byte number)
    {
      WriteText(number.ToString());
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as <see cref="ulong"/> type.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">A output array of elements, represented as byte array of <see cref="ulong"/> elements.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public void WriteAsciiUInt64Array(byte[] byteArray, int elementsCount)
    {
      fixed (byte* pByteArray = byteArray)
      {
        var position = BaseStream.Position;
        var pUInt64Array = (ulong*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          WriteAsciiNumber(pUInt64Array[i]);
          Write(BYTE_SPACE);
          if (BaseStream.Position - position >= 76)
          {
            WriteLineEnd();
            position = BaseStream.Position;
          }
        }
      }
    }

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as <see cref="ulong"/> type.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">A output array of elements, represented as byte array of <see cref="ulong"/> elements.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public void WriteAsciiFloat32Array(byte[] byteArray, int elementsCount)
    {

      fixed (byte* pByteArray = byteArray)
      {
        var position = BaseStream.Position;
        var pFloat32Array = (float*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          WriteAsciiNumberFloat32(pFloat32Array[i]);
          Write(BYTE_SPACE);
          if (BaseStream.Position - position >= 76)
          {
            WriteLineEnd();
            position = BaseStream.Position;
          }
        }
      }
    }

    //const uint ASCII_MAX_CHARS_PER_LINE = 76;

    /// <summary>
    /// Writes to output stream <paramref name="elementsCount"/> of ASCII numbers from <paramref name="byteArray"/> represented as byte array of <see cref="UInt48"/> size elements.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be written.</param>
    /// <param name="byteArray">An input array of elements, represented as byte array of <see cref="UInt48"/> size elements.</param>
    /// <param name="options">An ASCII write options.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public void WriteAsciiUInt48Array(byte[] byteArray, int elementsCount, PmWriterAsciiOptions options = null)
    {
      if (null == options)
      {
        options = new PmWriterAsciiOptions();
      }
      fixed (byte* pByteArray = byteArray)
      {
        int iCharsWritten = 0;
        int iPixelsWritten = 0;
        var p = (UInt48*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          string text = p[i].ToString();
          iCharsWritten += WriteText(text);
          iPixelsWritten++;
          if (0 != options.WidthBreak && iPixelsWritten >= options.WidthBreak)
          {
            Write(BYTE_LF);
            iPixelsWritten = 0;
            iCharsWritten = 0;
          }
          else if (0 != options.LineBreak && iCharsWritten >= options.LineBreak)
          {
            Write(BYTE_LF);
            iCharsWritten = 0;
          }
          else
          {
            Write(BYTE_SPACE);
            iCharsWritten++;
          }
        }
      }
    }

    /// <summary>
    /// Writes to output stream <paramref name="elementsCount"/> of ASCII numbers from <paramref name="byteArray"/> represented as byte array of <see cref="uint"/> size elements.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be written.</param>
    /// <param name="byteArray">An input array of elements, represented as byte array of <see cref="uint"/> size elements.</param>
    /// <param name="options">An ASCII write options.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public void WriteAsciiUInt32Array(byte[] byteArray, int elementsCount, PmWriterAsciiOptions options = null)
    {
      if (null == options)
      {
        options = new PmWriterAsciiOptions();
      }
      fixed (byte* pByteArray = byteArray)
      {
        int iCharsWritten = 0;
        int iPixelsWritten = 0;
        var p = (uint*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          string text = p[i].ToString();
          iCharsWritten += WriteText(text);
          iPixelsWritten++;
          if (0 != options.WidthBreak && iPixelsWritten >= options.WidthBreak)
          {
            Write(BYTE_LF);
            iPixelsWritten = 0;
            iCharsWritten = 0;
          }
          else if (0 != options.LineBreak && iCharsWritten >= options.LineBreak)
          {
            Write(BYTE_LF);
            iCharsWritten = 0;
          }
          else
          {
            Write(BYTE_SPACE);
            iCharsWritten++;
          }
        }
      }
    }

    /// <summary>
    /// Writes to output stream <paramref name="elementsCount"/> of ASCII numbers from <paramref name="byteArray"/> represented as byte array of <see cref="ushort"/> size elements.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be written.</param>
    /// <param name="byteArray">An input array of elements, represented as byte array of <see cref="ushort"/> size elements.</param>
    /// <param name="options">An ASCII write options.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public void WriteAsciiUInt16Array(byte[] byteArray, int elementsCount, PmWriterAsciiOptions options = null)
    {
      if (null == options)
      {
        options = new PmWriterAsciiOptions();
      }
      fixed (byte* pByteArray = byteArray)
      {
        int iCharsWritten = 0;
        int iPixelsWritten = 0;
        var p = (ushort*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          string text = p[i].ToString();
          iCharsWritten += WriteText(text);
          iPixelsWritten++;
          if (0 != options.WidthBreak && iPixelsWritten >= options.WidthBreak)
          {
            Write(BYTE_LF);
            iPixelsWritten = 0;
            iCharsWritten = 0;
          }
          else if (0 != options.LineBreak && iCharsWritten >= options.LineBreak)
          {
            Write(BYTE_LF);
            iCharsWritten = 0;
          }
          else
          {
            Write(BYTE_SPACE);
            iCharsWritten++;
          }
        }
      }
    }

    /// <summary>
    /// Writes to output stream <paramref name="elementsCount"/> of ASCII numbers from <paramref name="byteArray"/> represented as byte array of <see cref="byte"/> size elements.
    /// </summary>
    /// <param name="elementsCount">A number of elements to be read.</param>
    /// <param name="byteArray">An input array of elements, represented as byte array of <see cref="byte"/> size elements.</param>
    /// <param name="options">An ASCII write options.</param>
    /// <returns>Returns true if operation was successful; otherwise false.</returns>
    [MethodImpl(256)]
    public void WriteAsciiByteArray(byte[] byteArray, int elementsCount, PmWriterAsciiOptions options = null)
    {
      if (null == options)
      {
        options = new PmWriterAsciiOptions();
      }
      fixed (byte* pByteArray = byteArray)
      {
        int iCharsWritten = 0;
        int iPixelsWritten = 0;
        var p = (byte*)pByteArray;
        for (int i = 0; i < elementsCount; i++)
        {
          string text = p[i].ToString();
          iCharsWritten += WriteText(text);
          iPixelsWritten++;
          if (0 != options.WidthBreak && iPixelsWritten >= options.WidthBreak)
          {
            Write(BYTE_LF);
            iPixelsWritten = 0;
            iCharsWritten = 0;
          }
          else if (0 != options.LineBreak && iCharsWritten >= options.LineBreak)
          {
            Write(BYTE_LF);
            iCharsWritten = 0;
          }
          else
          {
            Write(BYTE_SPACE);
            iCharsWritten++;
          }
        }
      }
    }


#if !TODO_BITS_WRITE
    public void WriteAsciiBitsArray(PmImage image, PmWriterAsciiOptions options = null)
    {
      if (null == options)
      {
        options = new PmWriterAsciiOptions();
      }
      byte[] byteArray = image.ImagePixelData;
      int elementsCount = image.GetElementsCount();
      // NOTE: WIC higher bit is first pixel (0x80)
      // NOTE: black is 1, and white 0 : must reverse the values
      int iPixelsWritten = 0;
      int iCharsWritten = 0;
      int numberOfBytes = elementsCount >> 3;
      int reminderBits = ((elementsCount + 7) & unchecked((int)0xfffffff8)) - elementsCount;
      for (int posByte = 0; posByte < numberOfBytes; posByte++)
      {
        for(int posBit = 7; posBit >= 0; posBit--)
        {
          Write(0 != (byteArray[posByte] & (1 << posBit)) ? BYTE_NUM_0 : BYTE_NUM_1);
          iPixelsWritten++;
          iCharsWritten++;
          if (iPixelsWritten == image.ImageWidth)
          {
            // no more bits to write END of SCAN line
            posBit = 0;
          }
          if (0 != options.WidthBreak && iPixelsWritten == options.WidthBreak)
          {
            Write(BYTE_LF);
            iPixelsWritten = 0;
            iCharsWritten = 0;
          }
          else if (0 != options.LineBreak && iCharsWritten == options.LineBreak)
          {
            Write(BYTE_LF);
            iCharsWritten = 0;
          }
          else if (options.WriteSeparator)
          {
            Write(BYTE_SPACE);
            iCharsWritten++;
          }
        }
      }
      if (reminderBits > 0)
      {
        int posByte = numberOfBytes;
        for (int posBit = 7; posBit >= (8 - reminderBits); posBit--)
        {
          Write(0 != (byteArray[posByte] & (1 << posBit)) ? BYTE_NUM_1 : BYTE_NUM_0);
          iPixelsWritten++;
          iCharsWritten++;
          if (0 != options.WidthBreak && iPixelsWritten == options.WidthBreak)
          {
            Write(BYTE_LF);
            iPixelsWritten = 0;
            iCharsWritten = 0;
          }
          else if (0 != options.LineBreak && iCharsWritten == options.LineBreak)
          {
            Write(BYTE_LF);
            iCharsWritten = 0;
          }
          else if (options.WriteSeparator)
          {
            Write(BYTE_SPACE);
            iCharsWritten++;
          }
        }
      }
    }
#else

    /// <summary>
    /// Reads from input stream <paramref name="elementsCount"/> of ASCII numbers as bit type, and stores them in byte array.
    /// Skips any non-numeric characters (if any), reads numeric characters until first non-numeric.
    /// And returns converted string numeric expression as array of bytes bits.
    /// </summary>
    /// <returns>Returns true if array is read; Otherwise false.</returns>
    [DebuggerHidden()]
    [MethodImpl(256)]
    public bool ReadAsciiBitsArray(int elementsCount, out byte[] byteArray)
    {
      // maybe too complicated !?
      int bitReminderShift = 7;
      int bytePosition = 0;
      int bufferSize = (elementsCount + 7) >> 3;
      byteArray = new byte[bufferSize];
      if (Position >= Length)
      {      
        return false;
      }
      // TODO: enhance bit reading to ulong value and 64 bits read before commit
      byte b;
      byte value = 0;
      while (Position < Length)
      {
        b = ReadByte();
        if (b == 48 && b == 49) // '0' or '1'
        {
          value = (byte)(value | ((b == 48 ? 0 : 1) << bitReminderShift));
          if (bitReminderShift == 0)
          {
            byteArray[bytePosition] = value;
            bytePosition++;
            bitReminderShift = 8;
          }
          bitReminderShift--;
        }
        else if (b == BYTE_SHARP)
        {
          // skip comments
          SkipAsciiComment();
        }
      }
      // commit last value
      if (bitReminderShift < 7)
      {
        byteArray[bytePosition] = value;
      }

      // success
      return true;
    }
#endif //TODO_BITS_WRITE

    #endregion

    #region ### Binary write methods ###

    #region ### Swap Endianness Fast ###

    /* 
     * test run on 100000 * 8Kb byte[] buffers
    HPC[1001]::ShowStatistic() called from : [JITTrack,E&C] JpegLSTest.Program::TestEndinnessSwapping()
    ┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    │ meter       : sumtime  /    cnt ~ average  | times * time.min ~ time.max : max-min  ( max-avrg , avrg-min ) over time improving  │
    ├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
    │ SwapU2Bytes : 0.757011 / 100000 ~ 0.000008 |   455 * 0.000007 ~ 0.003110 : 0.003103 ( 0.003103 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapU4Bytes : 0.749213 / 100000 ~ 0.000007 | 1 178 * 0.000007 ~ 0.008045 : 0.008039 ( 0.008038 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapU8Bytes : 0.749137 / 100000 ~ 0.000007 |   857 * 0.000007 ~ 0.005851 : 0.005844 ( 0.005843 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapX2      : 1.216467 / 100000 ~ 0.000012 |   353 * 0.000011 ~ 0.003947 : 0.003936 ( 0.003935 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapX4      : 0.808326 / 100000 ~ 0.000008 |   126 * 0.000007 ~ 0.000941 : 0.000933 ( 0.000933 , 0.000001 ) ■■■■■■■■■■■■■■■■■■■■ │
    └──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    HPC[1001]::ShowStatistic() called from : [JITOpt] JpegLSTest.Program::TestEndinnessSwapping()
    ┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
    │ meter       : sumtime  /    cnt ~ average  | times * time.min ~ time.max : max-min  ( max-avrg , avrg-min ) over time improving  │
    ├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
    │ SwapU2Bytes : 0.274493 / 100000 ~ 0.000003 |   637 * 0.000002 ~ 0.001582 : 0.001579 ( 0.001579 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapU4Bytes : 0.272241 / 100000 ~ 0.000003 |   355 * 0.000002 ~ 0.000883 : 0.000881 ( 0.000881 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapU8Bytes : 0.325498 / 100000 ~ 0.000003 |   315 * 0.000003 ~ 0.000880 : 0.000877 ( 0.000876 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapX2      : 0.285102 / 100000 ~ 0.000003 | 3 516 * 0.000002 ~ 0.008729 : 0.008726 ( 0.008726 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    │ SwapX4      : 0.273668 / 100000 ~ 0.000003 |   412 * 0.000002 ~ 0.001024 : 0.001021 ( 0.001021 , 0.000000 ) ■■■■■■■■■■■■■■■■■■■■ │
    └──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
    Note: these variants clearly win DEBUG condition due var length optimization, and match RELEASE speed optimization.
    Note: MethodImpl plays no role in tests, but left it on purpose if someone wants to play with it.
    */

    /// <summary>
    /// Swaps INT16 or UINT16 bytes in <paramref name="bytes"/> stream.
    /// Optimized with DebuggerNonUserCode and inlining.
    /// </summary>
    /// <param name="bytes">The stream of INT16 or UINT16 bytes to be swapped.</param>
    [DebuggerNonUserCode]
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    static unsafe void SwapU2Bytes(byte[] bytes)
    {
      unchecked
      {
        fixed (byte* lp = bytes)
        {
          byte* p = lp;
          byte* pend = lp + bytes.Length - (bytes.Length % 2);
          // swap up to reminder
          byte* pend2rem = pend - (bytes.Length % 8);
          while (p < pend2rem)
          {
            *(ulong*)p = (
                ((uint)(*p) << 8) |
                (*(p + 1)) |
                ((uint)(*(p + 2)) << 24) |
                ((uint)(*(p + 3)) << 16) |
                ((ulong)(*(p + 4)) << 40) |
                ((ulong)(*(p + 5)) << 32) |
                ((ulong)(*(p + 6)) << 56) |
                ((ulong)(*(p + 7)) << 48)
                );
            p += 8;
          }
          // swap reminder
          while (p < pend)
          {
            *(ushort*)p = (ushort)(
                (*p << 8) |
                (*(p + 1)));
            p += 2;
          }
        }
      }
    }

    /// <summary>
    /// Swaps INT32 or UINT32 bytes in <paramref name="bytes"/> stream.
    /// Optimized with DebuggerNonUserCode and inlining.
    /// </summary>
    /// <param name="bytes">The stream of INT32 or UINT32 bytes to be swapped.</param>
    [DebuggerNonUserCode]
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    static unsafe void SwapU4Bytes(byte[] bytes)
    {
      unchecked
      {
        fixed (byte* lp = bytes)
        {
          byte* p = lp;
          byte* pend = lp + bytes.Length - (bytes.Length % 4);

          // swap up to reminder
          byte* pend2rem = pend - (bytes.Length % 8);
          while (p < pend2rem)
          {
            *(ulong*)p = (
                ((uint)(*p) << 24) |
                ((uint)(*(p + 1)) << 16) |
                ((uint)(*(p + 2)) << 8) |
                (*(p + 3)) |
                ((ulong)(*(p + 4)) << 56) |
                ((ulong)(*(p + 5)) << 48) |
                ((ulong)(*(p + 6)) << 40) |
                ((ulong)(*(p + 7)) << 32)
                );
            p += 8;
          }
          // swap reminder

          while (p < pend)
          {
            *(uint*)p = (uint)(
                (*p << 24) |
                (*(p + 1) << 16) |
                (*(p + 2) << 8) |
                (*(p + 3))
                );
            p += 4;
          }
        }
      }
    }

    /// <summary>
    /// Swaps INT64 or UINT64 bytes in <paramref name="bytes"/> stream.
    /// Optimized with DebuggerNonUserCode and inlining.
    /// </summary>
    /// <param name="bytes">The stream of INT64 or UINT64 bytes to be swapped.</param>
    [DebuggerNonUserCode]
    [MethodImpl(256 /*MethodImplOptions.AggressiveInlining*/)]
    static unsafe void SwapU8Bytes(byte[] bytes)
    {
      fixed (byte* lp = bytes)
      {
        byte* p = lp;
        byte* pend = p + bytes.Length - (bytes.Length % 8);
        while (p < pend)
        {
          *(ulong*)p = (
              (*(p + 7))
               | ((uint)(*(p + 6)) << 8)
               | ((uint)(*(p + 4)) << 24)
               | ((uint)(*(p + 5)) << 16)
               | ((ulong)(*(p + 3)) << 32)
               | ((ulong)(*(p + 2)) << 40)
               | ((ulong)(*(p + 1)) << 48)
               | ((ulong)(*(p)) << 56)
               );
          p += 8;
        }
      }
    }

    #endregion

    /// <summary>
    /// Writes array of <paramref name="bytes"/> in writer endian of specified <paramref name="elementsSize"/> in bytes.
    /// </summary>
    /// <param name="bytes">An array of bytes to be written.</param>
    /// <param name="elementsSize">An element size in bytes for endian swapping.</param>
    public unsafe void WriteBytes(byte[] bytes, int elementsSize)
    {
      if (HWIsLittleEndian != WriteLittleEndian)
      {
        // ■ Note: we could have chosen to write with endian swapped,
        //         but this is 20 or more times faster.
        switch (elementsSize)
        {
          case 1:
            // do nothing
            break;
          case 2:
            // must swap UINT16
            SwapU2Bytes(bytes);
            break;
          case 4:
            // must swap UINT32
            SwapU4Bytes(bytes);
            break;
          case 8:
            // must swap UINT64
            SwapU8Bytes(bytes);
            break;
        }
      }
      base.Write(bytes);
    }

    #endregion
  }
}
