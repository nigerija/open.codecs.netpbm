﻿namespace open.codecs.netpbm
{
  /// <summary>
  /// Portable Map image container read status
  /// </summary>
  public enum EPmReadStatus
  {
    /// <summary>
    /// Success
    /// </summary>
    OK = 0,

    /// <summary>
    /// Reader base error
    /// </summary>
    ERROR_BASE = 0x8000,
    /// <summary>
    /// Reader error : not implemented
    /// </summary>
    ERROR_NOT_SUPPORTED = ERROR_BASE + 1,

    /// <summary>
    /// Reader error : file not found
    /// </summary>
    ERROR_FILE_NOT_FOUND = ERROR_BASE + 2,
    /// <summary>
    /// Reader error : premature end of file (EOF)
    /// </summary>
    ERROR_EOF = ERROR_BASE + 3,
    /// <summary>
    /// Reader error : file header type (unsupported file type).
    /// </summary>
    ERROR_FILE_TYPE = ERROR_BASE + 4,


    /// <summary>
    /// Reader error : reading image width value
    /// </summary>
    ERROR_READING_IMAGE_WIDTH = ERROR_BASE + 11,
    /// <summary>
    /// Reader error : reading image height value
    /// </summary>
    ERROR_READING_IMAGE_HEIGHT = ERROR_BASE + 12,
    /// <summary>
    /// Reader error : reading image MAXVAL value
    /// </summary>
    ERROR_READING_IMAGE_MAXVAL = ERROR_BASE + 13,
    /// <summary>
    /// Reader error : reading image SIZE value
    /// </summary>
    ERROR_READING_IMAGE_SIZE = ERROR_BASE + 14,
    /// <summary>
    /// Reader error : reading image DEPTH value
    /// </summary>
    ERROR_READING_IMAGE_DEPTH = ERROR_BASE + 15,
    /// <summary>
    /// Reader error : reading image TUPLTYPE value
    /// </summary>
    ERROR_READING_IMAGE_TUPLTYPE = ERROR_BASE + 16,

    /// <summary>
    /// Reader error : parsing image width value
    /// </summary>
    ERROR_PARSING_IMAGE_WIDTH = ERROR_BASE + 21,
    /// <summary>
    /// Reader error : parsing image height value
    /// </summary>
    ERROR_PARSING_IMAGE_HEIGHT = ERROR_BASE + 22,
    /// <summary>
    /// Reader error : parsing image MAXVAL value
    /// </summary>
    ERROR_PARSING_IMAGE_MAXVAL = ERROR_BASE + 23,
    /// <summary>
    /// Reader error : parsing image SIZE value
    /// </summary>
    ERROR_PARSING_IMAGE_SIZE = ERROR_BASE + 24,
    /// <summary>
    /// Reader error : parsing image DEPTH value
    /// </summary>
    ERROR_PARSING_IMAGE_DEPTH = ERROR_BASE + 25,
    /// <summary>
    /// Reader error : parsing image TUPLTYPE value
    /// </summary>
    ERROR_PARSING_IMAGE_TUPLTYPE = ERROR_BASE + 26,

    /// <summary>
    /// Reader error : invalid image width value
    /// </summary>
    ERROR_INVALID_IMAGE_WIDTH = ERROR_BASE + 101,
    /// <summary>
    /// Reader error : invalid image height value
    /// </summary>
    ERROR_INVALID_IMAGE_HEIGHT = ERROR_BASE + 102,
    /// <summary>
    /// Reader error : invalid image maximum value
    /// </summary>
    ERROR_INVALID_IMAGE_MAXVAL = ERROR_BASE + 103,
    /// <summary>
    /// Reader error : unable to calculate BINARY Pixel Size
    /// </summary>
    ERROR_INVALID_BINARY_PIXEL_SIZE = ERROR_BASE + 104,
    /// <summary>
    /// Reader error : invalid image depth value
    /// </summary>
    ERROR_INVALID_IMAGE_DEPTH = ERROR_BASE + 105,
    /// <summary>
    /// Reader error : invalid PAM image TUPLTYPE value
    /// </summary>
    ERROR_INVALID_IMAGE_TUPLTYPE = ERROR_BASE + 106,
    /// <summary>
    /// Reader error : invalid sequence of properties in PAM file
    /// </summary>
    ERROR_INVALID_SEQUENCE = ERROR_BASE + 107,
  }
}
