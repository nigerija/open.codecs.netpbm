﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm.wic
{
  //
  // NOTE: reconstructed enum : System.Windows.Media.PixelFormatFlags
  //       and added missing flags for n-channel formats and storage types
  //       this should help to better identify rare WIC pixel formats.
  //

  /// <summary>
  /// Flags used by <see cref="System.Windows.Media.PixelFormat"/> structure to additionally describe the pixel format.
  /// <para>NOTE: Reconstructed enum from internal <b>System.Windows.Media.PixelFormatFlags</b> and added additional flags to fully describe additional WIC pixel formats seen in <see cref="WicPixelFormats"/>.</para>
  /// </summary>
  [Flags]
  public enum EWicPixelFormatFlags : uint
  {
    #region ### bits per pixel ###

    /// <summary>
    /// pixel format is unspecified or we do not care (WIC terminology).
    /// <para>NOTE: if flags AND <see cref="MASK_BitsPerPixel"/> returns zero it's this value; Otherwise, if flags is 0 entire format is undefined.</para>
    /// </summary>
    BitsPerPixelUndefined = 0,

    /// <summary>
    /// BitsPerPixel flags mask
    /// </summary>
    MASK_BitsPerPixel = 0x000000FF,

    /// <summary>
    /// 1 bit per pixel
    /// </summary>
    BitsPerPixel1 = 0x01,
    /// <summary>
    /// 2 bits per pixel
    /// </summary>
    BitsPerPixel2 = 0x02,
    /// <summary>
    /// 4 bits per pixel
    /// </summary>
    BitsPerPixel4 = 0x04,
    /// <summary>
    /// 8 bits per pixel
    /// </summary>
    BitsPerPixel8 = 0x08,
    /// <summary>
    /// 16 bits per pixel
    /// </summary>
    BitsPerPixel16 = 0x10,
    /// <summary>
    /// 24 bits per pixel
    /// </summary>
    BitsPerPixel24 = 0x18,
    /// <summary>
    /// 32 bits per pixel
    /// </summary>
    BitsPerPixel32 = 0x20,
    /// <summary>
    /// 40 bits per pixel
    /// </summary>
    BitsPerPixel40 = 0x28,
    /// <summary>
    /// 48 bits per pixel
    /// </summary>
    BitsPerPixel48 = 0x30,
    /// <summary>
    /// 56 bits per pixel
    /// </summary>    
    BitsPerPixel56 = 0x38,
    /// <summary>
    /// 64 bits per pixel
    /// </summary>
    BitsPerPixel64 = 0x40,
    /// <summary>
    /// 72 bits per pixel
    /// </summary>
    BitsPerPixel72 = 0x48,
    /// <summary>
    /// 80 bits per pixel
    /// </summary>
    BitsPerPixel80 = 0x50,
    /// <summary>
    /// 96 bits per pixel
    /// </summary>
    BitsPerPixel96 = 0x60,
    /// <summary>
    /// 112 bits per pixel
    /// </summary>
    BitsPerPixel112 = 0x70,
    /// <summary>
    /// 128 bits per pixel
    /// </summary>
    BitsPerPixel128 = 0x80,
    /// <summary>
    /// 144 bits per pixel
    /// </summary>
    BitsPerPixel144 = 0x90,

    #endregion

    #region ### is or premultiplied ###

    /// <summary>
    /// Is* and Premultiplied flags mask
    /// </summary>
    MASK_Is = 0x0000FF00,

    /// <summary>
    /// Grayscale only
    /// </summary>
    IsGray = 0x00000100,
    /// <summary>
    /// Are CMYK channels.
    /// <para>NOTE: not RGBA, BGRA, ARGB, or ABGR channels, nor any other with premultiplied alpha.</para>
    /// </summary>
    IsCMYK = 0x00000200,
    /// <summary>
    /// Standard SRGB
    /// <para>Gamma is approximately 2.2</para>
    /// </summary>
    IsSRGB = 0x00000400,
    /// <summary>
    /// MS extended SRGB
    /// <para>Gamma is 1.0</para>
    /// </summary>
    IsScRGB = 0x00000800,
    /// <summary>
    /// Premultiplied Alpha
    /// </summary>
    IsPremultiplied = 0x00001000,

    #endregion

    #region ## channel order / palettized / is... ###

    /// <summary>
    /// ChannelOrder flags mask
    /// </summary>
    MASK_ChannelOrder = 0x0001E000,

    /// <summary>
    /// [R,G,B] order of channels
    /// </summary>
    ChannelOrderRGB = 0x00002000,

    /// <summary>
    /// [B,G,R] order of channels
    /// </summary>
    ChannelOrderBGR = 0x00004000,

    /// <summary>
    /// [R,G,B,A] order of channels
    /// <para>NOTE: WIC documentation explicitly said Alpha channel comes last, moving letter 'a' to last position not to have confusion.</para>
    /// </summary>
    ChannelOrderRGBA = 0x00008000,

    /// <summary>
    /// [B,G,R,A] order of channels
    /// <para>NOTE: WIC documentation explicitly said Alpha channel comes last, moving letter 'a' to last position not to have confusion.</para>
    /// </summary>
    ChannelOrderBGRA = 0x00010000,

    /// <summary>
    /// Pixels are indexes into a palette
    /// </summary>
    Palettized = 0x00020000,

    /// <summary>
    /// N-Channel format with alpha 
    /// <para>NOTE: Requires <see cref="IsNChannel"/> to be set also.</para>
    /// <para>Added composite flag <see cref="IsNChannelAlpha"/> of these two, when we need to specify the format. </para>
    /// <para>But also leaving this <see cref="NChannelAlpha"/> flag, when we need to inspect if format has alpha channel.</para>
    /// </summary>
    NChannelAlpha = 0x00040000,

    /// <summary>
    /// N-Channel format
    /// </summary>
    IsNChannel = 0x00080000,

    // non-existing combination
    /// <summary>
    /// Composite of two flags : <see cref="IsNChannel"/> | <see cref="NChannelAlpha"/>.
    /// </summary>
    IsNChannelAlpha = IsNChannel | NChannelAlpha,

    #endregion

    #region ### storage fixed point/float ###

    /// <summary>
    /// Storage flags mask
    /// </summary>
    MASK_STORAGE = 0xFF000000,

    /// <summary>
    /// Extension: marks PixelFormat to be a channel value of Fixed Point (8-bit: s2.13, or 16-bit:s7.24)
    /// </summary>
    StorageFixedPoint = 0x01000000,
    /// <summary>
    /// Extension: marks PixelFormat to be a channel value of Float Point (16-bit half, 32-bit float)
    /// </summary>
    StorageFloat = 0x02000000,

    #endregion
  }
}
