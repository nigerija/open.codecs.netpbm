﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm.wic
{

  // TODO : ... just got lost for the moment thinking to implement WIC 100% which I don't need.
  //        ... so just something to think about until someone requests support for it...
  //
  // NOTE: absolutely not tested, not even sure if values are correct much less profiled for speed
  //       against version with 2nd complement calculations.
  //
  // NOTE: also not validated for endian...
  //

  /// <summary>
  /// Fixed Point 16-bit structure
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 2)]
  public struct T_FixedPoint16
  {
    /// <summary>
    /// A uint representation of fixed point value.
    /// </summary>
    public ushort Value;

    /// <summary>
    /// Sign negative bit mask
    /// </summary>
    public const ushort SIGN_NEGATIVE = 0x8000;
    /// <summary>
    /// Integer part bit mask
    /// </summary>
    public const ushort INTEGER_MASK = 0x6000;
    /// <summary>
    /// Integer part number of bits to shift.
    /// </summary>
    public const int INTEGER_SHIFT = 13;
    /// <summary>
    /// Fraction bits
    /// </summary>
    public const ushort FRACTION_BITS = 0x1FFF;


    /// <summary>
    /// Implicit conversion from Fixed Point to float.
    /// </summary>
    /// <param name="r">A fixed point value to be converted.</param>
    /// <returns>
    /// A single-precision floating-point number equivalent to the value T_FixedPoint16 r.
    /// </returns>
    [MethodImpl(256)]
    public static implicit operator float(T_FixedPoint16 r)
    {
      return ((r.Value & SIGN_NEGATIVE) != 0)
        // negative
        ? -((r.Value & INTEGER_MASK) >> INTEGER_SHIFT) - ((float)(r.Value & FRACTION_BITS) / FRACTION_BITS)
        // positive
        : ((r.Value & INTEGER_MASK) >> INTEGER_SHIFT) + ((float)(r.Value & FRACTION_BITS) / FRACTION_BITS)
        ;
    }

    /// <summary>
    /// Implicit conversion from float to Fixed point
    /// </summary>
    /// <param name="r">A float value to be converted.</param>
    /// <returns>
    /// A FixedPoint16 number equivalent to the value float r.
    /// </returns>
    [MethodImpl(256)]
    public static implicit operator T_FixedPoint16(float r)
    {
      byte ir = (byte)r;
      float f = r - ir;
      return new T_FixedPoint16()
      {
        Value = (ushort)((r < 0f)
        ? (SIGN_NEGATIVE) | (ushort)(-ir << INTEGER_SHIFT) & INTEGER_MASK | ((ushort)(-f * FRACTION_BITS) & FRACTION_BITS)
        : (ushort)(ir << INTEGER_SHIFT) & INTEGER_MASK | ((ushort)(f * FRACTION_BITS) & FRACTION_BITS))
      };
    }

  }

  /// <summary>
  /// Fixed Point 32-bit structure
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 4)]
  public struct T_FixedPoint32
  {
    /// <summary>
    /// A uint representation of fixed point value.
    /// </summary>
    public uint Value;

    /// <summary>
    /// Sign negative bit mask
    /// </summary>
    public const uint SIGN_NEGATIVE = 0x80000000;
    /// <summary>
    /// Integer part bit mask
    /// </summary>
    public const uint INTEGER_MASK = 0x7F000000; // 7
    /// <summary>
    /// Integer part number of bits to shift.
    /// </summary>
    public const int INTEGER_SHIFT = 24;
    /// <summary>
    /// Fraction bits
    /// </summary>
    public const uint FRACTION_BITS = 0x00FFFFFF; // 24


    /// <summary>
    /// Implicit conversion from Fixed Point to float.
    /// </summary>
    /// <param name="r">A fixed point value to be converted.</param>
    /// <returns>
    /// A single-precision floating-point number equivalent to the value T_FixedPoint32 r.
    /// </returns>
    [MethodImpl(256)]
    public static implicit operator float(T_FixedPoint32 r)
    {
      return ((r.Value & SIGN_NEGATIVE) != 0)
        // negative
        ? -((r.Value & INTEGER_MASK) >> INTEGER_SHIFT) - ((float)(r.Value & FRACTION_BITS) / FRACTION_BITS)
        // positive
        : ((r.Value & INTEGER_MASK) >> INTEGER_SHIFT) + ((float)(r.Value & FRACTION_BITS) / FRACTION_BITS)
        ;
    }

    /// <summary>
    /// Implicit conversion from float to Fixed point
    /// </summary>
    /// <param name="r">A float value to be converted.</param>
    /// <returns>
    /// A FixedPoint32 number equivalent to the value float r.
    /// </returns>
    [MethodImpl(256)]
    public static implicit operator T_FixedPoint32(float r)
    {
      int ir = (int)r;
      return new T_FixedPoint32()
      {
        Value = (uint)((r < 0f)
        ? (SIGN_NEGATIVE) | (uint)(-ir << INTEGER_SHIFT) & INTEGER_MASK | (-((uint)(r - ir) * FRACTION_BITS) & FRACTION_BITS)
        : (uint)(ir << INTEGER_SHIFT) & INTEGER_MASK | (((uint)(r - ir) * FRACTION_BITS) & FRACTION_BITS))
      };
    }

  }
}
