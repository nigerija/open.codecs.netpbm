﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Runtime.CompilerServices;

namespace open.codecs.netpbm.wic
{
  
  using static WicPixelFormatGUIDs;
  using static EWicPixelFormatFlags;

  /// <summary>
  /// PixelFormats mapping to Windows Imaging Components (WIC) GUID's not exposed by .NET Framework System.Windows.Media.PixelFormats class.
  /// <para>This class 'hacks' internals of <see cref="PixelFormat"/> structure to return all WIC native pixel formats.</para>
  /// </summary>
  public static class WicPixelFormats
  {
    // NOTE: put simply .NET lacks the knowledge all of what WIC actually can do.
    //       best guess why is this done like this is to remove use of rare pixel formats!?
    //       or to support windows 7sp1 that actually do not support all WIC pixel formats!?
    //       never the less this is the HACK, to force .NET to accept such images and allow conversions.

    // NOTE: Native Codec JPEG-XR actually supports most of this formats in decode/encode process,
    //       while other codecs do not!!! when saving such images WIC has to convert to their supported formats.
    //       check: https://docs.microsoft.com/en-us/windows/win32/wic/-wic-codec-native-pixel-formats
    //       In tests I've performed this conversion is actually performed by default by WIC encoding process.
    //       So do not assume images are what you think you have sent to encoder when saving them.
    //       This behavior is even beyond .NET Framework, only thing we could do is to document such cases.
    //
    
    // references to internal of PixelFormat members
    // I know this is bad practice, but .NET Framework is frozen at this time, so this should be final definition on internals.
    // otherwise we would need to reinvent the wheel of WIC in .NET Framework (Would you? For real?).

    private static readonly Type s_TPixelFormat = typeof(PixelFormat);
    private static readonly ConstructorInfo s_Constructor_Guid = s_TPixelFormat.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(Guid) }, null);
    private static readonly FieldInfo s_Flags_Set = s_TPixelFormat.GetField("_flags", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetField);
    private static readonly FieldInfo s_BitsPerPixel_Set = s_TPixelFormat.GetField("_bitsPerPixel", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetField);
    private static readonly PropertyInfo s_Guid_Get = s_TPixelFormat.GetProperty("Guid", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);
    private static readonly PropertyInfo s_Guid_Set = s_TPixelFormat.GetProperty("Guid", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty);

    /// <summary>
    /// Used in conversion from <see cref="EWicPixelFormatFlags"/> type to internal <see cref="PixelFormatFlags"/> type.
    /// </summary>
    private static readonly Type s_TMediaPixelFormatFlags = FindMediaType(typeof(PixelFormat).Assembly, "PixelFormatFlags");

    /// <summary>
    /// Returns any (public, internal, or private) <paramref name="typeName"/> type from specified <paramref name="assembly"/>.
    /// </summary>
    /// <param name="assembly">A assembly to be searched for type name.</param>
    /// <param name="typeName">A name of type to search for.</param>
    /// <returns>Return the instance of <see cref="Type"/> describing the type, if type found; Otherwise, returns null.</returns>
    private static Type FindMediaType(Assembly assembly, string typeName)
    {
      Type[] types = assembly.GetTypes();
      foreach (Type type in types)
      {
        if (type.Name == typeName)
        {
          return type;
        }
      }
      return null;
    }

    /// <summary>
    /// Returns the internal <see cref="Guid"/> value from specified (<see cref="PixelFormat"/>)  <paramref name="pixelFormat"/>.
    /// </summary>
    /// <param name="pixelFormat">A pixel format from which to get <see cref="Guid"/> value.</param>
    /// <returns>Returns a <see cref="Guid"/> value.</returns>
    [MethodImpl(256)]
    public static Guid GuidGet(PixelFormat pixelFormat)
    {
      object boxedPixelFormat = (object)pixelFormat;
      return (Guid)s_Guid_Get.GetValue(boxedPixelFormat);
    }

    /// <summary>
    /// Sets the internal <see cref="Guid"/> <paramref name="value"/> to specified (<see cref="PixelFormat"/>)  <paramref name="pixelFormat"/>.
    /// </summary>
    /// <param name="pixelFormat">A pixel format to which to set <see cref="Guid"/> value.</param>
    /// <param name="value">A <see cref="Guid"/> value to be set.</param>
    [MethodImpl(256)]
    public static void GuidSet(ref PixelFormat pixelFormat, Guid value)
    {
      object boxedPixelFormat = (object)pixelFormat;
      s_Guid_Set.SetValue(boxedPixelFormat, value);
    }

    /// <summary>
    /// Returns new instance of <see cref="PixelFormat"/> structure with specified format properties of the pixel.
    /// <para>NOTE: function uses reflection to set internals of <see cref="PixelFormat"/> structure, since .NET is not capable of interpreting all WIC pixel formats.</para>
    /// </summary>
    /// <param name="pixelFormatGuid">A one of <see cref="WicPixelFormatGUIDs"/> member <see cref="Guid"/> values.</param>
    /// <param name="bitsPerPixel">A nullable bits per pixel value to enforce setting of value on <see cref="PixelFormat"/> structure instance.</param>
    /// <param name="flags">A nullable <see cref="EWicPixelFormatFlags"/> value to enforce setting of value on <see cref="PixelFormat"/> structure instance.</param>
    /// <returns>Returns new instance of <see cref="PixelFormat"/> structure with specified information.</returns>
    public static PixelFormat PixelFormatFromGuid(Guid pixelFormatGuid, uint? bitsPerPixel = null, EWicPixelFormatFlags? flags = null)
    {
      PixelFormat pixelFormat = new PixelFormat();
      pixelFormat = (PixelFormat)s_Constructor_Guid.Invoke(new object[] { pixelFormatGuid });
      // NOTE: we must box the structure type in order to get (ref out set) value
      object boxedPixelFormat = (object)pixelFormat;
      if (flags.HasValue)
      {
        s_Flags_Set.SetValue(boxedPixelFormat, Enum.ToObject(s_TMediaPixelFormatFlags, (int)flags));
      }
      if (bitsPerPixel.HasValue)
      {
        s_BitsPerPixel_Set.SetValue(boxedPixelFormat, bitsPerPixel.Value);
      }
      return (PixelFormat)boxedPixelFormat;
    }


    // TODO: some packed bits pixel formats are missing HDR10 BT.2020 color space

    /// <summary>
    /// <see cref="PixelFormats.Default"/>
    /// <para>(unknown, WIC terminology:'don't care')</para>
    /// </summary>
    public static readonly PixelFormat PixelFormatDontCare = PixelFormatFromGuid(GUID_WICPixelFormatDontCare);
    /// <summary>
    /// <see cref="PixelFormats.Indexed1"/>
    /// <para>(channels:1 (indexed), pixel bits:1, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat1bppIndexed = PixelFormatFromGuid(GUID_WICPixelFormat1bppIndexed);
    /// <summary>
    /// <see cref="PixelFormats.Indexed2"/>
    /// <para>(channels:1 (indexed), pixel bits:2, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat2bppIndexed = PixelFormatFromGuid(GUID_WICPixelFormat2bppIndexed);
    /// <summary>
    /// <see cref = "PixelFormats.Indexed4" />
    /// <para>(channels:1 (indexed), pixel bits:4, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat4bppIndexed = PixelFormatFromGuid(GUID_WICPixelFormat4bppIndexed);
    /// <summary>
    /// <see cref = "PixelFormats.Indexed8" />
    /// <para>(channels:1 (indexed), pixel bits:8, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat8bppIndexed = PixelFormatFromGuid(GUID_WICPixelFormat8bppIndexed);
    /// <summary>
    /// <see cref = "PixelFormats.BlackWhite" />
    /// <para>(channels:1, channel bits:1, pixel bits:1, type: UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormatBlackWhite = PixelFormatFromGuid(GUID_WICPixelFormatBlackWhite);
    /// <summary>
    /// <see cref = "PixelFormats.Gray2" />
    /// <para>(channels:1, channel bits:2, pixel bits:2, type: UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat2bppGray = PixelFormatFromGuid(GUID_WICPixelFormat2bppGray);
    /// <summary>
    /// <see cref = "PixelFormats.Gray4" />
    /// <para>(channels:1, channel bits:4, pixel bits:4, type: UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat4bppGray = PixelFormatFromGuid(GUID_WICPixelFormat4bppGray);
    /// <summary>
    /// <see cref = "PixelFormats.Gray8" />
    /// <para>(channels:1, channel bits:8, pixel bits:8, type: UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat8bppGray = PixelFormatFromGuid(GUID_WICPixelFormat8bppGray);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:1, channel bits:8, pixel bits:32, type: UINT)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel16"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat8bppAlpha = PixelFormatFromGuid(GUID_WICPixelFormat8bppAlpha, 8, BitsPerPixel8);
    /// <summary>
    /// <see cref = "PixelFormats.Bgr555" />
    /// <para>(channels:3 (BGR), channel bits:5, pixel bits:16, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat16bppBGR555 = PixelFormatFromGuid(GUID_WICPixelFormat16bppBGR555);
    /// <summary>
    /// <see cref = "PixelFormats.Bgr565" />
    /// <para>(channels:3 (BGR), channel bits:B(5)G(6)R(5), pixel bits:16, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat16bppBGR565 = PixelFormatFromGuid(GUID_WICPixelFormat16bppBGR565);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (BGR), channel bits:B(5)G(5)R(5)A(1), pixel bits:16, type:UINT)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel16"/> | <see cref="ChannelOrderBGRA"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat16bppBGRA5551 = PixelFormatFromGuid(GUID_WICPixelFormat16bppBGRA5551, 16, BitsPerPixel16 | ChannelOrderBGRA | IsSRGB);
    /// <summary>
    /// <see cref = "PixelFormats.Gray16" />
    /// <para>(channels:1, channel bits:16, pixel bits:16, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat16bppGray = PixelFormatFromGuid(GUID_WICPixelFormat16bppGray);
    /// <summary>
    /// <see cref = "PixelFormats.Bgr24" />
    /// <para>(channels:3 (BGR), channel bits:8, pixel bits:24, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat24bppBGR = PixelFormatFromGuid(GUID_WICPixelFormat24bppBGR);
    /// <summary>
    /// <see cref = "PixelFormats.Rgb24" />
    /// <para>(channels:3 (RGB), channel bits:8, pixel bits:24, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat24bppRGB = PixelFormatFromGuid(GUID_WICPixelFormat24bppRGB);
    /// <summary>
    /// <see cref = "PixelFormats.Bgr32" />
    /// <para>(channels:3 (BGR), channel bits:8, pixel bits:32, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppBGR = PixelFormatFromGuid(GUID_WICPixelFormat32bppBGR);
    /// <summary>
    /// <see cref = "PixelFormats.Bgra32" />
    /// <para>(channels:4 (BGRA), channel bits:8, pixel bits:32, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppBGRA = PixelFormatFromGuid(GUID_WICPixelFormat32bppBGRA);
    /// <summary>
    /// <see cref = "PixelFormats.Pbgra32" />
    /// <para>(channels:4 (BGRA-premultiplied), channel bits:8, pixel bits:32, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppPBGRA = PixelFormatFromGuid(GUID_WICPixelFormat32bppPBGRA);
    /// <summary>
    /// <see cref = "PixelFormats.Gray32Float" />
    /// <para>(channels:1, channel bits:32, pixel bits:32, type:Float)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppGrayFloat = PixelFormatFromGuid(GUID_WICPixelFormat32bppGrayFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:8, pixel bits:32, type:UINT)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppRGB = PixelFormatFromGuid(GUID_WICPixelFormat32bppRGB, 32, BitsPerPixel32 | ChannelOrderRGB | IsSRGB);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (RGBA), channel bits:8, pixel bits:32, type:UINT)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppRGBA = PixelFormatFromGuid(GUID_WICPixelFormat32bppRGBA, 32, BitsPerPixel32 | ChannelOrderRGBA | IsSRGB);
    /// <summary>
    /// <see cref = "PixelFormats.Rgb48" />
    /// <para>(channels:4 (RGBA-premultiplied), channel bits:8, pixel bits:32, type:UINT)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsPremultiplied"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppPRGBA = PixelFormatFromGuid(GUID_WICPixelFormat32bppPRGBA, 32, BitsPerPixel32 | ChannelOrderRGBA | IsPremultiplied | IsSRGB);
    /// <summary>
    /// <see cref = "PixelFormats.Rgb48" />
    /// <para>(channels:3 (RGB), channel bits:16, pixel bits:48, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat48bppRGB = PixelFormatFromGuid(GUID_WICPixelFormat48bppRGB);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (BGR), channel bits:16, pixel bits:48, type:UINT)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderBGR"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat48bppBGR = PixelFormatFromGuid(GUID_WICPixelFormat48bppBGR, 64, BitsPerPixel64 | ChannelOrderBGR | IsSRGB);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:16, pixel bits:64, type:UINT)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppRGB = PixelFormatFromGuid(GUID_WICPixelFormat64bppRGB, 64, BitsPerPixel64 | ChannelOrderRGB | IsSRGB);
    /// <summary>
    /// <see cref = "PixelFormats.Rgba64" />
    /// <para>(channels:4 (RGBA), channel bits:16, pixel bits:64, type:UINT)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppRGBA = PixelFormatFromGuid(GUID_WICPixelFormat64bppRGBA, 64, BitsPerPixel64 | ChannelOrderRGBA | IsSRGB);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (BGRA), channel bits:16, pixel bits:64, type:UINT)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderBGRA"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppBGRA = PixelFormatFromGuid(GUID_WICPixelFormat64bppBGRA, 64, BitsPerPixel64 | ChannelOrderBGRA | IsSRGB);
    /// <summary>
    /// <see cref = "PixelFormats.Prgba64" />
    /// <para>(channels:4 (RGBA-premultiplied), channel bits:16, pixel bits:64, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppPRGBA = PixelFormatFromGuid(GUID_WICPixelFormat64bppPRGBA);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (BGRA-premultiplied), channel bits:16, pixel bits:64, type:UINT)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderBGRA"/> | <see cref="IsPremultiplied"/> | <see cref="IsSRGB"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppPBGRA = PixelFormatFromGuid(GUID_WICPixelFormat64bppPBGRA, 64, BitsPerPixel64 | ChannelOrderBGRA | IsPremultiplied | IsSRGB);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:1, channel bits:16, pixel bits:16, type:FixedPoint)</para>
    /// <para>NOTE: not supported by <see cref="System.Windows.Media.PixelFormat"/> </para>
    /// <para>NOTE: .NET Framework 4.8 does not define a value of <see cref="StorageFixedPoint"/>, but WIC clearly supports this pixel format.</para>
    /// <para>NOTE: private field PixelFormat._flags overridden to : <see cref="BitsPerPixel16"/> | <see cref="IsGray"/> | <see cref="StorageFixedPoint"/></para>
    /// </summary>
    public static readonly PixelFormat PixelFormat16bppGrayFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat16bppGrayFixedPoint, 16, BitsPerPixel16 | IsGray | StorageFixedPoint);
    /// <summary>
    /// <see cref="PixelFormats.Bgr101010"/>
    /// <para>(channels:3 (BGR), channel bits:10, pixel bits:32, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppBGR101010 = PixelFormatFromGuid(GUID_WICPixelFormat32bppBGR101010);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:16, pixel bits:48, type:FixedPoint)</para>
    /// <para>NOTE: .NET Framework 4.8 does not define a value of <see cref="StorageFixedPoint"/>, but WIC clearly supports this pixel format.</para>
    /// <para>NOTE: private field PixelFormat._flags overridden to : <see cref="BitsPerPixel48"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsScRGB"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat48bppRGBFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat48bppRGBFixedPoint, 48, BitsPerPixel48 | ChannelOrderRGB | IsScRGB | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (BGR), channel bits:16, pixel bits:48, type:FixedPoint)</para>
    /// <para>NOTE: .NET Framework 4.8 does not define a value of <see cref="StorageFixedPoint"/>, but WIC clearly supports this pixel format.</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel48"/> | <see cref="ChannelOrderBGR"/> | <see cref="IsScRGB"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat48bppBGRFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat48bppBGRFixedPoint, 48, BitsPerPixel48 | ChannelOrderBGR | IsScRGB | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:32, pixel bits:96, type:FixedPoint)</para>
    /// <para>NOTE: .NET Framework 4.8 does not define a value of <see cref="StorageFixedPoint"/>, but WIC clearly supports this pixel format.</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel96"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsScRGB"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat96bppRGBFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat96bppRGBFixedPoint, 96, BitsPerPixel96 | ChannelOrderRGB | IsScRGB | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:32, pixel bits:96, type:Float)</para>
    /// <para>NOTE: .NET Framework 4.8 does not define a value of <see cref="StorageFloat"/>, but WIC clearly supports this pixel format.</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel96"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/></para>
    /// </summary>
    public static readonly PixelFormat PixelFormat96bppRGBFloat = PixelFormatFromGuid(GUID_WICPixelFormat96bppRGBFloat, 96, BitsPerPixel96 | ChannelOrderRGB | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (RGBA), channel bits:32, pixel bits:128, type:Float)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel128"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat128bppRGBAFloat = PixelFormatFromGuid(GUID_WICPixelFormat128bppRGBAFloat, 128, BitsPerPixel128 | ChannelOrderRGBA | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (RGBA-premultiplied), channel bits:32, pixel bits:128, type:Float)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel128"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsPremultiplied"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat128bppPRGBAFloat = PixelFormatFromGuid(GUID_WICPixelFormat128bppPRGBAFloat, 128, BitsPerPixel128 | ChannelOrderRGBA | IsPremultiplied | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:32, pixel bits:128, type:Float)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel128"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat128bppRGBFloat = PixelFormatFromGuid(GUID_WICPixelFormat128bppRGBFloat, 128, BitsPerPixel128 | ChannelOrderRGB | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (CMYK), channel bits:8, pixel bits:32, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppCMYK = PixelFormatFromGuid(GUID_WICPixelFormat32bppCMYK);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (RGBA), channel bits:16, pixel bits:64, type:FoxedPoint)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsScRGB"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppRGBAFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat64bppRGBAFixedPoint, 64, BitsPerPixel64 | ChannelOrderRGBA | IsScRGB | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (BGRA), channel bits:16, pixel bits:64, type:FoxedPoint)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderBGRA"/> | <see cref="IsScRGB"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppBGRAFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat64bppBGRAFixedPoint, 64, BitsPerPixel64 | ChannelOrderBGRA | IsScRGB | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:16, pixel bits:64, type:FoxedPoint)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsScRGB"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppRGBFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat64bppRGBFixedPoint, 64, BitsPerPixel64 | ChannelOrderRGB | IsScRGB | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (RGBA), channel bits:32, pixel bits:128, type:FoxedPoint)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsScRGB"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat128bppRGBAFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat128bppRGBAFixedPoint, 128, BitsPerPixel128 | ChannelOrderRGBA | IsScRGB | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (RGB), channel bits:32, pixel bits:128, type:FoxedPoint)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel128"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsScRGB"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat128bppRGBFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat128bppRGBFixedPoint, 128, BitsPerPixel128 | ChannelOrderRGB | IsScRGB | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (RGBA), channel bits:16, pixel bits:64, type:Float[Half])</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppRGBAHalf = PixelFormatFromGuid(GUID_WICPixelFormat64bppRGBAHalf, 64, BitsPerPixel64 | ChannelOrderRGBA | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (RGBA-premultiplied), channel bits:16, pixel bits:64, type:Float[Half])</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsPremultiplied"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppPRGBAHalf = PixelFormatFromGuid(GUID_WICPixelFormat64bppPRGBAHalf, 64, BitsPerPixel64 | ChannelOrderRGBA | IsPremultiplied | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:16, pixel bits:64, type:Float[Half])</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppRGBHalf = PixelFormatFromGuid(GUID_WICPixelFormat64bppRGBHalf, 64, BitsPerPixel64 | ChannelOrderRGB | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB), channel bits:16, pixel bits:48, type:Float[Half])</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="ChannelOrderRGBA"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat48bppRGBHalf = PixelFormatFromGuid(GUID_WICPixelFormat48bppRGBHalf, 48, BitsPerPixel64 | ChannelOrderRGBA | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:3 (RGB[E]), channel bits:8, pixel bits:32, type:Float[Half])</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="ChannelOrderRGB"/> | <see cref="IsScRGB"/> | <see cref="StorageFloat"/> </para>
    /// <para>
    /// NOTE: format encodes three 16-bit floating-point values in 4 bytes, as follows:
    /// Three unsigned 8-bit mantissas for the R, G, and B channels, plus a shared 8-bit exponent. 
    /// This format provides 16-bit floating-point precision in a smaller pixel representation.
    /// </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppRGBE = PixelFormatFromGuid(GUID_WICPixelFormat32bppRGBE, 32, BitsPerPixel32 | ChannelOrderRGB | IsScRGB | StorageFloat);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:1, channel bits:16, pixel bits:16, type:Float)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel16"/> | <see cref="IsGray"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat16bppGrayHalf = PixelFormatFromGuid(GUID_WICPixelFormat16bppGrayHalf, 16, BitsPerPixel16 | IsGray | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:1, channel bits:32, pixel bits:32, type:FixedPoint)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="IsGray"/> | <see cref="StorageFixedPoint"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppGrayFixedPoint = PixelFormatFromGuid(GUID_WICPixelFormat32bppGrayFixedPoint, 32, BitsPerPixel32 | IsGray | StorageFixedPoint);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// .Net simply lacks the knowledge to explain this format
    /// <para>(channels:4 (RGBA), channel bits:R(10)G(10)B(10)A(2), pixel bits:32, type:UINT)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="ChannelOrderRGBA"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppRGBA1010102 = PixelFormatFromGuid(GUID_WICPixelFormat32bppRGBA1010102, 32, BitsPerPixel32 | ChannelOrderRGBA);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// .Net simply lacks the knowledge to explain this format
    /// <para>(channels:4 (RGBA), channel bits:R(10)G(10)B(10)A(2), pixel bits:32, type:UINT)</para>
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="ChannelOrderRGBA"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bppRGBA1010102XR = PixelFormatFromGuid(GUID_WICPixelFormat32bppRGBA1010102XR, 32, BitsPerPixel32 | ChannelOrderRGBA);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// <para>(channels:4 (CMYK), channel bits:16, pixel bits:64, type:UINT)</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bppCMYK = PixelFormatFromGuid(GUID_WICPixelFormat64bppCMYK, 64);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:3, channel bits:8, pixel bits:24, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel24"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat24bpp3Channels = PixelFormatFromGuid(GUID_WICPixelFormat24bpp3Channels, 24, BitsPerPixel24 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:4, channel bits:8, pixel bits:32, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bpp4Channels = PixelFormatFromGuid(GUID_WICPixelFormat32bpp4Channels, 32, BitsPerPixel32 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:5, channel bits:8, pixel bits:40, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel40"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat40bpp5Channels = PixelFormatFromGuid(GUID_WICPixelFormat40bpp5Channels, 40, BitsPerPixel40 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:6, channel bits:8, pixel bits:48, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel48"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat48bpp6Channels = PixelFormatFromGuid(GUID_WICPixelFormat48bpp6Channels, 48, BitsPerPixel48 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:7, channel bits:8, pixel bits:56, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel56"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat56bpp7Channels = PixelFormatFromGuid(GUID_WICPixelFormat56bpp7Channels, 56, BitsPerPixel56 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:8, channel bits:8, pixel bits:64, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bpp8Channels = PixelFormatFromGuid(GUID_WICPixelFormat64bpp8Channels, 64, BitsPerPixel64 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:3, channel bits:16, pixel bits:48, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel48"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat48bpp3Channels = PixelFormatFromGuid(GUID_WICPixelFormat48bpp3Channels, 48, BitsPerPixel48 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:4, channel bits:16, pixel bits:64, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bpp4Channels = PixelFormatFromGuid(GUID_WICPixelFormat64bpp4Channels, 64, BitsPerPixel64 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:5, channel bits:16, pixel bits:80, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel80"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat80bpp5Channels = PixelFormatFromGuid(GUID_WICPixelFormat80bpp5Channels, 80, BitsPerPixel80 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:6, channel bits:16, pixel bits:96, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel96"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat96bpp6Channels = PixelFormatFromGuid(GUID_WICPixelFormat96bpp6Channels, 96, BitsPerPixel96 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:7, channel bits:16, pixel bits:112, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel112"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat112bpp7Channels = PixelFormatFromGuid(GUID_WICPixelFormat112bpp7Channels, 112, BitsPerPixel112 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:8, channel bits:16, pixel bits:128, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel128"/> | <see cref="IsNChannel"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat128bpp8Channels = PixelFormatFromGuid(GUID_WICPixelFormat128bpp8Channels, 128, BitsPerPixel128 | IsNChannel);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:CMYK, channel bits:8, pixel bits:40, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel40"/> | <see cref="IsCMYK"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat40bppCMYKAlpha = PixelFormatFromGuid(GUID_WICPixelFormat40bppCMYKAlpha, 40, BitsPerPixel40 | IsCMYK | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:CMYK, channel bits:16, pixel bits:80, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel80"/> | <see cref="IsCMYK"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat80bppCMYKAlpha = PixelFormatFromGuid(GUID_WICPixelFormat80bppCMYKAlpha, 80, BitsPerPixel80 | IsCMYK | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:3+A, channel bits:8, pixel bits:32, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel32"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat32bpp3ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat32bpp3ChannelsAlpha, 32, BitsPerPixel32 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:4+A, channel bits:8, pixel bits:40, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel40"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat40bpp4ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat40bpp4ChannelsAlpha, 40, BitsPerPixel40 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:5+A, channel bits:8, pixel bits:48, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel48"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat48bpp5ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat48bpp5ChannelsAlpha, 48, BitsPerPixel48 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:6+A, channel bits:8, pixel bits:56, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel56"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat56bpp6ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat56bpp6ChannelsAlpha, 56, BitsPerPixel56 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:7+A, channel bits:8, pixel bits:64, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bpp7ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat64bpp7ChannelsAlpha, 64, BitsPerPixel64 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:8+A, channel bits:8, pixel bits:72, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel72"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat72bpp8ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat72bpp8ChannelsAlpha, 72, BitsPerPixel72 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:3+A, channel bits:16, pixel bits:64, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel64"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat64bpp3ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat64bpp3ChannelsAlpha, 64, BitsPerPixel64 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:4+A, channel bits:16, pixel bits:80, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel80"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat80bpp4ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat80bpp4ChannelsAlpha, 80, BitsPerPixel80 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:5+A, channel bits:16, pixel bits:96, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel96"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat96bpp5ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat96bpp5ChannelsAlpha, 96, BitsPerPixel96 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:6+A, channel bits:16, pixel bits:112, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel112"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat112bpp6ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat112bpp6ChannelsAlpha, 112, BitsPerPixel112 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:7+A, channel bits:16, pixel bits:128, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel128"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat128bpp7ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat128bpp7ChannelsAlpha, 128, BitsPerPixel128 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// (channels:8+A, channel bits:16, pixel bits:144, type:UINT)
    /// <para>NOTE: private member PixelFormat._flags overridden to : <see cref="BitsPerPixel144"/> | <see cref="IsNChannelAlpha"/> </para>
    /// </summary>
    public static readonly PixelFormat PixelFormat144bpp8ChannelsAlpha = PixelFormatFromGuid(GUID_WICPixelFormat144bpp8ChannelsAlpha, 144, BitsPerPixel144 | IsNChannelAlpha);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// Can't be used to display image
    /// <para>8bppY, bits:(-1) 0, flags: 0h (BitsPerPixelUndefined), alpha: N, guid: '91b4db54-2df9-42f0-b449-2909bb3df88e'</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat8bppY = PixelFormatFromGuid(GUID_WICPixelFormat8bppY);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// Can't be used to display image
    /// <para>8bppCb, bits:(-1) 0, flags: 0h (BitsPerPixelUndefined), alpha: N, guid: '1339f224-6bfe-4c3e-9302-e4f3a6d0ca2a'</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat8bppCb = PixelFormatFromGuid(GUID_WICPixelFormat8bppCb);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// Can't be used to display image
    /// <para>8bppCr, bits:(-1) 0, flags: 0h (BitsPerPixelUndefined), alpha: N, guid: 'b8145053-2116-49f0-8835-ed844b205c51'</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat8bppCr = PixelFormatFromGuid(GUID_WICPixelFormat8bppCr);
    /// <summary>
    /// WIC Native Pixel Format only.
    /// Can't be used to display image
    /// <para>8bppCbCr, bits:(-1) 0, flags: 0h (BitsPerPixelUndefined), alpha: N, guid: 'ff95ba6e-11e0-4263-bb45-01721f3460a4'</para>
    /// </summary>
    public static readonly PixelFormat PixelFormat16bppCbCr = PixelFormatFromGuid(GUID_WICPixelFormat16bppCbCr);



  }


}
