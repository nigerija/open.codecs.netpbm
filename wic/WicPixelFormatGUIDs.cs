﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm.wic
{
  // TODO: WicPixelFormatGUIDs finish XML DOC supported native codecs encode/decode
  // DOCS : https://docs.microsoft.com/en-us/windows/win32/wic/-wic-codec-native-pixel-formats

  /// <summary>
  /// WIC native pixel formats GUID definitions.
  /// </summary>
  public static class WicPixelFormatGUIDs
  {
    /// <summary>
    /// Name prefix for cached names
    /// </summary>
    private const string NAME_PREFIX = "GUID_WICPixelFormat";

    /// <summary>
    /// Undefined pixel format
    /// <para>Native decoder support: ?</para>
    /// <para>Native encoder support: ?</para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormatDontCare = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc900}");
    /// <summary>
    /// <para>Native decoder support: BMP, PNG, TIFF </para>
    /// <para>Native encoder support: BMP, PNG, TIFF </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat1bppIndexed = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc901}");
    /// <summary>
    /// <para>Native decoder support: PNG </para>
    /// <para>Native encoder support: PNG </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat2bppIndexed = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc902}");
    /// <summary>
    /// <para>Native decoder support: BMP, PNG, TIFF </para>
    /// <para>Native encoder support: BMP, PNG, TIFF </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat4bppIndexed = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc903}");
    /// <summary>
    /// <para>Native decoder support: BMP, GIF, PNG, TIFF </para>
    /// <para>Native encoder support: BMP, GIF, PNG, TIFF </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat8bppIndexed = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc904}");
    /// <summary>
    /// <para>Native decoder support: PNG, TIFF, JPEG-XR </para>
    /// <para>Native encoder support: PNG, TIFF, JPEG-XR </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormatBlackWhite = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc905}");
    /// <summary>
    /// <para>Native decoder support: PNG </para>
    /// <para>Native encoder support: PNG </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat2bppGray = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc906}");
    /// <summary>
    /// <para>Native decoder support: PNG, TIFF </para>
    /// <para>Native encoder support: PNG, TIFF </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat4bppGray = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc907}");
    /// <summary>
    /// Grayscale 8-bit pixel format 
    /// <para>Native decoder support: JPEG, PNG, TIFF, JPEG-XR </para>
    /// <para>Native encoder support: JPEG, PNG, TIFF, JPEG-XR </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat8bppGray = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc908}");
    /// <summary>
    /// Alpha only pixel format (channels : 1, bits per channel : 8, bits per pixel : 32, storage : UINT)
    /// <para>Native decoder support: none</para>
    /// <para>Native encoder support: none</para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat8bppAlpha = new Guid("{e6cd0116-eeba-4161-aa85-27dd9fb3a895}");
    /// <summary>
    /// Packed bit pixel format
    /// <para>Native decoder support: BMP, JPEG-XR </para>
    /// <para>Native encoder support: BMP, JPEG-XR </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat16bppBGR555 = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc909}");
    /// <summary>
    /// Packed bit pixel format
    /// <para>Native decoder support: BMP, </para>
    /// <para>Native encoder support: BMP, </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat16bppBGR565 = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc90a}");
    /// <summary>
    /// Packed 16-bit BGRA (channels: 3, bits per channel:5(B)/6(G)/5(R),	bits per pixel: 16, storage:UINT)
    /// <para>Native decoder support: none</para>
    /// <para>Native encoder support: none</para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat16bppBGRA5551 = new Guid("{05ec7c2b-f1e6-4961-ad46-e1cc810a87d2}");
    /// <summary>
    /// <para>Native decoder support: PNG, TIFF, JPEG-XR, JPEG?</para>
    /// <para>Native encoder support: PNG, TIFF, JPEG-XR, JPEG?</para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat16bppGray = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc90b}");
    /// <summary>
    /// <para>Native decoder support: BMP, JPEG, PNG, TIFF, JPEG-XR </para>
    /// <para>Native encoder support: BMP, JPEG, PNG, TIFF, JPEG-XR </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat24bppBGR = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc90c}");
    /// <summary>
    /// <para>Native decoder support: JPEG-XR </para>
    /// <para>Native encoder support: JPEG-XR </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat24bppRGB = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc90d}");
    /// <summary>
    /// <para>Native decoder support: BMP, PNG, TIFF, JPEG-XR, </para>
    /// <para>Native encoder support: BMP, PNG, TIFF, JPEG-XR, </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppBGR = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc90e}");
    /// <summary>
    /// 
    /// <para>Native decoder support: BMP, ICO, PNG, TIFF, JPEG-XR, DDS </para>
    /// <para>Native encoder support: BMP, ICO, PNG, TIFF, JPEG-XR, DDS </para>
    /// <para>* BMP GUID_WICPixelFormat32bppBGRA is only supported in Windows 8, the Platform Update for Windows 7, and above.</para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppBGRA = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc90f}");
    /// <summary>
    /// <para>Native decoder support: TIFF, DDS </para>
    /// <para>Native encoder support: BMP, DDS </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppPBGRA = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc910}");
    /// <summary>
    /// <para>Native decoder support: TIFF </para>
    /// <para>Native encoder support: none </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppGrayFloat = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc911}");
    /// <summary>
    /// <para>Native decoder support: none </para>
    /// <para>Native encoder support: none </para>
    /// <para>* GUID_WICPixelFormat32bppRGB is only supported in Windows 8, the Platform Update for Windows 7, and above.</para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppRGB = new Guid("{d98c6b95-3efe-47d6-bb25-eb1748ab0cf1}");
    /// <summary>
    /// <para>Native decoder support: none </para>
    /// <para>Native encoder support: none </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppRGBA = new Guid("{f5c7ad2d-6a8d-43dd-a7a8-a29935261ae9}");
    /// <summary>
    /// <para>Native decoder support: none </para>
    /// <para>Native encoder support: none </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppPRGBA = new Guid("{3cc4a650-a527-4d37-a916-3142c7ebedba}");
    /// <summary>
    /// <para>Native decoder support: PNG, TIFF, JPEG-XR </para>
    /// <para>Native encoder support: PNG, TIFF, JPEG-XR </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat48bppRGB = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc915}");
    /// <summary>
    /// <para>Native decoder support: none </para>
    /// <para>Native encoder support: PNG </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat48bppBGR = new Guid("{e605a384-b468-46ce-bb2e-36f180e64313}");
    /// <summary>
    /// <para>Native decoder support: none </para>
    /// <para>Native encoder support: none </para>
    /// <para>* GUID_WICPixelFormat64bppRGB is only supported in Windows 8, the Platform Update for Windows 7, and above.</para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppRGB = new Guid("{a1182111-186d-4d42-bc6a-9c8303a8dff9}");
    /// <summary>
    /// <para>Native decoder support: PNG, TIFF, JPEG-XR </para>
    /// <para>Native encoder support: PNG, TIFF, JPEG-XR </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppRGBA = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc916}");
    /// <summary>
    /// <para>Native decoder support: none </para>
    /// <para>Native encoder support: PNG </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppBGRA = new Guid("{1562ff7c-d352-46f9-979e-42976b792246}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppPRGBA = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc917}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppPBGRA = new Guid("{8c518e8e-a4ec-468b-ae70-c9a35a9c5530}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat16bppGrayFixedPoint = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc913}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppBGR101010 = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc914}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat48bppRGBFixedPoint = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc912}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat48bppBGRFixedPoint = new Guid("{49ca140e-cab6-493b-9ddf-60187c37532a}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat96bppRGBFixedPoint = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc918}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat96bppRGBFloat = new Guid("{e3fed78f-e8db-4acf-84c1-e97f6136b327}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat128bppRGBAFloat = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc919}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat128bppPRGBAFloat = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc91a}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat128bppRGBFloat = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc91b}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppCMYK = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc91c}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppRGBAFixedPoint = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc91d}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppBGRAFixedPoint = new Guid("{356de33c-54d2-4a23-bb04-9b7bf9b1d42d}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppRGBFixedPoint = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc940}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat128bppRGBAFixedPoint = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc91e}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat128bppRGBFixedPoint = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc941}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppRGBAHalf = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc93a}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppPRGBAHalf = new Guid("{58ad26c2-c623-4d9d-b320-387e49f8c442}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppRGBHalf = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc942}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat48bppRGBHalf = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc93b}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// <para>NOTE: 
    /// * The GUID_WICPixelFormat32bppRGBE format encodes three 16-bit floating-point values in 4 bytes, as follows:
    /// Three unsigned 8-bit mantissas for the R, G, and B channels, plus a shared 8-bit exponent.
    /// This format provides 16-bit floating-point precision in a smaller pixel representation.</para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppRGBE = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc93d}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat16bppGrayHalf = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc93e}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppGrayFixedPoint = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc93f}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppRGBA1010102 = new Guid("{25238D72-FCF9-4522-b514-5578e5ad55e0}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bppRGBA1010102XR = new Guid("{00DE6B9A-C101-434b-b502-d0165ee1122c}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bppCMYK = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc91f}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat24bpp3Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc920}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bpp4Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc921}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat40bpp5Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc922}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat48bpp6Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc923}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat56bpp7Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc924}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bpp8Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc925}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat48bpp3Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc926}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bpp4Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc927}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat80bpp5Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc928}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat96bpp6Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc929}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat112bpp7Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc92a}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat128bpp8Channels = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc92b}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat40bppCMYKAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc92c}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat80bppCMYKAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc92d}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat32bpp3ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc92e}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat40bpp4ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc92f}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat48bpp5ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc930}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat56bpp6ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc931}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bpp7ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc932}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat72bpp8ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc933}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat64bpp3ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc934}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat80bpp4ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc935}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat96bpp5ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc936}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat112bpp6ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc937}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat128bpp7ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc938}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat144bpp8ChannelsAlpha = new Guid("{6fddc324-4e03-4bfe-b185-3d77768dc939}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat8bppY = new Guid("{91B4DB54-2DF9-42F0-B449-2909BB3DF88E}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat8bppCb = new Guid("{1339F224-6BFE-4C3E-9302-E4F3A6D0CA2A}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat8bppCr = new Guid("{B8145053-2116-49F0-8835-ED844B205C51}");
    /// <summary>
    /// <para>Native decoder support: </para>
    /// <para>Native encoder support: </para>
    /// </summary>
    public static readonly Guid GUID_WICPixelFormat16bppCbCr = new Guid("{FF95BA6E-11E0-4263-BB45-01721F3460A4}");

    /// <summary>
    /// static cache with guid to name list
    /// </summary>
    private static readonly Dictionary<Guid, string> s_CacheGuidName = new Dictionary<Guid, string>();
    /// <summary>
    /// static cache with name to guid list
    /// </summary>
    private static readonly Dictionary<string, Guid> s_CacheNameGuid = new Dictionary<string, Guid>();

    /// <summary>
    /// Instantiates and populates static members (cache lists)
    /// </summary>
    static WicPixelFormatGUIDs()
    {
      Type t = typeof(WicPixelFormatGUIDs);
      FieldInfo[] fields = t.GetFields();
      foreach(FieldInfo field in fields)
      {
        var name = field.Name.Substring(NAME_PREFIX.Length);
        var guid = (Guid)field.GetValue(null);
        s_CacheGuidName.Add(guid, name);
        s_CacheNameGuid.Add(name, guid);
      }
    }

    /// <summary>
    /// Returns pixel formats string name from GUID pixel format, if pixel format <paramref name="guid"/> found in the internal cache list; Otherwise, returns null.
    /// </summary>
    /// <param name="guid">A pixel format GUID to be translated to string name.</param>
    /// <returns>Returns the string with pixel format name, or null.</returns>
    public static string PixelFormatNameFromGuid(Guid guid)
    {
      s_CacheGuidName.TryGetValue(guid, out string value);
      return value;
    }
  }
}
