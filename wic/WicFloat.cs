﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm.wic
{
  /// <summary>
  /// <para>Contains method that performs correction of float values using correction curve for float pixel formats.</para>
  /// <para>Used in tests to generate valid gradients for float WIC pixel formats.</para>
  /// </summary>
  public class WicFloat
  {
    /// <summary>
    /// Correction multiplier factor steps of 0.050f.
    /// </summary>
    private const float Float32CorrectionStep = 0.05f;

    /// <summary>
    /// Correction multiplier factor from 0.000f to 1.000f in steps of 0.050f.
    /// </summary>
    private static readonly float[] s_Float32CorrectionCurve = new float[]
    {
      0.000000f, // 0.000
      0.003933f, // 0.050
      0.010008f, // 0.100
      0.019571f, // 0.150
      0.033036f, // 0.200
      0.050771f, // 0.250
      0.073090f, // 0.300
      0.100263f, // 0.350
      0.132570f, // 0.400
      0.170255f, // 0.450
      0.213562f, // 0.500
      0.262678f, // 0.550
      0.317810f, // 0.600
      0.379171f, // 0.650
      0.446945f, // 0.700
      0.521320f, // 0.750
      0.602440f, // 0.800
      0.690480f, // 0.850
      0.785610f, // 0.900
      0.887910f, // 0.950
      1.000000f, // 1.000
      1.000000f, // repeat 1.000000f not to include 'if(index == length-1)' in correction formula
    };

    /// <summary>
    /// Get maximum <see cref="s_Float32CorrectionCurve"/> index.
    /// </summary>
    private static readonly int s_Float32CorrectionCurveMaxIndex = s_Float32CorrectionCurve.Length - 1;

    /// <summary>
    /// <para>Corrects the output float value to linear intensity of float value from 0 to 1, that matches the linearity of the UINT resulting values.</para>
    /// <para>Otherwise the color curve for the float values gives resulting brighter images and non linear curve of intensity (inverse function of this).</para>
    /// </summary>
    /// <param name="value">A value to be corrected</param>
    /// <returns>Returns correct float value that represents the correct linear intensity of float value from 0 to 1.</returns>
    [MethodImpl(256)]
    public static float Float32Correction(float value)
    {
      // TODO: add version with min max value test and CLAMP value <0 || >1 
      int index = (int)(value / Float32CorrectionStep);
      // CLAMP
      if (index >= s_Float32CorrectionCurveMaxIndex)
      {
        return s_Float32CorrectionCurve[s_Float32CorrectionCurveMaxIndex];
      }
      // NOTE: do not use 'FLOAT % (modulo) STEP', it returns invalid reminder in 0.1% of cases !!!
      //       instead use reminder calculation, which is always correct in 100% cases.
      //float q2 = (value % Float32CorrectionStep) / Float32CorrectionStep;
      float q2 = (value - (Float32CorrectionStep * index)) / Float32CorrectionStep;
      float q1 = 1f - q2;
      return (s_Float32CorrectionCurve[index] * q1) + (s_Float32CorrectionCurve[index + 1] * q2);
    }
  }
}
