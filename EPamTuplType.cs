﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm
{
  /// <summary>
  /// Portable Arbitrary Map (.PAM) TUPLTYPE enumerated constants.
  /// <para>
  /// These enumerated constants are set when reading or writing PAM files, which are read or
  /// written with following Portable Map image container file type : <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/>.
  /// </para>
  /// </summary>
  public enum EPamTuplType
  {
    /// <summary>
    /// Undefined TUPLTYPE
    /// </summary>
    Undefined = 0,

    /// <summary>
    /// <para>Standard</para>
    /// Special case of GRAYSCALE (black and white)
    /// <para>Formats (channels : 1)</para>
    /// <para><see cref="EPmPixelFormat.W_1bit_1u1"/>, bits per pixel : 8 (1/8 bytes), MaxVal : [1];</para>
    /// </summary>
    BLACKANDWHITE,
    /// <summary>
    /// <para>Standard</para>
    /// <para>Formats (channels : 1)</para>
    /// <para><see cref="EPmPixelFormat.Y_8bits_1u8"/> 1 bytes, MaxVal : [2..255];</para>
    /// <para><see cref="EPmPixelFormat.Y_16bits_1u16"/> 2 bytes, MaxVal : [2..65535];</para>
    /// </summary>
    GRAYSCALE,
    /// <summary>
    /// <para>Standard</para>
    /// <para>Formats (channels : 3)</para>
    /// <para><see cref="EPmPixelFormat.RGB_24bits_3u8"/> 3 bytes, MaxVal : [2..255];</para>
    /// <para><see cref="EPmPixelFormat.RGB_48bits_3u16"/> 6 bytes, MaxVal : [2..65535];</para>    
    /// </summary>
    RGB,
    /// <summary>
    /// <para>Standard</para>
    /// <para>Formats (channels : 2)</para>
    /// <para><see cref="EPmPixelFormat.WA_16bits_2u8"/> 2 bytes, MaxVal : [1];</para>
    /// </summary>
    BLACKANDWHITE_ALPHA,
    /// <summary>
    /// <para>Standard</para>
    /// <para>Formats (channels : 2)</para>
    /// <para><see cref="EPmPixelFormat.YA_16bits_2u8"/> 2 bytes, MaxVal : [2..255];</para>
    /// <para><see cref="EPmPixelFormat.YA_32bits_2u16"/> 4 bytes, MaxVal : [2..65535];</para>
    /// </summary>
    GRAYSCALE_ALPHA,
    /// <summary>
    /// <para>Standard</para>
    /// <para>Formats (channels : 4)</para>
    /// <para><see cref="EPmPixelFormat.RGBA_32bits_4u8"/> 4 bytes, MaxVal : [2..255];</para>
    /// <para><see cref="EPmPixelFormat.RGBA_64bits_4u16"/> 8 bytes, MaxVal : [2..65535];</para>
    /// </summary>
    RGB_ALPHA,
    /// <summary>
    /// <para>Non-standard extension (per PFM specification)</para>
    /// <para>Formats (channels : 1)</para>
    /// <para><see cref="EPmPixelFormat.Y_32bits_1u32"/> 4 bytes, MaxVal : [0f..1f];</para>
    /// </summary>
    FLOAT_GRAYSCALE,
    /// <summary>
    /// <para>Non-standard extension (per PFM specification)</para>
    /// <para>Formats (channels : 3)</para>
    /// <para><see cref="EPmPixelFormat.RGB_96bits_3f32"/> 96 bytes, MaxVal : [0f..1f];</para>
    /// </summary>
    FLOAT_RGB,
    /// <summary>
    /// <para>Non-standard extension (per PF4 specification)</para>
    /// <para>Formats (channels : 2)</para>
    /// <para><see cref="EPmPixelFormat.YA_64bits_2f32"/> 64 bytes, MaxVal : [0f..1f];</para>
    /// </summary>
    FLOAT_GRAYSCALE_ALPHA,
    /// <summary>
    /// <para>Non-standard extension (per PF4 specification)</para>
    /// <para>Formats (channels : 4)</para>
    /// <para><see cref="EPmPixelFormat.RGBA_128bits_4f32"/> 128 bytes, MaxVal : [0f..1f];</para>
    /// </summary>
    FLOAT_RGB_ALPHA,
  }
}
