﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm
{
  /// <summary>
  /// Portable Map image container type, header magic value.
  /// </summary>
  public enum EPmFileType : uint
  {
    /// <summary>
    /// Unknown Pixel data format
    /// </summary>
    UNKNOWN = 0,
    /// <summary>
    /// <para>Pixel data format : Portable Bit Map ASCII format.</para>
    /// <para>File extension: '.pbm'.</para>
    /// <para>Header magic value: 'P1'.</para>
    /// <para>Channels: 1bit (W)</para>
    /// <para>Colors: 2 (0 - white, 1 - black).</para>
    /// </summary>
    P1_BITMAP_ASCII = 0x5031,
    /// <summary>
    /// <para>Pixel data format : Portable Gray Map ASCII format.</para>
    /// <para>File extension: '.pgm'.</para>
    /// <para>Header magic value: 'P2'.</para>
    /// <para>Channels: 1 (Y)</para>
    /// <para>Colors: 0–255 (gray scale), 0–65535 (gray scale), variable, black-to-white range.</para>
    /// </summary>
    P2_GRAYMAP_ASCII = 0x5032,
    /// <summary>
    /// <para>Pixel data format : Portable Pix Map ASCII format.</para>
    /// <para>File extension: '.ppm'.</para>
    /// <para>Header magic value: 'P3'.</para>
    /// <para>Channels: 3 (RGB)</para>
    /// <para>Colors: 6777216 (0–255 for each RGB channel value), 281474976710656 (0-65535 for each RGB channel value).</para>
    /// </summary>
    P3_PIXMAP_ASCII = 0x5033,
    /// <summary>
    /// <para>Pixel data format : Portable Bit Map BINARY format.</para>
    /// <para>File extension: '.pbm'.</para>
    /// <para>Header magic value: 'P4'.</para>
    /// <para>Channels: 1bit (W)</para>
    /// <para>Colors: 0 - white, and 1 - black.</para>
    /// </summary>
    P4_BITMAP_BINARY = 0x5034,
    /// <summary>
    /// <para>Pixel data format : Portable Gray Map BINARY format.</para>
    /// <para>File extension: '.pgm'.</para>
    /// <para>Header magic value: 'P5'.</para>
    /// <para>Channels: 1 (Y)</para>
    /// <para>Colors: 0–255 (gray scale), 0–65535 (gray scale), variable, black-to-white range.</para>
    /// </summary>
    P5_GRAYMAP_BINARY = 0x5035,
    /// <summary>
    /// <para>Pixel data format: Portable Pix Map BINARY format.</para>
    /// <para>File extension: '.ppm'.</para>
    /// <para>Header magic value: 'P6'.</para>
    /// <para>Channels: 3 (RGB)</para>
    /// <para>Colors: 6777216 (0–255 for each RGB channel value), 281474976710656 (0-65535 for each RGB channel value).</para>
    /// </summary>
    P6_PIXMAP_BINARY = 0x5036,
    /// <summary>
    /// <para>Pixel data format : Portable Arbitrary Map BINARY format.</para>
    /// <para>File extension: '.pam'.</para>
    /// <para>Header magic value: 'P7'.</para>
    /// <para>Channels: arbitrary channels *.
    /// </para>
    /// <para>Colors: arbitrary colors *.</para>
    /// <para>*: many formats are supported (1-bit (W), 8-bit or 16-bit (Y), 16-bit or 32-bit (YA), 24-bit or 48-bit (RGB), 
    /// 32-bit or 64-bit (RGBA), 16-bit (WA), 32-bit (Y FLOAT), 64-bit (YA FLOAT), 96-bit (RGB FLOAT), 128-bit (RGBA FLOAT), ...)</para>
    /// <para>For details on channels and colors see enumeration <see cref="EPamTuplType"/>.</para>
    /// </summary>
    P7_ARBITRARY_MAP_BINARY = 0x50ff,
    /// <summary>
    /// <para>Pixel data format : Portable FloatMap Gray BINARY format.</para>
    /// <para>File extension: '.pfm'.</para>
    /// <para>Header magic value: 'Pf'.</para>
    /// <para>Channels: 1 (Y)</para>
    /// <para>Colors: IEEE 754 float for each pixel value.</para>
    /// </summary>
    PF_FLOATMAP_GRAY_BINARY = 0x5066,
    /// <summary>
    /// <para>Pixel data format : Portable FloatMap Pix BINARY format.</para>
    /// <para>File extension: '.pfm'.</para>
    /// <para>Header magic value: 'PF'.</para>
    /// <para>Channels: 3 (RGB)</para>
    /// <para>Colors: IEEE 754 float for each RGB channel value.</para>
    /// </summary>
    PF_FLOATMAP_PIX_BINARY = 0x5046,
    /// <summary>
    /// <para>Pixel data format : Portable FloatMap Gray-Alpha BINARY format.</para>
    /// <para>File extension: '.pfm'.</para>
    /// <para>Header magic value: 'Pf4'.</para>
    /// <para>Channels: 2 (YA)</para>
    /// <para>Colors: IEEE 754 float for each YA channel value.</para>
    /// </summary>
    PF4_FLOATMAP_GRAY_ALPHA_BINARY = 0x506634,
    /// <summary>
    /// <para>Pixel data format : Portable FloatMap Pix-Alpha BINARY format.</para>
    /// <para>File extension: '.pfm'.</para>
    /// <para>Header magic value: 'PF4'.</para>
    /// <para>Channels: 4 (RGBA)</para>
    /// <para>Colors: IEEE 754 float for each RGBA channel value.</para>
    /// </summary>
    PF4_FLOATMAP_PIX_ALPHA_BINARY = 0x504634,
  }
}
