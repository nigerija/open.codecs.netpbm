﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace open.codecs.netpbm.types
{
  // Pixel structure(s)
  // not all of these structures are in use, shall try to document ones in use by this lib.
  // others are left on purpose if we need it somewhere else to convert between different formats.

  // TODO: validate big endian hardware !? ARMv6 big and below does anybody uses this?, above is little endian!

  // Grayscale pixel formats Y : U8, U16, U32, U48, U64  

  /// <summary>
  /// Represents pixel value for 8-bit grayscale (Y) image.
  /// <para>Used in : <see cref="PixelFormats.Gray8"/>, <see cref="EPmPixelFormat.Y_8bits_1u8"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_Y_1xU8
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 1;
    /// <summary>
    /// 8-bit grayscale component value.
    /// </summary>
    public byte Y;
  }

  /// <summary>
  /// Represents pixel value for 16-bit grayscale (Y) image.
  /// <para>Used in : <see cref="PixelFormats.Gray16"/>, <see cref="EPmPixelFormat.Y_16bits_1u16"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_Y_1xU16
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 2;
    /// <summary>
    /// 16-bit grayscale component value.
    /// </summary>
    public ushort Y;
  }

  // Convert_Y_1u32_to_Y_1u16_le

  /// <summary>
  /// Represents pixel value for 32-bit grayscale (Y) image in little-endian.
  /// <para>Used in : <see cref="EPmPixelFormat.Y_32bits_1u32"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_Y_1xU32_h16_le 
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 4;

    /// <summary>
    /// 32-bit grayscale component value.
    /// </summary>
    [FieldOffset(0)] public uint Y;
    /// <summary>
    /// Higher 16-bits of grayscale component value.
    /// </summary>
    [FieldOffset(2)] public ushort Yh16;
  }

  // Convert_Y_1u32_to_Y_1u16_be

  /// <summary>
  /// Represents pixel value for 32-bit grayscale (Y) image in big-endian.
  /// <para>Used in : <see cref="EPmPixelFormat.Y_32bits_1u32"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_Y_1xU32_h16_be
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 4;

    /// <summary>
    /// 32-bit grayscale component value.
    /// </summary>
    [FieldOffset(0)] public uint Y;
    /// <summary>
    /// Higher 16-bits of grayscale component value.
    /// </summary>
    [FieldOffset(0)] public ushort Yh16;
  }

  /// <summary>
  /// Represents pixel value for 48-bit grayscale (Y) image in little-endian.
  /// <para>Used in : <see cref="EPmPixelFormat.Y_48bits_1u48"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_Y_1xU48_h16_le
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 6;

    /// <summary>
    /// Grayscale (Y) lowest 32-bit UINT value (little-endian).
    /// </summary>
    [FieldOffset(0)] public uint Yl32;
    /// <summary>
    /// Grayscale (Y) highest 16-bit UINT value (little-endian).
    /// </summary>
    [FieldOffset(4)] public ushort Yh16;

  }

  /// <summary>
  /// Represents pixel value for 48-bit grayscale (Y) image in big-endian.
  /// <para>Used in : <see cref="EPmPixelFormat.Y_48bits_1u48"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_Y_1xU48_h16_be
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 6;

    /// <summary>
    /// Grayscale (Y) highest 16-bit UINT value (big-endian).
    /// </summary>
    [FieldOffset(0)] public ushort Yh16;
    /// <summary>
    /// Grayscale (Y) lowest 32-bit UINT value (big-endian).
    /// </summary>
    [FieldOffset(2)] public uint Yl32;
  }

  /// <summary>
  /// Represents pixel value for 64-bit grayscale (Y) image in little-endian.
  /// <para>Used in : <see cref="EPmPixelFormat.Y_64bits_1u64"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_Y_1xU64_h16_le
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 8;

    /// <summary>
    /// Grayscale (Y) 64-bit UINT value.
    /// </summary>
    [FieldOffset(0)] public ulong Y;
    /// <summary>
    /// Grayscale (Y) highest 16-bit UINT value (little-endian).
    /// </summary>
    [FieldOffset(6)] public ushort Yh16;
  }

  /// <summary>
  /// Represents pixel value for 64-bit grayscale (Y) image in big-endian.
  /// <para>Used in : <see cref="EPmPixelFormat.Y_64bits_1u64"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_Y_1xU64_h16_be
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 8;

    /// <summary>
    /// Grayscale (Y) 64-bit UINT value.
    /// </summary>
    [FieldOffset(0)] public ulong Y;
    /// <summary>
    /// Grayscale (Y) highest 16-bit UINT value (big-endian).
    /// </summary>
    [FieldOffset(0)] public ushort Yh16;

  }

  // Grayscale-Alpha pixel formats  YA : 2xU8, 2xU16, 2xU32, 2xU64, 1xF32

  /// <summary>
  /// Represents pixel value for 16-bit grayscale (Y) with alpha (A) image, where both components are 8-bit.
  /// <para>Used in : <see cref="EPmPixelFormat.YA_16bits_2u8"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_YA_2xU8
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 2;

    /// <summary>
    /// Grayscale (Y) component 8-bit UINT value.
    /// </summary>
    public byte Y;
    /// <summary>
    /// Alpha (A) component 8-bit UINT value.
    /// </summary>
    public byte A;
  }

  /// <summary>
  /// Represents pixel value for 32-bit grayscale (Y) with alpha (A) image, where both components are 16-bit UINT in little-endian, with following order : gray (Y) and alpha (A).
  /// <para>Used in : <see cref="EPmPixelFormat.YA_32bits_2u16"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_YA_2xU16_ou8_le
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 4;

    /// <summary>
    /// Grayscale (Y) component 16-bit UINT value.
    /// </summary>
    [FieldOffset(0)] public ushort Y;
    /// <summary>
    /// Grayscale (Y) component highest 8-bit UINT value (little-endian).
    /// </summary>
    [FieldOffset(1)] public byte Yh8;
    /// <summary>
    /// Alpha (A) component 16-bit UINT value.
    /// </summary>
    [FieldOffset(2)] public ushort A;
    /// <summary>
    /// Alpha (A) component highest 8-bit UINT value (little-endian).
    /// </summary>
    [FieldOffset(3)] public byte Ah8;
  }

  /// <summary>
  /// Represents pixel value for 32-bit grayscale (Y) with alpha (A) image, where both components are 16-bit UINT in big-endian, with following order : gray (Y) and alpha (A).
  /// <para>Used in : <see cref="EPmPixelFormat.YA_32bits_2u16"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_YA_2xU16_ou8_be
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 4;

    /// <summary>
    /// Grayscale (Y) component 16-bit UINT value.
    /// </summary>
    [FieldOffset(0)] public ushort Y;
    /// <summary>
    /// Grayscale (Y) component highest 8-bit UINT value (big-endian).
    /// </summary>
    [FieldOffset(0)] public byte Yh8;
    /// <summary>
    /// Alpha (A) component 16-bit UINT value.
    /// </summary>
    [FieldOffset(2)] public ushort A;
    /// <summary>
    /// Alpha (A) component highest 8-bit UINT value (big-endian).
    /// </summary>
    [FieldOffset(2)] public byte Ah8;
  }

  /// <summary>
  /// Represents pixel value for 64-bit grayscale (Y) with alpha (A) image, where both components are 32-bit UINT, with following order : gray (Y) and alpha (A).
  /// <para>Used in : <see cref="EPmPixelFormat.YA_64bits_2f32"/>.</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_YA_2xU32
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 8;

    /// <summary>
    /// Grayscale (Y) component 32-bit UINT value.
    /// </summary>
    public uint Y;
    /// <summary>
    /// Alpha (A) component 32-bit UINT value.
    /// </summary>
    public uint A;
  }

  /// <summary>
  /// Represents pixel value for 128-bit grayscale (Y) with alpha (A) image.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_YA_2xU64
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 16;

    /// <summary>
    /// Grayscale (Y) component 64-bit UINT value.
    /// </summary>
    public ulong Y;
    /// <summary>
    /// Alpha (A) component 64-bit UINT value.
    /// </summary>
    public ulong A;
  }

  /// <summary>
  /// Represents pixel value for 64-bit grayscale (Y) with alpha (A) image.
  /// Used in : <see cref="EPmPixelFormat.YA_64bits_2f32"/>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_YA_2xF32
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 8;

    /// <summary>
    /// Grayscale (Y) component 32-bit float value.
    /// </summary>
    public float Y;
    /// <summary>
    /// Alpha (A) component 32-bit float value.
    /// </summary>
    public float A;
  }

  // Color pixel formats RGB : 3xU8, 3xU16, 3xU32, 3xU64, 3xF32

  /// <summary>
  /// <para>Represents pixel value of 24-bit color RGB image, where each components is 8-bit UINT in following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="PixelFormats.Rgb24"/>, <see cref="EPmPixelFormat.RGB_24bits_3u8"/>.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xU8
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 3;

    /// <summary>
    /// Red (R) component 8-bit UINT value.
    /// </summary>
    public byte R;
    /// <summary>
    /// Green (G) component 8-bit UINT value.
    /// </summary>
    public byte G;
    /// <summary>
    /// Blue (B) component 8-bit UINT value.
    /// </summary>
    public byte B;
  }

  /// <summary>
  /// <para>Represents pixel value of 48-bit color RGB image, where each components is 16-bit UINT in following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="PixelFormats.Rgb48"/>, <see cref="EPmPixelFormat.RGB_48bits_3u16"/>.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xU16
  {
    /// <summary>
    /// Total size in bytes of this structure.
    /// </summary>
    public const int SizeOf = 6;

    /// <summary>
    /// Red (R) component 16-bit UINT value.
    /// </summary>
    public ushort R;
    /// <summary>
    /// Green (G) component 16-bit UINT value.
    /// </summary>
    public ushort G;
    /// <summary>
    /// Blue (B) component 16-bit UINT value.
    /// </summary>
    public ushort B;
  }

  /// <summary>
  /// <para>Represents pixel value of 96-bit color RGB image, where each components is 32-bit UINT in following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="EPmPixelFormat.RGB_96bits_3u32"/>.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xU32
  {
    /// <summary>
    /// Total size in bytes of this structure.
    /// </summary>
    public const int SizeOf = 12;

    /// <summary>
    /// Red (R) component 32-bit UINT value.
    /// </summary>
    public uint R;
    /// <summary>
    /// Green (G) component 32-bit UINT value.
    /// </summary>
    public uint G;
    /// <summary>
    /// Blue (B) component 32-bit UINT value.
    /// </summary>
    public uint B;
  }

  /// <summary>
  /// <para>Represents pixel value of 96-bit color RGB image, where each components is 32-bit UINT in little-endian, with following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="EPmPixelFormat.RGB_96bits_3u32"/>.
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xU32_h16_le
  {
    /// <summary>
    /// Total size in bytes of this structure.
    /// </summary>
    public const int SizeOf = 12;
    /// <summary>
    /// Red (R) component 32-bit UINT value.
    /// </summary>
    [FieldOffset(0)] public uint R;
    /// <summary>
    /// Red (R) component highest 16-bit UINT value (little-endian).
    /// </summary>
    [FieldOffset(2)] public ushort Rh16;
    /// <summary>
    /// Green (G) component 32-bit UINT value.
    /// </summary>
    [FieldOffset(4)] public uint G;
    /// <summary>
    /// Green (G) component highest 16-bit UINT value (little-endian).
    /// </summary>
    [FieldOffset(6)] public ushort Gh16;
    /// <summary>
    /// Blue (B) component 32-bit UINT value.
    /// </summary>
    [FieldOffset(8)] public uint B;
    /// <summary>
    /// Blue (B) component highest 16-bit UINT value (little-endian).
    /// </summary>
    [FieldOffset(10)] public ushort Bh16;
  }

  /// <summary>
  /// <para>Represents pixel value of 96-bit color RGB image, where each components is 32-bit UINT in big-endian, with following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="EPmPixelFormat.RGB_96bits_3u32"/>.
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xU32_h16_be
  {
    /// <summary>
    /// Total size in bytes of this structure.
    /// </summary>
    public const int SizeOf = 12;
    /// <summary>
    /// Red (R) component 32-bit UINT value.
    /// </summary>
    [FieldOffset(0)] public uint R;
    /// <summary>
    /// Red (R) component highest 16-bit UINT value (big-endian).
    /// </summary>
    [FieldOffset(0)] public ushort Rh16;
    /// <summary>
    /// Green (G) component 32-bit UINT value.
    /// </summary>
    [FieldOffset(4)] public uint G;
    /// <summary>
    /// Green (G) component highest 16-bit UINT value (big-endian).
    /// </summary>
    [FieldOffset(4)] public ushort Gh16;
    /// <summary>
    /// Blue (B) component 32-bit UINT value.
    /// </summary>
    [FieldOffset(8)] public uint B;
    /// <summary>
    /// Blue (B) component highest 16-bit UINT value (big-endian).
    /// </summary>
    [FieldOffset(8)] public ushort Bh16;
  }

  /// <summary>
  /// <para>Represents pixel value of 192-bit color RGB image, where each components is 64-bit UINT in following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="EPmPixelFormat.RGB_192bits_3u64"/>.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xU64
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 24;

    /// <summary>
    /// Red (R) 64-bit UINT component value.
    /// </summary>
    public ulong R;
    /// <summary>
    /// Green (G) 64-bit UINT component value.
    /// </summary>
    public ulong G;
    /// <summary>
    /// Blue (B) 64-bit UINT component value.
    /// </summary>
    public ulong B;
  }

  /// <summary>
  /// <para>Represents pixel value of 192-bit color RGB image, where each components is 64-bit UINT little-endian in following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="EPmPixelFormat.RGB_192bits_3u64"/>.
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xU64_ou16_le
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 24;

    /// <summary>
    /// Red (R) 64-bit UINT component value.
    /// </summary>
    [FieldOffset(0)] public ulong R;
    /// <summary>
    /// highest 16 bit of R component in little-endian
    /// </summary>
    [FieldOffset(6)] public ushort Rh16;
    /// <summary>
    /// Green (G) 64-bit UINT component value.
    /// </summary>
    [FieldOffset(8)] public ulong G;
    /// <summary>
    /// highest 16 bit of G component in little-endian
    /// </summary>
    [FieldOffset(14)] public ushort Gh16;
    /// <summary>
    /// Blue (B) 64-bit UINT component value.
    /// </summary>
    [FieldOffset(16)] public ulong B;
    /// <summary>
    /// highest 16 bit of B component in little-endian
    /// </summary>
    [FieldOffset(22)] public ushort Bh16;
  }

  /// <summary>
  /// <para>Represents pixel value of 192-bit color RGB image, where each components is 64-bit UINT big-endian in following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="EPmPixelFormat.RGB_192bits_3u64"/>.
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xU64_ou16_be
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 24;
    /// <summary>
    /// Red (R) 64-bit UINT component value.
    /// </summary>
    [FieldOffset(0)] public ulong R;
    /// <summary>
    /// highest 16 bit of R components in big-endian
    /// </summary>
    [FieldOffset(0)] public ushort Rh16;
    /// <summary>
    /// Green (G) 64-bit UINT component value.
    /// </summary>
    [FieldOffset(8)] public ulong G;
    /// <summary>
    /// highest 16 bit of R components in big-endian
    /// </summary>
    [FieldOffset(8)] public ushort Gh16;
    /// <summary>
    /// Blue (B) 64-bit UINT component value.
    /// </summary>
    [FieldOffset(16)] public ulong B;
    /// <summary>
    /// highest 16 bit of R components in big-endian
    /// </summary>
    [FieldOffset(16)] public ushort Bh16;
  }

  /// <summary>
  /// <para>Represents pixel value of 96-bit color RGB image, where each components is 32-bit FLOAT in following order : red (R), green (G), and blue (B).</para>
  /// Used in : <see cref="EPmPixelFormat.RGB_96bits_3f32"/>.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGB_3xF32
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 12;
    /// <summary>
    /// Red (R) 32-bit FLOAT component value.
    /// </summary>
    public float R;
    /// <summary>
    /// Green (G) 32-bit FLOAT component value.
    /// </summary>
    public float G;
    /// <summary>
    /// Blue (B) 32-bit FLOAT component value.
    /// </summary>
    public float B;
  }

  // Color-Alpha pixel formats RGBA : 4xU8, 4xU16

  /// <summary>
  /// <para>Represents pixel value of 32-bit color RGBA image, where each components is 8-bit UINT in following order : red (R), green (G), blue (B), and alpha (A).</para>
  /// Used in : <see cref="EPmPixelFormat.RGBA_32bits_4u8"/>.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGBA_4xU8
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 4;
    /// <summary>
    /// Red (R) 8-bit UINT component value.
    /// </summary>
    public byte R;
    /// <summary>
    /// Green (G) 8-bit UINT component value.
    /// </summary>
    public byte G;
    /// <summary>
    /// Blue (B) 8-bit UINT component value.
    /// </summary>
    public byte B;
    /// <summary>
    /// Alpha (A) 8-bit UINT component value.
    /// </summary>
    public byte A;
  }

  /// <summary>
  /// <para>Represents pixel value of 64-bit color RGBA image, where each components is 16-bit UINT in following order : red (R), green (G), blue (B), and alpha (A).</para>
  /// Used in : <see cref="PixelFormats.Prgba64"/>, <see cref="EPmPixelFormat.RGBA_64bits_4u16"/>.
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGBA_4xU16
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 8;
    /// <summary>
    /// Red (R) 16-bit UINT component value.
    /// </summary>
    public ushort R;
    /// <summary>
    /// Green (G) 16-bit UINT component value.
    /// </summary>
    public ushort G;
    /// <summary>
    /// Blue (B) 16-bit UINT component value.
    /// </summary>
    public ushort B;
    /// <summary>
    /// Alpha (A) 16-bit UINT component value.
    /// </summary>
    public ushort A;
  }

  /// <summary>
  /// <para>Represents pixel value of 128-bit color RGBA image, where each components is 32-bit UINT in following order : red (R), green (G), blue (B), and alpha (A).</para>
  /// Used in : .
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGBA_4xU32
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 16;
    /// <summary>
    /// Red (R) 32-bit UINT component value.
    /// </summary>
    public uint R;
    /// <summary>
    /// Green (G) 32-bit UINT component value.
    /// </summary>
    public uint G;
    /// <summary>
    /// Blue (B) 32-bit UINT component value.
    /// </summary>
    public uint B;
    /// <summary>
    /// Alpha (A) 32-bit UINT component value.
    /// </summary>
    public uint A;
  }

  /// <summary>
  /// <para>Represents pixel value of 256-bit color RGBA image, where each components is 64-bit float in following order : red (R), green (G), blue (B), and alpha (A).</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGBA_4xU64
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 32;
    /// <summary>
    /// Red (R) 64-bit UINT component value.
    /// </summary>
    public ulong R;
    /// <summary>
    /// Green (G) 64-bit UINT component value.
    /// </summary>
    public ulong G;
    /// <summary>
    /// Blue (B) 64-bit UINT component value.
    /// </summary>
    public ulong B;
    /// <summary>
    /// Alpha (A) 64-bit UINT component value.
    /// </summary>
    public ulong A;
  }

  /// <summary>
  /// <para>Represents pixel value of 128-bit color RGBA image, where each components is 32-bit float in following order : red (R), green (G), blue (B), and alpha (A).</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGBA_4xF32
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 16;
    /// <summary>
    /// Red (R) 32-bit FLOAT component value.
    /// </summary>
    public float R;
    /// <summary>
    /// Green (G) 32-bit FLOAT component value.
    /// </summary>
    public float G;
    /// <summary>
    /// Blue (B) 32-bit FLOAT component value.
    /// </summary>
    public float B;
    /// <summary>
    /// Alpha (A) 32-bit FLOAT component value.
    /// </summary>
    public float A;
  }

  // Color-Alpha (Reverse color channels order) pixel formats BGRA : 4xU8

  /// <summary>
  /// <para>Represents pixel value of 32-bit color BGRA image, where each components is 8-bit UINT in following order : blue (B), green (G), red (R), and alpha (A).</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_BGRA_4xU8
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 4;
    /// <summary>
    /// Blue (B) 8-bit UINT component value.
    /// </summary>
    public byte B;
    /// <summary>
    /// Green (G) 8-bit UINT component value.
    /// </summary>
    public byte G;
    /// <summary>
    /// Red (R) 8-bit UINT component value.
    /// </summary>
    public byte R;
    /// <summary>
    /// Alpha (A) 8-bit UINT component value.
    /// </summary>
    public byte A;
  }

  /// <summary>
  /// <para>Represents pixel value of 64-bit color BGRA image, where each components is 16-bit UINT in following order : blue (B), green (G), red (R), and alpha (A).</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_BGRA_4xU16
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 8;
    /// <summary>
    /// Blue (B) 16-bit UINT component value.
    /// </summary>
    public ushort B;
    /// <summary>
    /// Green (G) 16-bit UINT component value.
    /// </summary>
    public ushort G;
    /// <summary>
    /// RED (R) 16-bit UINT component value.
    /// </summary>
    public ushort R;
    /// <summary>
    /// Alpha (A) 16-bit UINT component value.
    /// </summary>
    public ushort A;
  }

  // Color + 1 empty channel pixel formats  RGBx : 4xF32

  /// <summary>
  /// <para>Represents pixel value of 128-bit color RGBx image, where each components is 32-bit float in following order : red (R), green (G), blue (B), unused (x).</para>
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1, Size = SizeOf)]
  public struct T_PIXEL_RGBx_4xF32
  {
    /// <summary>
    /// Total size in bytes of this structure
    /// </summary>
    public const int SizeOf = 16;
    /// <summary>
    /// Red (R) 32-bit FLOAT component value.
    /// </summary>
    public float R;
    /// <summary>
    /// Green (G) 32-bit FLOAT component value.
    /// </summary>
    public float G;
    /// <summary>
    /// Blue (B) 32-bit FLOAT component value.
    /// </summary>
    public float B;
    /// <summary>
    /// Unused (X) 32-bit FLOAT component value.
    /// </summary>
    public float X;
  }
  
}
