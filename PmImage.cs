﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm
{
  /// <summary>
  /// Encapsulates Portable Any Type image, which consists of the pixel data and its attributes.
  /// </summary>
  public class PmImage
  {
    /// <summary>
    /// A file type recognized on read
    /// </summary>
    public EPmFileType FileType;
    
    /// <summary>
    /// Image width
    /// </summary>
    public int ImageWidth;
    
    /// <summary>
    /// Image height
    /// </summary>
    public int ImageHeight;
    
    /// <summary>
    /// Image MAXVAL
    /// </summary>
    public ulong ImageMaxVal;
    
    /// <summary>
    /// Image MAXVAL in float, when format is float.
    /// </summary>
    public float ImageMaxValFloat;
    
    /// <summary>
    /// setting this value before calling read shall hint reader with user pixel format.
    /// </summary>
    public EPmPixelFormat PixelFormat;
    
    /// <summary>
    /// Gets Image bits per pixel
    /// </summary>
    public int BitsPerPixel { get; internal set; }

    /// <summary>
    /// Gets Image bits per Channel
    /// </summary>
    public int BitsPerChannel { get; internal set; }

    /// <summary>
    /// Returns the number of pixels
    /// </summary>
    public int ImagePixelCount { get => ImageHeight * ImageWidth; }

    /// <summary>
    /// Image pixel data
    /// <para>NOTE: image data is 2D matrix, where the first line in matrix represents the top line in the image; This is not common to Windows Bitmap where bitmap first data line is actually bottom image line.</para>
    /// 
    /// </summary>
    public byte[] ImagePixelData;

    /// <summary>
    /// The list of comment read from file or to be written to file.
    /// </summary>
    public List<string> Comments = new List<string>();

    /// <summary>
    /// PAM extension: 
    /// The depth attribute defines the number of channels in the image, such as:
    /// <para>1 for grayscale (Y) images, or bitmap (W) images;</para>
    /// <para>2 for grayscale with alpha (YA) images, or bitmap with alpha (WA) images;</para>
    /// <para>3 for color (RGB) images;</para>
    /// <para>4 for color with alpha (RGBA) images;</para>
    /// </summary>
    internal int ImageDepth;

    /// <summary>
    /// PAM extension: 
    /// The tuple type attribute specifies what kind of image the PAM file represents,
    /// thus enabling it to stand for the older NETPBM formats, as well as to be extended to new uses, e.g., transparency.
    /// </summary>
    internal EPamTuplType ImageTuplType;

    /// <summary>
    /// PFM extension: 
    /// 'PF', 'Pf', 'PF4', 'Pf4', when big endian is required ?
    /// </summary>
    public bool ImageDataLittleEndian = true;

    /// <summary>
    /// Returns string representation of <see cref="PmImage"/> object.
    /// </summary>
    /// <returns>
    /// Returns string representation this object.
    /// </returns>
    public override string ToString()
    {
      return $@"FileType : {FileType}, ImageWidth : {ImageWidth}, ImageHeight : {ImageHeight}, ImageMaxVal : {ImageMaxVal}, PixelFormat : {PixelFormat}, BytesPerPixel : {BitsPerPixel}, ImageData : {(null != ImagePixelData ? ImagePixelData.Length : -1)}";
    }

    //const ulong UInt48_MaxValue = (((ulong)1) << 48) - 1;

    /// <summary>
    /// Returns <see cref="EPmPixelFormat"/> value best guessed from <see cref="PmImage.FileType"/> and <see cref="PmImage.ImageMaxVal"/> members.
    /// </summary>
    /// <returns>Returns a <see cref="EPmPixelFormat"/> value.</returns>
    [MethodImpl(256)]
    public EPmPixelFormat GetPixelFormat()
    {
      ulong maxVal = ImageMaxVal;
      switch (FileType)
      {
        case EPmFileType.P1_BITMAP_ASCII:
        case EPmFileType.P4_BITMAP_BINARY:

          return EPmPixelFormat.W_1bit_1u1;           // 1-bit,   1/8 byte

        case EPmFileType.P2_GRAYMAP_ASCII:
        case EPmFileType.P5_GRAYMAP_BINARY:

          if (maxVal <= byte.MaxValue)                // 8-bits,  1 byte
          {
            return EPmPixelFormat.Y_8bits_1u8;
          }
          else if (maxVal <= ushort.MaxValue)         // 16-bits, 2 bytes
          {
            return EPmPixelFormat.Y_16bits_1u16;
          }
          else if (maxVal <= uint.MaxValue)           // 32-bits, 4 bytes
          {
            return EPmPixelFormat.Y_32bits_1u32;
          }
          else if (maxVal <= UInt48.UInt48MaxValue)   // 48-bits, 6 bytes
          {
            return EPmPixelFormat.Y_48bits_1u48;
          }
          return EPmPixelFormat.Y_64bits_1u64;        // 64-bits, 8 bytes

        case EPmFileType.P3_PIXMAP_ASCII:
        case EPmFileType.P6_PIXMAP_BINARY:

          if (maxVal <= byte.MaxValue)                  // 24-bits, 3 bytes
          {
            return EPmPixelFormat.RGB_24bits_3u8;
          }
          else if (maxVal <= ushort.MaxValue)           // 48-bits, 6 bytes
          {
            return EPmPixelFormat.RGB_48bits_3u16;
          }
          else if (maxVal <= uint.MaxValue)             // 96-bits, 12 bytes
          {
            return EPmPixelFormat.RGB_96bits_3u32;
          }
          return EPmPixelFormat.RGB_192bits_3u64;     // 128-bits, 16 bytes

        case EPmFileType.PF_FLOATMAP_GRAY_BINARY:

          return EPmPixelFormat.Y_32bits_1f32;        // 32-bit, 4 bytes

        case EPmFileType.PF_FLOATMAP_PIX_BINARY:

          return EPmPixelFormat.RGB_96bits_3f32;      // 96-bit, 12 bytes

        case EPmFileType.PF4_FLOATMAP_GRAY_ALPHA_BINARY:

          return EPmPixelFormat.YA_64bits_2f32;       // 64-bit, 8 bytes

        case EPmFileType.PF4_FLOATMAP_PIX_ALPHA_BINARY:

          return EPmPixelFormat.RGBA_128bits_4f32;    // 128-bit, 16 bytes

        case EPmFileType.P7_ARBITRARY_MAP_BINARY:

          #region ### Standard (mapped to previous file type handlers) ###
          if (ImageTuplType == EPamTuplType.BLACKANDWHITE)
          {
            // 1-bit,   1/8 bytes
            goto case EPmFileType.P4_BITMAP_BINARY;
          }
          else if (ImageTuplType == EPamTuplType.GRAYSCALE)
          {
            // 8-bits,  1 byte
            // 16-bits, 2 bytes
            goto case EPmFileType.P5_GRAYMAP_BINARY;
          }
          else if (ImageTuplType == EPamTuplType.RGB)
          {
            // 24-bits, 3 bytes
            // 48-bits, 6 bytes
            goto case EPmFileType.P6_PIXMAP_BINARY;
          }
          #endregion
          #region ### Standard (PAM only) ###
          else if (ImageTuplType == EPamTuplType.BLACKANDWHITE_ALPHA)
          {
            // NOTE: does not exist in other formats (can't be mapped).
            // 16-bits, 2 bytes
            return EPmPixelFormat.WA_16bits_2u8;
          }
          else if (ImageTuplType == EPamTuplType.GRAYSCALE_ALPHA)
          {
            // NOTE: does not exist in other formats (can't be mapped).
            if (maxVal <= byte.MaxValue)
              return EPmPixelFormat.YA_16bits_2u8;    // 16-bits, 2 bytes
            return EPmPixelFormat.YA_32bits_2u16;     // 32-bits, 4 bytes
          }
          else if (ImageTuplType == EPamTuplType.RGB_ALPHA)
          {
            // NOTE: does not exist in other formats (can't be mapped).
            if (maxVal <= byte.MaxValue)
              return EPmPixelFormat.RGBA_32bits_4u8;  // 32-bits, 4 bytes
            return EPmPixelFormat.RGBA_64bits_4u16;   // 64-bits, 8 bytes
          }
          #endregion
          #region ### Non-Standard (FLOAT-mapped to previous file type handlers) ###
          else if (ImageTuplType == EPamTuplType.FLOAT_GRAYSCALE)
          {
            // 32-bits, 4 bytes
            goto case EPmFileType.PF_FLOATMAP_GRAY_BINARY;
          }
          else if (ImageTuplType == EPamTuplType.FLOAT_GRAYSCALE_ALPHA)
          {
            // 64-bits, 8 bytes
            goto case EPmFileType.PF4_FLOATMAP_GRAY_ALPHA_BINARY;
          }
          else if (ImageTuplType == EPamTuplType.FLOAT_RGB)
          {
            // 96-bits, 12 bytes
            goto case EPmFileType.PF_FLOATMAP_PIX_BINARY;
          }
          else if (ImageTuplType == EPamTuplType.FLOAT_RGB_ALPHA)
          {
            // 128-bits, 16 bytes
            goto case EPmFileType.PF4_FLOATMAP_PIX_ALPHA_BINARY;
          }
          #endregion
          // unknown format or just unsupported atm
          break;
        case EPmFileType.UNKNOWN:
          // unknown format or just unsupported atm
          break;
      }
      // otherwise invalid and unsupported
      return EPmPixelFormat.Unknown;
    }

    /// <summary>
    /// Returns true if <see cref="PixelFormat"/> is one of the float formats; Otherwise, returns false.
    /// </summary>
    public bool IsPixelFormatFloat
    {
      [MethodImpl(256)]
      get
      {
        switch(PixelFormat)
        {
          case EPmPixelFormat.RGBA_128bits_4f32:
          case EPmPixelFormat.RGB_96bits_3f32:
          case EPmPixelFormat.YA_64bits_2f32:
          case EPmPixelFormat.Y_32bits_1f32:
            return true;
        }
        return false;
      }
    }

    /// <summary>
    /// Returns true if <see cref="FileType"/> is arbitrary map file format.
    /// </summary>
    public bool IsFileTypeP7ArbitratyMap
    {
      [MethodImpl(256)]
      get
      {
        return FileType == EPmFileType.P7_ARBITRARY_MAP_BINARY;
      }
    }

    /// <summary>
    /// Returns true if <see cref="FileType"/> is one of ASCII file formats.
    /// </summary>
    public bool IsFileTypeAscii
    {
      [MethodImpl(256)]
      get
      {
        switch(FileType)
        {
          case EPmFileType.P1_BITMAP_ASCII:
          case EPmFileType.P2_GRAYMAP_ASCII:
          case EPmFileType.P3_PIXMAP_ASCII:
            return true;
        }
        return false;
      }
    }


    [MethodImpl(256)]
    private static ulong CalculateBitsMaxValue(int bits)
    {
      if (bits >= 64)
        return ulong.MaxValue;
      return (((ulong)1) << bits) - 1;
    }

    [MethodImpl(256)]
    private static float CalculateBitsMaxValueFloat(int bits)
    {
      // TODO: not so correct does not include negative range.
      //       plus division not correct at all.
      if (bits >= 32)
        return float.MaxValue;
      return float.MaxValue / (1 << bits);
    }

    /// <summary>
    /// Returns the MAXVAL value multiplier for specified <paramref name="targetChannelBits"/> bits.
    /// </summary>
    /// <param name="targetChannelBits">A number of bits per target channel.</param>
    /// <returns>Returns a double number.</returns>
    public double GetMaxValMultiplier(int targetChannelBits)
    {
      ulong targetChannelBitsMaxValue = CalculateBitsMaxValue(targetChannelBits);
      double targetChannelBitsMaxValueFloat = CalculateBitsMaxValueFloat(targetChannelBits);
      switch (FileType)
      {
        case EPmFileType.P1_BITMAP_ASCII:
        case EPmFileType.P4_BITMAP_BINARY:
          // 1-bit,   1/8 byte 
          return ((double)targetChannelBitsMaxValue / ImageMaxVal);

        case EPmFileType.P2_GRAYMAP_ASCII:
        case EPmFileType.P5_GRAYMAP_BINARY:

          // 8-bits,  1 byte
          // 16-bits, 2 bytes
          // 32-bits, 4 bytes
          // 48-bits, 6 bytes
          // 64-bits, 8 bytes
          return ((double)targetChannelBitsMaxValue / ImageMaxVal);

        case EPmFileType.P3_PIXMAP_ASCII:
        case EPmFileType.P6_PIXMAP_BINARY:

          // 24-bits, 3 bytes
          // 48-bits, 6 bytes
          // 96-bits, 12 bytes
          // 128-bits, 16 bytes
          return ((double)targetChannelBitsMaxValue / ImageMaxVal);

        case EPmFileType.PF_FLOATMAP_GRAY_BINARY:

          // 32-bit, 4 bytes
          return (targetChannelBitsMaxValueFloat / ImageMaxValFloat);

        case EPmFileType.PF_FLOATMAP_PIX_BINARY:

          // 96-bit, 12 bytes
          return (targetChannelBitsMaxValueFloat / ImageMaxValFloat);

        case EPmFileType.PF4_FLOATMAP_GRAY_ALPHA_BINARY:

          // 64-bit, 8 bytes
          return (targetChannelBitsMaxValueFloat / ImageMaxValFloat);

        case EPmFileType.PF4_FLOATMAP_PIX_ALPHA_BINARY:

          // 128-bit, 16 bytes
          return (targetChannelBitsMaxValueFloat / ImageMaxValFloat);

        case EPmFileType.P7_ARBITRARY_MAP_BINARY:
          // map to above cases
          #region ### Standard ###
          if (ImageTuplType == EPamTuplType.BLACKANDWHITE)
          {
            // 1-bit,   1/8 bytes
            goto case EPmFileType.P1_BITMAP_ASCII;
          }
          else if (ImageTuplType == EPamTuplType.BLACKANDWHITE_ALPHA)
          {
            // 16-bits, 2 bytes
            goto case EPmFileType.P1_BITMAP_ASCII; // ?
          }
          else if (ImageTuplType == EPamTuplType.GRAYSCALE)
          {
            // 8-bits,  1 byte
            // 16-bits, 2 bytes
            goto case EPmFileType.P2_GRAYMAP_ASCII;
          }
          else if (ImageTuplType == EPamTuplType.GRAYSCALE_ALPHA)
          {
            // 16-bits, 2 bytes (channels : YA)
            // 32-bits, 4 bytes (channels : YA)
            goto case EPmFileType.P2_GRAYMAP_ASCII;
          }
          else if (ImageTuplType == EPamTuplType.RGB)
          {
            // 24-bits, 3 bytes
            // 48-bits, 6 bytes
            goto case EPmFileType.P3_PIXMAP_ASCII;
          }
          else if (ImageTuplType == EPamTuplType.RGB_ALPHA)
          {
            // 32-bits, 4 bytes (channels : RGBA)
            // 64-bits, 8 bytes (channels : RGBA)
            goto case EPmFileType.P3_PIXMAP_ASCII;
          }
          #endregion
          #region ### Non-Standard (FLOAT) ###
          else if (ImageTuplType == EPamTuplType.FLOAT_GRAYSCALE)
          {
            // 32-bits, 4 bytes (channels: Y)
            goto case EPmFileType.PF_FLOATMAP_GRAY_BINARY;
          }
          else if (ImageTuplType == EPamTuplType.FLOAT_RGB)
          {
            // 96-bits, 12 bytes (channels: RGB)
            goto case EPmFileType.PF_FLOATMAP_PIX_BINARY;
          }
          else if (ImageTuplType == EPamTuplType.FLOAT_GRAYSCALE_ALPHA)
          {
            // 64-bits, 8 bytes (channels: YA)
            goto case EPmFileType.PF4_FLOATMAP_GRAY_ALPHA_BINARY;
          }
          else if (ImageTuplType == EPamTuplType.FLOAT_RGB_ALPHA)
          {
            // 128-bits, 16 bytes (channels: RGBA)
            goto case EPmFileType.PF4_FLOATMAP_PIX_ALPHA_BINARY;
          }
          #endregion
          // unknown format or just unsupported atm
          break;
        case EPmFileType.UNKNOWN:
          // unknown format or just unsupported atm
          break;
      }
      // otherwise invalid and unsupported
      return 0;
    }

    /// <summary>
    /// Validates pixel format if <see cref="PixelFormat"/> is <see cref="EPmPixelFormat.Unknown"/> by setting <see cref="PixelFormat"/> member with result from <see cref="GetPixelFormat"/> method.
    /// </summary>
    /// <returns>Returns resolved pixel format.</returns>
    [MethodImpl(256)]
    public EPmPixelFormat ValidatePixelFormat()
    {
      // try guess pixel format if unknown
      if (PixelFormat == EPmPixelFormat.Unknown)
      {
        PixelFormat = GetPixelFormat();
      }
      return PixelFormat;
    }

    /// <summary>
    /// Returns the number of pixels.
    /// </summary>    
    [MethodImpl(256)]
    internal int GetPixelCount()
    {
      return ImageHeight * ImageWidth;
    }

    /// <summary>
    /// Returns the number of elements (example: image RGB_48bits (width:10, height:10) has 10 * 10 * 3 = 300 elements of UInt48)
    /// </summary>
    [MethodImpl(256)]
    internal int GetElementsCount()
    {
      return (GetPixelCount() * GetPixelComponents());
    }

    /// <summary>
    /// Gets pixel size in bits from <see cref="PixelFormat"/> member, if member not set it will try to best guess the <see cref="EPmPixelFormat"/> (see <see cref="GetPixelFormat()"/>).
    /// </summary>
    /// <returns>Returns the size of pixel in bits.</returns>
    public int GetPixelBitsSize()
    {
      EPmPixelFormat pixelFormat = ValidatePixelFormat();
      switch (pixelFormat)
      {
        case EPmPixelFormat.W_1bit_1u1:
          return 1;
        case EPmPixelFormat.Y_8bits_1u8:
          return 8;
        case EPmPixelFormat.WA_16bits_2u8:
        case EPmPixelFormat.YA_16bits_2u8:
        case EPmPixelFormat.Y_16bits_1u16:
          return 16;
        case EPmPixelFormat.RGB_24bits_3u8:
          return 24;
        case EPmPixelFormat.YA_32bits_2u16:
        case EPmPixelFormat.Y_32bits_1u32:
        case EPmPixelFormat.Y_32bits_1f32:
        case EPmPixelFormat.RGBA_32bits_4u8:
          return 32;
        case EPmPixelFormat.Y_48bits_1u48:
        case EPmPixelFormat.RGB_48bits_3u16:
          return 48;
        case EPmPixelFormat.Y_64bits_1u64:
        case EPmPixelFormat.YA_64bits_2f32:
        case EPmPixelFormat.RGBA_64bits_4u16:
          return 64;
        case EPmPixelFormat.RGB_96bits_3u32:
        case EPmPixelFormat.RGB_96bits_3f32:
          return 96;
        case EPmPixelFormat.RGBA_128bits_4f32:
          return 128;
        case EPmPixelFormat.RGB_192bits_3u64:
          return 192;
      }
      throw new NotSupportedException($"GetPixelBitsSize: unsupported pixel format '{pixelFormat}'");
    }

    /// <summary>
    /// Gets channel size in bits from <see cref="PixelFormat"/> member, if member not set it will try to best guess the <see cref="EPmPixelFormat"/> (see <see cref="GetPixelFormat()"/>).
    /// </summary>
    /// <returns>Returns the size of channel in bits.</returns>
    public int GetChannelBitsSize()
    {
      EPmPixelFormat pixelFormat = ValidatePixelFormat();
      switch (pixelFormat)
      {
        case EPmPixelFormat.W_1bit_1u1:
          return 1;
        case EPmPixelFormat.Y_8bits_1u8:
        case EPmPixelFormat.WA_16bits_2u8:
        case EPmPixelFormat.YA_16bits_2u8:
        case EPmPixelFormat.RGB_24bits_3u8:
        case EPmPixelFormat.RGBA_32bits_4u8:
          return 8;
        case EPmPixelFormat.Y_16bits_1u16:
        case EPmPixelFormat.YA_32bits_2u16:
        case EPmPixelFormat.RGB_48bits_3u16:
        case EPmPixelFormat.RGBA_64bits_4u16:
          return 16;
        case EPmPixelFormat.Y_32bits_1u32:
        case EPmPixelFormat.Y_32bits_1f32:
        case EPmPixelFormat.YA_64bits_2f32:
        case EPmPixelFormat.RGB_96bits_3u32:
        case EPmPixelFormat.RGB_96bits_3f32:
        case EPmPixelFormat.RGBA_128bits_4f32:
          return 32;
        case EPmPixelFormat.Y_48bits_1u48:
          return 48;
        case EPmPixelFormat.Y_64bits_1u64:
        case EPmPixelFormat.RGB_192bits_3u64:
          return 64;
      }
      throw new NotSupportedException($"GetChannelBitsSize: unsupported pixel format '{pixelFormat}'");
    }

    /// <summary>
    /// Gets pixel number of components from <see cref="PixelFormat"/> member, 
    /// if member not set it will try to best guess the <see cref="EPmPixelFormat"/> (see <see cref="GetPixelFormat()"/>).
    /// </summary>
    /// <returns>Returns the size of pixel in bytes.</returns>
    [MethodImpl(256)]
    public int GetPixelComponents()
    {
      EPmPixelFormat pixelFormat = ValidatePixelFormat();
      switch (pixelFormat)
      {
        case EPmPixelFormat.W_1bit_1u1:
        case EPmPixelFormat.Y_8bits_1u8:
        case EPmPixelFormat.Y_16bits_1u16:
        case EPmPixelFormat.Y_32bits_1u32:
        case EPmPixelFormat.Y_32bits_1f32:
        case EPmPixelFormat.Y_48bits_1u48:
        case EPmPixelFormat.Y_64bits_1u64:
          return 1;
        case EPmPixelFormat.WA_16bits_2u8:
        case EPmPixelFormat.YA_16bits_2u8:
        case EPmPixelFormat.YA_32bits_2u16:
        case EPmPixelFormat.YA_64bits_2f32:
          return 2;
        case EPmPixelFormat.RGB_24bits_3u8:
        case EPmPixelFormat.RGB_48bits_3u16:
        case EPmPixelFormat.RGB_96bits_3u32:
        case EPmPixelFormat.RGB_96bits_3f32:
        case EPmPixelFormat.RGB_192bits_3u64:
          return 3;
        case EPmPixelFormat.RGBA_32bits_4u8:
        case EPmPixelFormat.RGBA_64bits_4u16:
        case EPmPixelFormat.RGBA_128bits_4f32:
          return 4;
      }
      throw new NotSupportedException($"GetPixelComponents: unsupported pixel format '{pixelFormat}'");
    }

    /// <summary>
    /// Returns the number of bytes required to hold image data.
    /// </summary>
    /// <returns>Returns the positive number if number of bytes could be calculated; Otherwise -1.</returns>
    public int GetPixelDataSize()
    {
      int elements = GetElementsCount();
      EPmPixelFormat pixelFormat = ValidatePixelFormat();
      switch (pixelFormat)
      {
        case EPmPixelFormat.W_1bit_1u1:
          // are bits
          return ((elements + 7) / 8);          // * sizeof(bit) // byte aligned
        case EPmPixelFormat.RGBA_32bits_4u8:
        case EPmPixelFormat.RGB_24bits_3u8:
        case EPmPixelFormat.WA_16bits_2u8:
        case EPmPixelFormat.YA_16bits_2u8:
        case EPmPixelFormat.Y_8bits_1u8:
          // array of uint8
          return (elements);                    // * sizeof(byte)
        case EPmPixelFormat.RGBA_64bits_4u16:
        case EPmPixelFormat.RGB_48bits_3u16:
        case EPmPixelFormat.YA_32bits_2u16:
        case EPmPixelFormat.Y_16bits_1u16:
          // array of uint16
          return (elements << 1);               // * sizeof(ushort)
        case EPmPixelFormat.RGB_96bits_3u32:
        case EPmPixelFormat.Y_32bits_1u32:
        case EPmPixelFormat.Y_32bits_1f32:
          // array of uint32
          return (elements << 2);               // * sizeof(uint)
        case EPmPixelFormat.RGBA_128bits_4f32:
        case EPmPixelFormat.RGB_96bits_3f32:
          // array of float32
          return (elements << 2);               // * sizeof(float)
        case EPmPixelFormat.Y_48bits_1u48:
          // array of uint48
          return ((elements << 2) + (elements << 1));
        case EPmPixelFormat.RGB_192bits_3u64:
        case EPmPixelFormat.Y_64bits_1u64:
        case EPmPixelFormat.YA_64bits_2f32:
          // array of uint64
          return (elements << 3);               // * sizeof(ulong)
      }
      throw new NotSupportedException($"GetPixelDataSize: unsupported pixel format '{pixelFormat}'");
    }

    /// <summary>
    /// Gets the positive number of bytes required for single line of pixels, if pixel size is defined; Otherwise returns -1.
    /// <para>See <see cref="GetPixelBitsSize"/> method for more details.</para>
    /// </summary>
    public int Stride
    {
      [MethodImpl(256)]
      get
      {
        return StrideBits <= 0 ? -1 : (StrideBits >> 3);
      }
    }

    /// <summary>
    /// Gets the positive number of bits required for single line of pixels, if pixel size is defined; Otherwise returns -1.
    /// <para>See <see cref="GetPixelBitsSize"/> method for more details.</para>
    /// </summary>
    public int StrideBits
    {
      [MethodImpl(256)]
      get
      {
        int pixelsSizeBits = GetPixelBitsSize();
        return pixelsSizeBits <= 0 ? -1 : ((pixelsSizeBits * ImageWidth) + 7);
      }
    }

    /// <summary>
    /// Initializes new memory for <see cref="ImagePixelData"/> member.
    /// </summary>
    /// <returns>Returns true if new memory buffer created; Otherwise false.</returns>
    public bool CreatePixelBuffer()
    {
      int sizeInBytes = GetPixelDataSize();
      if (sizeInBytes <= 0)
      {
        return false;
      }
      ImagePixelData = new byte[sizeInBytes];
      return true;
    }

    /// <summary>
    /// not-allowed outside
    /// </summary>
    internal PmImage()
    {

    }

    /// <summary>
    /// Creates new instance of <see cref="PmImage"/> class with specified parameters.
    /// </summary>
    /// <param name="fileType">A <see cref="EPmFileType"/> format to be used when saving the image.</param>
    /// <param name="width">A width for new image.</param>
    /// <param name="height">A height for new image.</param>
    /// <param name="pixelFormat">A <see cref="EPmPixelFormat"/> size for new image.</param>
    /// <param name="maxValue">A max value for UINT types to be set (otherwise it shall be calculated from a <paramref name="pixelFormat"/> value).</param>
    /// <param name="maxValueFloat">A max value for FLOAT types to be set (otherwise it shall be set to value of 1f).</param>
    public PmImage(EPmFileType fileType, int width, int height, EPmPixelFormat pixelFormat, ulong maxValue = 0, float maxValueFloat = 1f)
    {
      FileType = fileType;
      ImageWidth = width;
      ImageHeight = height;
      PixelFormat = pixelFormat;
      BitsPerPixel = GetPixelBitsSize();
      BitsPerChannel = BitsPerPixel / GetPixelComponents();
      ImageMaxVal = maxValue != 0 ? maxValue : CalculateBitsMaxValue(BitsPerChannel);
      ImageMaxValFloat = maxValueFloat != 0 ? maxValueFloat : CalculateBitsMaxValueFloat(BitsPerChannel);

      switch (fileType)
      {
        case EPmFileType.P7_ARBITRARY_MAP_BINARY:
          // if user requested for file type PAM, set TuplType from pixel format
          switch (pixelFormat)
          {
            #region ### Standard (OPAQUE) ###
            case EPmPixelFormat.W_1bit_1u1:
              ImageTuplType = EPamTuplType.BLACKANDWHITE;
              break;
            case EPmPixelFormat.Y_8bits_1u8:
            case EPmPixelFormat.Y_16bits_1u16:
            case EPmPixelFormat.Y_32bits_1u32:
            case EPmPixelFormat.Y_48bits_1u48:
            case EPmPixelFormat.Y_64bits_1u64:
              ImageTuplType = EPamTuplType.GRAYSCALE;
              break;
            case EPmPixelFormat.RGB_24bits_3u8:
            case EPmPixelFormat.RGB_48bits_3u16:
            case EPmPixelFormat.RGB_96bits_3u32:
            case EPmPixelFormat.RGB_192bits_3u64:
              ImageTuplType = EPamTuplType.RGB;
              break;
            #endregion
            #region ### Standard (ALPHA) ###
            case EPmPixelFormat.WA_16bits_2u8:
              ImageTuplType = EPamTuplType.BLACKANDWHITE_ALPHA;
              break;
            case EPmPixelFormat.YA_16bits_2u8:
            case EPmPixelFormat.YA_32bits_2u16:
              ImageTuplType = EPamTuplType.GRAYSCALE_ALPHA;
              break;
            case EPmPixelFormat.RGBA_32bits_4u8:
            case EPmPixelFormat.RGBA_64bits_4u16:
              ImageTuplType = EPamTuplType.RGB_ALPHA;
              break;
            #endregion
            #region ### Non-Standard (FLOAT) ###
            case EPmPixelFormat.Y_32bits_1f32:
              ImageTuplType = EPamTuplType.FLOAT_GRAYSCALE;
              break;
            case EPmPixelFormat.YA_64bits_2f32:
              ImageTuplType = EPamTuplType.FLOAT_GRAYSCALE_ALPHA;
              break;
            case EPmPixelFormat.RGB_96bits_3f32:
              ImageTuplType = EPamTuplType.FLOAT_RGB;
              break;
            case EPmPixelFormat.RGBA_128bits_4f32:
              ImageTuplType = EPamTuplType.FLOAT_RGB_ALPHA;
              break;
            #endregion
            default:
              throw (new ArgumentOutOfRangeException($"{nameof(pixelFormat)} is out of range, please check arguments"));
          }
          break;
        case EPmFileType.PF_FLOATMAP_GRAY_BINARY:
        case EPmFileType.PF_FLOATMAP_PIX_BINARY:
        case EPmFileType.PF4_FLOATMAP_GRAY_ALPHA_BINARY:
        case EPmFileType.PF4_FLOATMAP_PIX_ALPHA_BINARY:
          ImageMaxValFloat = 1f;
          break;
      }

      if (BitsPerPixel <= 0)
      {
        throw (new ArgumentOutOfRangeException($"{nameof(pixelFormat)} is out of range, please check arguments"));
      }
      CreatePixelBuffer();
    }

  }
}
