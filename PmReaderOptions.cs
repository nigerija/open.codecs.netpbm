﻿using System.IO;

namespace open.codecs.netpbm
{
  /// <summary>
  /// A <see cref="PmFile.Read(string, PmReaderOptions)"/> or <see cref="PmFile.Read(Stream, PmReaderOptions)"/> options.
  /// </summary>
  public class PmReaderOptions
  {
    /// <summary>
    /// True to leave the stream open after was read; Otherwise, false to close it.
    /// (Default: false)
    /// </summary>
    public bool LeaveStreamOpen;

    /// <summary>
    /// Reads only header information without reading pixel data.
    /// (Default: false)
    /// </summary>
    public bool ReadHeaderOnly;

    /// <summary>
    /// If there was an error during the read operation <see cref="PmFile.Image"/> object instance will not be nulled; Otherwise, it will be nulled.
    /// (Default: false)
    /// </summary>
    public bool OnErrorLeaveImageData;
  }
}
