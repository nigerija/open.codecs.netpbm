﻿

# Portable Map (NETPNM) file format

Portable Map file extensions:

    .PAM : Portable Arbitrary Map (a new portable file format)
    .PBM : Portable Bit Map
    .PFM : Portable Float Map
    .PF4 : Portable Float Map (additional alpha channel present)
    .PGM : Portable Grey Map
    .PNM : Portable Any Map (encompasses other formats)
    .PPM : Portable Pix Map

[WHITESPACE], {WS}

    Can be one of more following characters (bytes):
    ' '   : (0x20) space 
    '\t'  : (0x08) tab 
    '\n'  : (0x0A) line feed 
    '\r'  : (0x0D) carry return (permitted, but not counted as end of line)

    NOTE: some parsers may incorrectly parse carry return '\r' as end of line,
          so it would be best to avoid writing them.

[LF], {LF}

    '\n'  : (0x0A) A single LINE-FEED character (byte).

{#}
    '#'   : (0x23) A single sharp character (byte), used for comment lines.

PM File Format structure:

    --- start of file ---
    [MAGIC-IDENTIFIER]{LF}
    [HEADERDATA]{LF}
    [PIXELDATA]
    --- end of file ---

    NOTE: header data permits comment a lines starting with sharp character '#' 

    --- start of header ---
    [COMMENT1]{LF}
    ...
    [COMMENTN]{LF}
    [WIDTH]{SPACE}[HEIGHT]{LF}
    [MAXVAL]
    --- end of header ---

[MAGIC-IDENTIFIER]

    Identifies with two or three bytes the type of data in the file as following:

    'P1'  : ASCII Bit Map              (1 - Black and White bit component)
    'P2'  : ASCII Gray Map             (1 - Grayscale component)
    'P3'  : ASCII Pix Map              (3 - RGB components)
    'P4'  : Binary Bit Map             (1 - Black and White bit component)
    'P5'  : Binary Gray Map            (1 - Grayscale component)
    'P6'  : Binary Pix Map             (3 - RGB components)
    'P7'  : ASCII/Binary Arbitrary Map (1~4 - Arbitrary components, see PAM for details)
    'Pf'  : Binary Float Map           (1 - Grayscale component)
    'Pf4' : Binary Float Map           (2 - Grayscale Alpha components)
    'PF'  : Binary Float Map           (3 - RGB components)
    'PF4' : Binary Float Map           (4 - RGBA components)

[COMMENT1..N]

    Arbitrary text quoted between sharp '#' and {LF} characters (bytes).
    example: '{#} this is commented text{LF}'

[WIDTH]

    N : a number representing the number of columns of the (image) map.

[HEIGHT]

    N : a number representing the number of rows of the (image) map.

[MAXVAL]

    N : a number representing maximum pixel value (range 0-MAXVAL).

    NOTE: if binary format negative -1 indicates pixel data in range of 0..1 written as little endian,
          otherwise pixel data is written in big-endian. All pixel values are always positive numbers.

[PIXELDATA]

    depends on MAGIC-IDENTIFIER if it is an ASCII or Binary data.



    When pixel data is in ASCII format it can be written in as:
    
    'P1' - ASCII Bit Map:

        1. '[b1]{WS}[b2]{WS}[b3]{WS}...[bN]'EOF
        2. '[b1][b2][b3]...[bN]'EOF
    
        where b* is Bit (Black-White) value component.

        NOTE 1: that this is the only ASCII format that permits missing {WS}'s (2.).
        NOTE 2: The file content of bits in Bit (Black-White) format: 
                the value of '0' is white color, and the value of '1' is black color.                
        NOTE 3: Internal memory is kept in WIC format where value of bit 1 is white, and value of bit 0 is black.
                Conversions are made when file is read and when file is written (to conform with note 2).
        NOTE 4: Memory scanlines (rows) are byte aligned, meaning that new scanline (row) starts at byte boundary.
                This conforms with WIC BlackAndWhite format and can be displayed natively.

    'P2' - ASCII Gray Map:

        1. '[g1]{WS}[g2]{WS}[g3]{WS}...[gN]'EOF

        Where g* is gray pixel value component

    'P3' - ASCII Pix Map:
        1. '[pv1r]{WS}[pv1g]{WS}[pv1b]{WS}[pv2r]{WS}[pv2g]{WS}[pv2b]{WS}[pv3r]{WS}[pv3g]{WS}[pv3b]{WS}...[pvNr]{WS}[pvNg]{WS}[pvNb]'EOF

        pv*r : red pixel value component 
        pv*g : green pixel value component 
        pv*b : blue pixel value component 

    When pixel data is in BINARY format it can be written in as:

    'P4' - binary bit (black and white) format where each bit is pixel value
        
        1. each pixel is single bit starting from fist pixel as highest bit in first byte.
        
        NOTE 1: File scanlines (rows) are byte aligned, meaning that new scanline (row) starts at byte boundary.
        NOTE 2: The file content of bits in Bit (Black-White) format: 
                the value of 0 is white color, and the value of 1 is black color.
        NOTE 3: Internal memory is kept in WIC format where value of bit 1 is white, and value of bit 0 is black.
                Conversions are made when file is read and when file is written (to conform with note 2).
        NOTE 4: Memory scanlines (rows) are byte aligned, meaning that new scanline (row) starts at byte boundary.
                This conforms with WIC BlackAndWhite format and can be displayed natively.

        Total size of pixel data is : ((WIDTH+7) / 8) * HEIGHT              
        
    'P5' - binary gray format depends on MAXVAL where each (byte,ushort,uint,ulong) is pixel value written in little-endian as matrix data.

        1. each pixel is a value between 0 and MAXVAL value, and it can be written as 1, 2, 4 or 8 bytes in little-endian as matrix data.

        Total size of pixel data is : WIDTH * HEIGHT * SizeOfPixel
        Where SizeOfPixel is 1, 2, 4 or 8 depending on MAXVAL value:
            If MAXVAL < 256                  : 1 byte
            If MAXVAL < 65536                : 2 bytes
            If MAXVAL < 4294967296           : 4 bytes
            If MAXVAL < 18446744073709551616 : 8 bytes

    'P6' - binary color format depends on MAXVAL where each triple of values (byte,ushort,uint,ulong) is pixel value written in little-endian as matrix data.

        1. each pixel is a triple values between 0 and MAXVAL value, and it can be written as 3, 6, 12 or 24 bytes in little-endian as matrix data.

        Total size of pixel data is : WIDTH * HEIGHT * SizeOfPixel
        Where SizeOfPixel is 3, 6, 12 or 24 depending on MAXVAL value:
            If MAXVAL < 256                  : 3 bytes
            If MAXVAL < 65536                : 6 bytes
            If MAXVAL < 4294967296           : 12 bytes
            If MAXVAL < 18446744073709551616 : 24 bytes

    'P7' - ASCII/Binary Arbitrary format
    
        For details see PAM file format.

    'Pf' - Float Gray Map

    'PF' - Float Pix Map

    'Pf4' - Float Gray-Alpha Map

    'PF4' - Float Pix-Alpha Map

Note that internals do not depend if mode is ASCII or Binary these two modes are interchangeable.
And one can change mode by reading P1 (ASCII) black and white and setting PmImage.FileType to P4 
(Binary) black and white and write new file with same image content. This doesn't apply between 
different components format and number, we may not change P1 to P2 or P3 or vice verso. 
Library original intent was not to convert between internal formats. But to allow us to read PM files,
and compare read content with other libraries outputs (with a Standard provided images).

  // WIC Pixel Formats for completeness there are all

  // TODO: check D2D1RenderTarget ... what it supports actually from pixel formats
  // 

  // DOCS: https://docs.microsoft.com/en-us/windows/win32/wic/-wic-codec-native-pixel-formats
  // missing GUID's

  // NOTE: additional source found at : https://github.com/CMCHTPC/DelphiDX12/blob/master/Units/DX12.WinCodec.pas
  //       meaning that we can actually certify WIC to generate more that this class actually supports

  /**
  // Extracted private members from PixelFormat:
  //
  // PixelFormats static members:
  // ========================================================================================================================================================================
  // Default               , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc900'
  //                 Masks: null
  // Indexed1              , bits:   1, flags: 20001h (BitsPerPixel1, Palettized                                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc901'
  //                 [0]01
  // Indexed2              , bits:   2, flags: 20002h (BitsPerPixel2, Palettized                                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc902'
  //                 [0]03
  // Indexed4              , bits:   4, flags: 20004h (BitsPerPixel4, Palettized                                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc903'
  //                 [0]0F
  // Indexed8              , bits:   8, flags: 20008h (BitsPerPixel8, Palettized                                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc904'
  //                 [0]FF
  // BlackWhite            , bits:   1, flags:   101h (BitsPerPixel1, IsGray                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc905'
  //                 [0]01
  // Gray2                 , bits:   2, flags:   102h (BitsPerPixel2, IsGray                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc906'
  //                 [0]03
  // Gray4                 , bits:   4, flags:   104h (BitsPerPixel4, IsGray                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc907'
  //                 [0]0F
  // Gray8                 , bits:   8, flags:   108h (BitsPerPixel8, IsGray                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc908'
  //                 [0]FF
  // Bgr555                , bits:  16, flags:  4410h (BitsPerPixel16, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc909'
  //                 [0]1F00
  //                 [1]E003
  //                 [2]007C
  // Bgr565                , bits:  16, flags:  4410h (BitsPerPixel16, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90a'
  //                 [0]1F00
  //                 [1]E007
  //                 [2]00F8
  // Rgb128Float           , bits: 128, flags:  2880h (BitsPerPixel128, IsScRGB, ChannelOrderRGB                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91b'
  //                 [0]FFFFFFFF000000000000000000000000
  //                 [1]00000000FFFFFFFF0000000000000000
  //                 [2]0000000000000000FFFFFFFF00000000
  // Bgr24                 , bits:  24, flags:  4418h (BitsPerPixel24, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90c'
  //                 [0]FF0000
  //                 [1]00FF00
  //                 [2]0000FF
  // Rgb24                 , bits:  24, flags:  2418h (BitsPerPixel24, IsSRGB, ChannelOrderRGB                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90d'
  //                 [0]FF0000
  //                 [1]00FF00
  //                 [2]0000FF
  // Bgr101010             , bits:  32, flags:  4420h (BitsPerPixel32, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc914'
  //                 [0]FF030000
  //                 [1]00FC0F00
  //                 [2]0000F03F
  // Bgr32                 , bits:  32, flags:  4420h (BitsPerPixel32, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90e'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  // Bgra32                , bits:  32, flags: 10420h (BitsPerPixel32, IsSRGB, ChannelOrderABGR                 ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90f'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // Pbgra32               , bits:  32, flags: 11420h (BitsPerPixel32, IsSRGB, Premultiplied, ChannelOrderABGR  ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc910'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // Rgb48                 , bits:  48, flags:  2430h (BitsPerPixel48, IsSRGB, ChannelOrderRGB                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc915'
  //                 [0]FFFF00000000
  //                 [1]0000FFFF0000
  //                 [2]00000000FFFF
  // Rgba64                , bits:  64, flags:  8440h (BitsPerPixel64, IsSRGB, ChannelOrderARGB                 ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc916'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // Prgba64               , bits:  64, flags:  9440h (BitsPerPixel64, IsSRGB, Premultiplied, ChannelOrderARGB  ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc917'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // Gray16                , bits:  16, flags:   510h (BitsPerPixel16, IsGray, IsSRGB                           ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90b'
  //                 [0]FFFF
  // Gray32Float           , bits:  32, flags:   920h (BitsPerPixel32, IsGray, IsScRGB                          ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc911'
  //                 [0]FFFFFFFF
  // Rgba128Float          , bits: 128, flags:  8880h (BitsPerPixel128, IsScRGB, ChannelOrderARGB               ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc919'
  //                 [0]FFFFFFFF000000000000000000000000
  //                 [1]00000000FFFFFFFF0000000000000000
  //                 [2]0000000000000000FFFFFFFF00000000
  //                 [3]000000000000000000000000FFFFFFFF
  // Prgba128Float         , bits: 128, flags:  9880h (BitsPerPixel128, IsScRGB, Premultiplied, ChannelOrderARGB), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91a'
  //                 [0]FFFFFFFF000000000000000000000000
  //                 [1]00000000FFFFFFFF0000000000000000
  //                 [2]0000000000000000FFFFFFFF00000000
  //                 [3]000000000000000000000000FFFFFFFF
  // Cmyk32                , bits:  32, flags:   220h (BitsPerPixel32, IsCMYK                                   ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91c'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // ========================================================================================================================================================================
  // WicPixelFormat static members:
  // ========================================================================================================================================================================
  // DontCare              , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc900'
  //                 Masks: null
  // 1bppIndexed           , bits:   1, flags: 20001h (BitsPerPixel1, Palettized                                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc901'
  //                 [0]01
  // 2bppIndexed           , bits:   2, flags: 20002h (BitsPerPixel2, Palettized                                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc902'
  //                 [0]03
  // 4bppIndexed           , bits:   4, flags: 20004h (BitsPerPixel4, Palettized                                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc903'
  //                 [0]0F
  // 8bppIndexed           , bits:   8, flags: 20008h (BitsPerPixel8, Palettized                                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc904'
  //                 [0]FF
  // BlackWhite            , bits:   1, flags:   101h (BitsPerPixel1, IsGray                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc905'
  //                 [0]01
  // 2bppGray              , bits:   2, flags:   102h (BitsPerPixel2, IsGray                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc906'
  //                 [0]03
  // 4bppGray              , bits:   4, flags:   104h (BitsPerPixel4, IsGray                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc907'
  //                 [0]0F
  // 8bppGray              , bits:   8, flags:   108h (BitsPerPixel8, IsGray                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc908'
  //                 [0]FF
  // 8bppAlpha             , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: 'e6cd0116-eeba-4161-aa85-27dd9fb3a895'
  //                 [0]FF
  // 16bppBGR555           , bits:  16, flags:  4410h (BitsPerPixel16, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc909'
  //                 [0]1F00
  //                 [1]E003
  //                 [2]007C
  // 16bppBGR565           , bits:  16, flags:  4410h (BitsPerPixel16, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90a'
  //                 [0]1F00
  //                 [1]E007
  //                 [2]00F8
  // 16bppBGRA5551         , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '05ec7c2b-f1e6-4961-ad46-e1cc810a87d2'
  //                 [0]1F00
  //                 [1]E003
  //                 [2]007C
  //                 [3]0080
  // 16bppGray             , bits:  16, flags:   510h (BitsPerPixel16, IsGray, IsSRGB                           ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90b'
  //                 [0]FFFF
  // 24bppBGR              , bits:  24, flags:  4418h (BitsPerPixel24, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90c'
  //                 [0]FF0000
  //                 [1]00FF00
  //                 [2]0000FF
  // 24bppRGB              , bits:  24, flags:  2418h (BitsPerPixel24, IsSRGB, ChannelOrderRGB                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90d'
  //                 [0]FF0000
  //                 [1]00FF00
  //                 [2]0000FF
  // 32bppBGR              , bits:  32, flags:  4420h (BitsPerPixel32, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90e'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  // 32bppBGRA             , bits:  32, flags: 10420h (BitsPerPixel32, IsSRGB, ChannelOrderABGR                 ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc90f'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // 32bppPBGRA            , bits:  32, flags: 11420h (BitsPerPixel32, IsSRGB, Premultiplied, ChannelOrderABGR  ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc910'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // 32bppGrayFloat        , bits:  32, flags:   920h (BitsPerPixel32, IsGray, IsScRGB                          ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc911'
  //                 [0]FFFFFFFF
  // 32bppRGB              , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: 'd98c6b95-3efe-47d6-bb25-eb1748ab0cf1'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  // 32bppRGBA             , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: 'f5c7ad2d-6a8d-43dd-a7a8-a29935261ae9'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // 32bppPRGBA            , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '3cc4a650-a527-4d37-a916-3142c7ebedba'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // 48bppRGB              , bits:  48, flags:  2430h (BitsPerPixel48, IsSRGB, ChannelOrderRGB                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc915'
  //                 [0]FFFF00000000
  //                 [1]0000FFFF0000
  //                 [2]00000000FFFF
  // 48bppBGR              , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: 'e605a384-b468-46ce-bb2e-36f180e64313'
  //                 [0]FFFF00000000
  //                 [1]0000FFFF0000
  //                 [2]00000000FFFF
  // 64bppRGB              , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: 'a1182111-186d-4d42-bc6a-9c8303a8dff9'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  // 64bppRGBA             , bits:  64, flags:  8440h (BitsPerPixel64, IsSRGB, ChannelOrderARGB                 ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc916'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 64bppBGRA             , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '1562ff7c-d352-46f9-979e-42976b792246'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 64bppPRGBA            , bits:  64, flags:  9440h (BitsPerPixel64, IsSRGB, Premultiplied, ChannelOrderARGB  ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc917'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 64bppPBGRA            , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '8c518e8e-a4ec-468b-ae70-c9a35a9c5530'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 16bppGrayFixedPoint   , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc913'
  //                 [0]FFFF
  // 32bppBGR101010        , bits:  32, flags:  4420h (BitsPerPixel32, IsSRGB, ChannelOrderBGR                  ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc914'
  //                 [0]FF030000
  //                 [1]00FC0F00
  //                 [2]0000F03F
  // 48bppRGBFixedPoint    , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc912'
  //                 [0]FFFF00000000
  //                 [1]0000FFFF0000
  //                 [2]00000000FFFF
  // 48bppBGRFixedPoint    , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '49ca140e-cab6-493b-9ddf-60187c37532a'
  //                 [0]FFFF00000000
  //                 [1]0000FFFF0000
  //                 [2]00000000FFFF
  // 96bppRGBFixedPoint    , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc918'
  //                 [0]FFFFFFFF0000000000000000
  //                 [1]00000000FFFFFFFF00000000
  //                 [2]0000000000000000FFFFFFFF
  // 96bppRGBFloat         , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: 'e3fed78f-e8db-4acf-84c1-e97f6136b327'
  //                 [0]FFFFFFFF0000000000000000
  //                 [1]00000000FFFFFFFF00000000
  //                 [2]0000000000000000FFFFFFFF
  // 128bppRGBAFloat       , bits: 128, flags:  8880h (BitsPerPixel128, IsScRGB, ChannelOrderARGB               ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc919'
  //                 [0]FFFFFFFF000000000000000000000000
  //                 [1]00000000FFFFFFFF0000000000000000
  //                 [2]0000000000000000FFFFFFFF00000000
  //                 [3]000000000000000000000000FFFFFFFF
  // 128bppPRGBAFloat      , bits: 128, flags:  9880h (BitsPerPixel128, IsScRGB, Premultiplied, ChannelOrderARGB), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91a'
  //                 [0]FFFFFFFF000000000000000000000000
  //                 [1]00000000FFFFFFFF0000000000000000
  //                 [2]0000000000000000FFFFFFFF00000000
  //                 [3]000000000000000000000000FFFFFFFF
  // 128bppRGBFloat        , bits: 128, flags:  2880h (BitsPerPixel128, IsScRGB, ChannelOrderRGB                ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91b'
  //                 [0]FFFFFFFF000000000000000000000000
  //                 [1]00000000FFFFFFFF0000000000000000
  //                 [2]0000000000000000FFFFFFFF00000000
  // 32bppCMYK             , bits:  32, flags:   220h (BitsPerPixel32, IsCMYK                                   ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91c'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // 64bppRGBAFixedPoint   , bits:   0, flags:  8800h (IsScRGB, ChannelOrderARGB                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91d'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 64bppBGRAFixedPoint   , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '356de33c-54d2-4a23-bb04-9b7bf9b1d42d'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 64bppRGBFixedPoint    , bits:   0, flags:  2800h (IsScRGB, ChannelOrderRGB                                 ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc940'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  // 128bppRGBAFixedPoint  , bits:   0, flags:  8800h (IsScRGB, ChannelOrderARGB                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91e'
  //                 [0]FFFFFFFF000000000000000000000000
  //                 [1]00000000FFFFFFFF0000000000000000
  //                 [2]0000000000000000FFFFFFFF00000000
  //                 [3]000000000000000000000000FFFFFFFF
  // 128bppRGBFixedPoint   , bits:   0, flags:  2800h (IsScRGB, ChannelOrderRGB                                 ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc941'
  //                 [0]FFFFFFFF000000000000000000000000
  //                 [1]00000000FFFFFFFF0000000000000000
  //                 [2]0000000000000000FFFFFFFF00000000
  // 64bppRGBAHalf         , bits:   0, flags:  8800h (IsScRGB, ChannelOrderARGB                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc93a'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 64bppPRGBAHalf        , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '58ad26c2-c623-4d9d-b320-387e49f8c442'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 64bppRGBHalf          , bits:   0, flags:  2800h (IsScRGB, ChannelOrderRGB                                 ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc942'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  // 48bppRGBHalf          , bits:   0, flags:  2800h (IsScRGB, ChannelOrderRGB                                 ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc93b'
  //                 [0]FFFF00000000
  //                 [1]0000FFFF0000
  //                 [2]00000000FFFF
  // 32bppRGBE             , bits:   0, flags:  2800h (IsScRGB, ChannelOrderRGB                                 ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc93d'
  //                 [0]FF0000FF
  //                 [1]00FF00FF
  //                 [2]0000FFFF
  // 16bppGrayHalf         , bits:   0, flags:   900h (IsGray, IsScRGB                                          ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc93e'
  //                 [0]FFFF
  // 32bppGrayFixedPoint   , bits:   0, flags:   900h (IsGray, IsScRGB                                          ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc93f'
  //                 [0]FFFFFFFF
  // 32bppRGBA1010102      , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '25238d72-fcf9-4522-b514-5578e5ad55e0'
  //                 [0]FF030000
  //                 [1]00FC0F00
  //                 [2]0000F03F
  //                 [3]000000C0
  // 32bppRGBA1010102XR    , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '00de6b9a-c101-434b-b502-d0165ee1122c'
  //                 [0]FF030000
  //                 [1]00FC0F00
  //                 [2]0000F03F
  //                 [3]000000C0
  // 64bppCMYK             , bits:   0, flags:   200h (IsCMYK                                                   ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc91f'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 24bpp3Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc920'
  //                 [0]FF0000
  //                 [1]00FF00
  //                 [2]0000FF
  // 32bpp4Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc921'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // 40bpp5Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc922'
  //                 [0]FF00000000
  //                 [1]00FF000000
  //                 [2]0000FF0000
  //                 [3]000000FF00
  //                 [4]00000000FF
  // 48bpp6Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc923'
  //                 [0]FF0000000000
  //                 [1]00FF00000000
  //                 [2]0000FF000000
  //                 [3]000000FF0000
  //                 [4]00000000FF00
  //                 [5]0000000000FF
  // 56bpp7Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc924'
  //                 [0]FF000000000000
  //                 [1]00FF0000000000
  //                 [2]0000FF00000000
  //                 [3]000000FF000000
  //                 [4]00000000FF0000
  //                 [5]0000000000FF00
  //                 [6]000000000000FF
  // 64bpp8Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc925'
  //                 [0]FF00000000000000
  //                 [1]00FF000000000000
  //                 [2]0000FF0000000000
  //                 [3]000000FF00000000
  //                 [4]00000000FF000000
  //                 [5]0000000000FF0000
  //                 [6]000000000000FF00
  //                 [7]00000000000000FF
  // 48bpp3Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc926'
  //                 [0]FFFF00000000
  //                 [1]0000FFFF0000
  //                 [2]00000000FFFF
  // 64bpp4Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc927'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 80bpp5Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc928'
  //                 [0]FFFF0000000000000000
  //                 [1]0000FFFF000000000000
  //                 [2]00000000FFFF00000000
  //                 [3]000000000000FFFF0000
  //                 [4]0000000000000000FFFF
  // 96bpp6Channels        , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc929'
  //                 [0]FFFF00000000000000000000
  //                 [1]0000FFFF0000000000000000
  //                 [2]00000000FFFF000000000000
  //                 [3]000000000000FFFF00000000
  //                 [4]0000000000000000FFFF0000
  //                 [5]00000000000000000000FFFF
  // 112bpp7Channels       , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc92a'
  //                 [0]FFFF000000000000000000000000
  //                 [1]0000FFFF00000000000000000000
  //                 [2]00000000FFFF0000000000000000
  //                 [3]000000000000FFFF000000000000
  //                 [4]0000000000000000FFFF00000000
  //                 [5]00000000000000000000FFFF0000
  //                 [6]000000000000000000000000FFFF
  // 128bpp8Channels       , bits:   0, flags: 80000h (IsNChannel                                               ), alpha: N, guid: '6fddc324-4e03-4bfe-b185-3d77768dc92b'
  //                 [0]FFFF0000000000000000000000000000
  //                 [1]0000FFFF000000000000000000000000
  //                 [2]00000000FFFF00000000000000000000
  //                 [3]000000000000FFFF0000000000000000
  //                 [4]0000000000000000FFFF000000000000
  //                 [5]00000000000000000000FFFF00000000
  //                 [6]000000000000000000000000FFFF0000
  //                 [7]0000000000000000000000000000FFFF
  // 40bppCMYKAlpha        , bits:   0, flags: 40200h (IsCMYK, NChannelAlpha                                    ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc92c'
  //                 [0]FF00000000
  //                 [1]00FF000000
  //                 [2]0000FF0000
  //                 [3]000000FF00
  //                 [4]00000000FF
  // 80bppCMYKAlpha        , bits:   0, flags: 40200h (IsCMYK, NChannelAlpha                                    ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc92d'
  //                 [0]FFFF0000000000000000
  //                 [1]0000FFFF000000000000
  //                 [2]00000000FFFF00000000
  //                 [3]000000000000FFFF0000
  //                 [4]0000000000000000FFFF
  // 32bpp3ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc92e'
  //                 [0]FF000000
  //                 [1]00FF0000
  //                 [2]0000FF00
  //                 [3]000000FF
  // 40bpp4ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc92f'
  //                 [0]FF00000000
  //                 [1]00FF000000
  //                 [2]0000FF0000
  //                 [3]000000FF00
  //                 [4]00000000FF
  // 48bpp5ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc930'
  //                 [0]FF0000000000
  //                 [1]00FF00000000
  //                 [2]0000FF000000
  //                 [3]000000FF0000
  //                 [4]00000000FF00
  //                 [5]0000000000FF
  // 56bpp6ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc931'
  //                 [0]FF000000000000
  //                 [1]00FF0000000000
  //                 [2]0000FF00000000
  //                 [3]000000FF000000
  //                 [4]00000000FF0000
  //                 [5]0000000000FF00
  //                 [6]000000000000FF
  // 64bpp7ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc932'
  //                 [0]FF00000000000000
  //                 [1]00FF000000000000
  //                 [2]0000FF0000000000
  //                 [3]000000FF00000000
  //                 [4]00000000FF000000
  //                 [5]0000000000FF0000
  //                 [6]000000000000FF00
  //                 [7]00000000000000FF
  // 72bpp8ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc933'
  //                 [0]FF0000000000000000
  //                 [1]00FF00000000000000
  //                 [2]0000FF000000000000
  //                 [3]000000FF0000000000
  //                 [4]00000000FF00000000
  //                 [5]0000000000FF000000
  //                 [6]000000000000FF0000
  //                 [7]00000000000000FF00
  //                 [8]0000000000000000FF
  // 64bpp3ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc934'
  //                 [0]FFFF000000000000
  //                 [1]0000FFFF00000000
  //                 [2]00000000FFFF0000
  //                 [3]000000000000FFFF
  // 80bpp4ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc935'
  //                 [0]FFFF0000000000000000
  //                 [1]0000FFFF000000000000
  //                 [2]00000000FFFF00000000
  //                 [3]000000000000FFFF0000
  //                 [4]0000000000000000FFFF
  // 96bpp5ChannelsAlpha   , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc936'
  //                 [0]FFFF00000000000000000000
  //                 [1]0000FFFF0000000000000000
  //                 [2]00000000FFFF000000000000
  //                 [3]000000000000FFFF00000000
  //                 [4]0000000000000000FFFF0000
  //                 [5]00000000000000000000FFFF
  // 112bpp6ChannelsAlpha  , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc937'
  //                 [0]FFFF000000000000000000000000
  //                 [1]0000FFFF00000000000000000000
  //                 [2]00000000FFFF0000000000000000
  //                 [3]000000000000FFFF000000000000
  //                 [4]0000000000000000FFFF00000000
  //                 [5]00000000000000000000FFFF0000
  //                 [6]000000000000000000000000FFFF
  // 128bpp7ChannelsAlpha  , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc938'
  //                 [0]FFFF0000000000000000000000000000
  //                 [1]0000FFFF000000000000000000000000
  //                 [2]00000000FFFF00000000000000000000
  //                 [3]000000000000FFFF0000000000000000
  //                 [4]0000000000000000FFFF000000000000
  //                 [5]00000000000000000000FFFF00000000
  //                 [6]000000000000000000000000FFFF0000
  //                 [7]0000000000000000000000000000FFFF
  // 144bpp8ChannelsAlpha  , bits:   0, flags: c0000h (NChannelAlpha, IsNChannel                                ), alpha: Y, guid: '6fddc324-4e03-4bfe-b185-3d77768dc939'
  //                 [0]FFFF00000000000000000000000000000000
  //                 [1]0000FFFF0000000000000000000000000000
  //                 [2]00000000FFFF000000000000000000000000
  //                 [3]000000000000FFFF00000000000000000000
  //                 [4]0000000000000000FFFF0000000000000000
  //                 [5]00000000000000000000FFFF000000000000
  //                 [6]000000000000000000000000FFFF00000000
  //                 [7]0000000000000000000000000000FFFF0000
  //                 [8]00000000000000000000000000000000FFFF
  // 8bppY                 , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '91b4db54-2df9-42f0-b449-2909bb3df88e'
  //                 Masks: null
  // 8bppCb                , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: '1339f224-6bfe-4c3e-9302-e4f3a6d0ca2a'
  //                 Masks: null
  // 8bppCr                , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: 'b8145053-2116-49f0-8835-ed844b205c51'
  //                 Masks: null
  // 16bppCbCr             , bits:   0, flags:     0h (BitsPerPixelUndefined                                    ), alpha: N, guid: 'ff95ba6e-11e0-4263-bb45-01721f3460a4'
  //                 Masks: null
  // ========================================================================================================================================================================
  //*/
  NOTE : WicPixelFormats.cs is actually hacking internals of .NET in order to allow other WIC formats to work.
         And it just works cause .NET is using WIC which supports many more formats unlike .NET version.
  
         if bits says 0, or Masks is null, pixel format is invalid and requires correction see WicPixelFormats.cs
