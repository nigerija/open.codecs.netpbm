﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace open.codecs.netpbm
{
  /// <summary>
  /// Portable Map image container write status
  /// </summary>
  public enum EPmWriteStatus
  {
    /// <summary>
    /// Success
    /// </summary>
    OK = 0,

    /// <summary>
    /// Write base error 
    /// </summary>
    ERROR_BASE = 0x9000,

    /// <summary>
    /// Write error : operation not supported (Write ascii pixel data not possible).
    /// </summary>
    ERROR_NOT_SUPPORTED = ERROR_BASE + 1,

    /// <summary>
    /// Write error : file already exists.
    /// </summary>
    ERROR_FILE_ALREADY_EXSTS = ERROR_BASE + 2,

    /// <summary>
    /// Write error : unknown file type (undefined).
    /// </summary>
    ERROR_UNKNOWN_FILE_TYPE = ERROR_BASE + 3,

    /// <summary>
    /// Write error : invalid PAM image TUPLTYPE (undefined).
    /// </summary>
    ERROR_INVALID_IMAGE_TUPLTYPE = ERROR_BASE + 10,

    /// <summary>
    /// Write error : invalid image binary pixel size, unable to calculate pixel size bits.
    /// </summary>
    ERROR_INVALID_BINARY_PIXEL_SIZE = ERROR_BASE + 11,

    
  }
}
