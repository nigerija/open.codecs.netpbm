﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Media.Imaging;

namespace open.codecs.netpbm
{
  /// <summary>
  /// Portable Map codec information
  /// </summary>
  public class PmCodecInfo : BitmapCodecInfo
  {
    // TODO: validate if project, COM library, and codec, can all three have same GUID (COM and Codec).
    //       atm I am retrieving from assembly GUID information.

    static readonly Assembly s_Assembly = Assembly.GetAssembly(typeof(PmCodecInfo));
    static readonly AssemblyName s_AssemblyName = s_Assembly.GetName();    
    static readonly Guid s_CodecGuid = new Guid(((GuidAttribute)s_Assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0]).Value);
    static readonly string s_Author = "Nikola Bozovic";
    static readonly string s_FriendlyName = "Portable Map (NetPbm) codec";
    static readonly string s_DeviceManufacturer = "open";
    static readonly string s_DeviceModels = "open.codecs";
    static readonly string s_MimeTypes = "image/x-portable-bitmap,image/x-portable-graymap,image/x-portable-pixmap,image/x-portable-anymap,image/x-portable-arbitrarymap,image/x-portable-floatmap";
    static readonly string s_FileExtensions = ".pbm,.pgm,*.ppm,*.pnm,*.pam,*.pfm";


    /// <summary>
    /// Gets a value that identifies the container format for the codec.
    /// </summary>
    public override Guid ContainerFormat => s_CodecGuid;
    /// <summary>
    /// Gets a value that identifies the author of the codec.
    /// </summary>
    public override string Author => s_Author;
    /// <summary>
    /// Gets a value that identifies the version of the codec.
    /// </summary>
    public override Version Version => s_Assembly.GetName().Version;
    /// <summary>
    /// Gets a value that identifies the specification version of the codec.
    /// </summary>
    public override Version SpecificationVersion => s_Assembly.GetName().Version;
    /// <summary>
    /// Gets a value that represents the friendly name of the codec
    /// </summary>
    public override string FriendlyName => s_FriendlyName;
    /// <summary>
    /// Gets a value that identifies the device manufacturer of the codec.
    /// </summary>
    public override string DeviceManufacturer => s_DeviceManufacturer;
    /// <summary>
    /// Gets a value that identifies the device models of the codec.
    /// </summary>
    public override string DeviceModels => s_DeviceModels;
    /// <summary>
    /// Gets a value that identifies the Multipurpose Internet Mail Extensions (MIME) types
    /// associated with the codec.
    /// </summary>
    public override string MimeTypes => s_MimeTypes;
    /// <summary>
    /// Gets a value that identifies the file extensions associated with the codec.
    /// </summary>
    public override string FileExtensions => s_FileExtensions;
    /// <summary>
    /// Gets a value that indicates whether the codec supports animation.
    /// </summary>
    public override bool SupportsAnimation => false;
    /// <summary>
    /// Gets a value that indicates whether the codec supports lossless of images.
    /// </summary>
    public override bool SupportsLossless => true;
    /// <summary>
    /// Gets a value that identifies whether the codec supports multiple frames.
    /// </summary>
    public override bool SupportsMultipleFrames => false;

  }
}
