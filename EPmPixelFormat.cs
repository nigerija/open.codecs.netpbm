﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using open.codecs.netpbm.wic;

namespace open.codecs.netpbm
{
  /// <summary>
  /// Portable Map image pixel format  
  /// <code>
  /// name format : '[components]_[bits]bits_[count]x[type]'
  /// · [components] : definition on format and order of components:
  /// (W : black and white 1bit; 
  /// Y : grayscale; 
  /// R : red;
  /// G : green; 
  /// B : blue;
  /// A : alpha;)
  /// 
  /// · [bits]  : number of bits for single pixel value;
  /// · [count] : number of components;
  /// · [type]  : each component type:
  /// (u1 : unsigned int 1-bit;
  /// u8 : unsigned int 8-bit;
  /// u16 : unsigned int 16-bit;
  /// u32 : unsigned int 32-bit;
  /// u48 : unsigned int 48-bit;
  /// u64 : unsigned int 64-bit;
  /// f32 : IEEE 754 float 32-bit [values between 0f and 1f if not stated otherwise with MaxVal];)
  /// </code>
  /// <para>■ NOTE: Color pixel format is always RGB[A] order of components, unlike the Bitmap color pixel format where we have BGR[A] order of components.</para>
  /// </summary>
  public enum EPmPixelFormat
  {
    /// <summary>
    /// Unknown or otherwise invalid pixel format.
    /// </summary>
    Unknown = 0,

    /// <summary>
    /// BlackAndWhite, where component is unsigned int 1-bit.
    /// <para>[G:1] = 1 bit = 1/8 byte.</para>
    /// <para>Container support: <see cref="EPmFileType.P1_BITMAP_ASCII"/>, <see cref="EPmFileType.P4_BITMAP_BINARY"/>, <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.BLACKANDWHITE"/>).</para>
    /// <para>Converter support: native <see cref="PixelFormats.BlackWhite"/>.</para>
    /// <para>■ NOTE: Only valid values are 0 and 1 in bits (bytes still can have values of 0 through 255 as they represent 8 pixels in this format maxVal is ignored in this case and not written to P1/P4 files).</para>
    /// <para>■ NOTE: All scanlines (rows) are byte aligned, meaning that new scanline (row) starts at next byte boundary (as in original NETPBM project).</para>
    /// <para>■ NOTE: When written to the file the bit value in the file of 0 is white, and bit value in the file of 1 is black. This is opposite to WIC 1bpp pixel format and memory of <see cref="PmImage.ImagePixelData"/>, the bit value of 0 is black and the bit value of 1 is white.</para>
    /// </summary>
    W_1bit_1u1,
    /// <summary>
    /// BlackAndWhite-Alpha, where each component is unsigned int 8-bit.
    /// <para>[G:8][A:8] = 16 bits = 2 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.BLACKANDWHITE_ALPHA"/>).</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Bgra32"/>.</para>
    /// <para>■ NOTE: only valid values are 0 and 1 (pixels are mapped to two bytes, but bytes values must be 0 or 1, maxVal is ignored).</para>
    /// <para>■ NOTE: (? under investigation) the bit value of 0 is white, and bit value of 1 is black, this is opposite to WIC 1bpp pixel format where 0 is black, and 1 is white.</para>
    /// </summary>
    WA_16bits_2u8,
    /// <summary>
    /// Grayscale, where component is unsigned int 8-bit
    /// <para>Channel [G:8] = 8 bits = 1 byte, with maximum value per channel in range : [2..255].</para>
    /// <para>Container support: <see cref="EPmFileType.P2_GRAYMAP_ASCII"/>, <see cref="EPmFileType.P5_GRAYMAP_BINARY"/>, <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.GRAYSCALE"/>).</para>
    /// <para>Converter support: native <see cref="PixelFormats.Gray8"/>.</para>
    /// </summary>
    Y_8bits_1u8,
    /// <summary>
    /// Grayscale-Alpha, where each component is unsigned int 16-bit
    /// <para>Channels [G:8][A:8] = 16 bits = 2 bytes, with maximum value per channel in range : [2..255].</para>
    /// <para>Container support: <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.GRAYSCALE_ALPHA"/>).</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Bgra32"/>.</para>
    /// </summary>
    YA_16bits_2u8,
    /// <summary>
    /// Grayscale pixel of 16 bits (2 bytes),
    /// <para>[G:16] = 16 bits = 2 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.GRAYSCALE_ALPHA"/>).</para>
    /// <para>Converter support: native <see cref="PixelFormats.Gray16"/>.</para>
    /// </summary>
    Y_16bits_1u16,
    /// <summary>
    /// Grayscale-Alpha, where each component is unsigned int 16-bit
    /// <para>[G:16][A:16] = 32 bits = 4 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.GRAYSCALE_ALPHA"/>).</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Bgra32"/>.</para>
    /// </summary>
    YA_32bits_2u16,
    /// <summary>
    /// Grayscale, where component is unsigned int 32-bit
    /// <para>[G:32] = 32 bits = 4 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P2_GRAYMAP_ASCII"/>, <see cref="EPmFileType.P5_GRAYMAP_BINARY"/>.</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Gray16"/>.</para>
    /// </summary>
    Y_32bits_1u32,
    /// <summary>
    /// Grayscale, where component is IEEE 754 float 32-bit [values are between 0f and 1f, if not stated otherwise with MaxVal].
    /// <para>[G:32] = 32 bits = 4 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.PF_FLOATMAP_GRAY_BINARY"/>, <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.FLOAT_GRAYSCALE"/>).</para>
    /// <para>Converter support: native <see cref="PixelFormats.Gray32Float"/>.</para>
    /// </summary>
    Y_32bits_1f32,
    /// <summary>
    /// Grayscale-Alpha, where each component is IEEE 754 float 32-bit [values are between 0f and 1f, if not stated otherwise with MaxVal].
    /// <para>[G:32][A:32] = 64 bits = 8 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.PF4_FLOATMAP_GRAY_ALPHA_BINARY"/>, <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.FLOAT_GRAYSCALE_ALPHA"/>).</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Rgba64"/>.</para>
    /// </summary>
    YA_64bits_2f32,
    /// <summary>
    /// Grayscale, where component is unsigned int 48-bit
    /// <para>[G:48] = 48 bits = 6 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P2_GRAYMAP_ASCII"/>, <see cref="EPmFileType.P5_GRAYMAP_BINARY"/>.</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Gray16"/>.</para>
    /// </summary>
    Y_48bits_1u48,
    /// <summary>
    /// Grayscale, where component is unsigned int 64-bit
    /// <para>[G:64] = 64 bits = 8 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P2_GRAYMAP_ASCII"/>, <see cref="EPmFileType.P5_GRAYMAP_BINARY"/>.</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Gray16"/>.</para>
    /// </summary>
    Y_64bits_1u64,
    /// <summary>
    /// Color Red-Green-Blue, where each color component is unsigned int 8-bit.
    /// <para>[R:8][G:8][B:8] = 24 bits = 3 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P3_PIXMAP_ASCII"/>, <see cref="EPmFileType.P6_PIXMAP_BINARY"/>, <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.RGB"/>).</para>
    /// <para>Converter support: native <see cref="PixelFormats.Rgb24"/>.</para>
    /// </summary>
    RGB_24bits_3u8,
    /// <summary>
    /// Color Red-Green-Blue-Alpha, where each color component is unsigned int 8-bit.
    /// <para>[R:8][G:8][B:8][A:8] = 32 bits = 4 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P3_PIXMAP_ASCII"/>, <see cref="EPmFileType.P6_PIXMAP_BINARY"/>, <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.RGB_ALPHA"/>).</para>
    /// <para>Converter support: native WIC <see cref="WicPixelFormats.PixelFormat32bppRGBA"/>.</para>
    /// </summary>
    RGBA_32bits_4u8,
    /// <summary>
    /// Color Red-Green-Blue, where each color component is unsigned int 16-bit.
    /// <para>[R:16][G:16][B:16] = 48 bits = 6 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P3_PIXMAP_ASCII"/>, <see cref="EPmFileType.P6_PIXMAP_BINARY"/>.</para>
    /// <para>Converter support: native <see cref="PixelFormats.Rgb48"/>.</para>
    /// </summary>
    RGB_48bits_3u16,
    /// <summary>
    /// Color Red-Green-Blue-Alpha, where each color component is unsigned int 16-bit.
    /// <para>[R:16][G:16][B:16][A:16] = 64 bits = 8 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P3_PIXMAP_ASCII"/>, <see cref="EPmFileType.P6_PIXMAP_BINARY"/>.</para>
    /// <para>Converter support: native <see cref="PixelFormats.Rgba64"/>.</para>
    /// </summary>
    RGBA_64bits_4u16,
    /// <summary>
    /// Color Red-Green-Blue, where each color component is unsigned int 32-bit.
    /// <para>[R:32][G:32][B:32] = 96 bits = 12 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P3_PIXMAP_ASCII"/>, <see cref="EPmFileType.P6_PIXMAP_BINARY"/>.</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Rgb48"/>.</para>
    /// </summary>
    RGB_96bits_3u32,
    /// <summary>
    /// Color Red-Green-Blue, where each color component is IEEE 754 float 32-bit [values are between 0f and 1f, if not stated otherwise with MaxVal].
    /// <para>[R:32][G:32][B:32] = 96 bits = 12 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.PF_FLOATMAP_PIX_BINARY"/>, <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.FLOAT_RGB"/>).</para>
    /// <para>Converter support: native WIC <see cref="WicPixelFormats.PixelFormat96bppRGBFloat"/>.</para>
    /// </summary>
    RGB_96bits_3f32, // *
    /// <summary>
    /// Color Red-Green-Blue-Alpha, where each color component is IEEE 754 float 32-bit [values are between 0f and 1f, if not stated otherwise with MaxVal].
    /// <para>[R:32][G:32][B:32][A:32] = 128 bits = 16 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.PF4_FLOATMAP_PIX_ALPHA_BINARY"/>, <see cref="EPmFileType.P7_ARBITRARY_MAP_BINARY"/> (<see cref="EPamTuplType.FLOAT_RGB_ALPHA"/>).</para>
    /// <para>Converter support: native <see cref="PixelFormats.Rgba128Float"/>.</para>
    /// </summary>
    RGBA_128bits_4f32, // *
    /// <summary>
    /// Color Red-Green-Blue 64 bits per channel total 192-bits (24 bytes).
    /// <para>[R:64][G:64][B:64] = 192 bits = 24 bytes.</para>
    /// <para>Container support: <see cref="EPmFileType.P3_PIXMAP_ASCII"/>, <see cref="EPmFileType.P6_PIXMAP_BINARY"/>.</para>
    /// <para>Converter support: converted to <see cref="PixelFormats.Rgb48"/>.</para>
    /// </summary>
    RGB_192bits_3u64, // *


  }
}
