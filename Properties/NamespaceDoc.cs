﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;

namespace open.codecs.netpbm
{
  //
  // ■ Note: these are only documentation definitions for namespace's defined in this library.
  //

  /// <summary>
  /// <c>open.codecs.netpbm</c> library documentation.
  /// </summary>  
  [CompilerGenerated()] class NamespaceDoc { }

  namespace types
  {
    /// <summary>
    /// <c>open.codecs.netpbm</c> library documentation.
    /// </summary>  
    [CompilerGenerated()] class NamespaceDoc { }
  }

  namespace wic
  {
    /// <summary>
    /// <c>open.codecs.netpbm</c> library documentation.
    /// </summary>  
    [CompilerGenerated()] class NamespaceDoc { }
  }
}

