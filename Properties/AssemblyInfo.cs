﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("open.codecs.netpbm")]
[assembly: AssemblyDescription("open codecs Portable-Map (NetPbm)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("open")]
[assembly: AssemblyProduct("open.codecs.netpbm")]
[assembly: AssemblyCopyright("Copyright © Nikola Bozovic 2021 All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
// Solution Project GUID: 8E8825AB-294B-4520-9A77-B7FA2926E8CD
[assembly: Guid("8e8825ab-294b-4520-9a77-b7fa2926e8cd")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//

// TODO: automatic version assignment 

// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2021.12.30.1")]
[assembly: AssemblyFileVersion("2021.12.30.1")]
