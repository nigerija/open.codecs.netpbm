﻿using System.IO;

namespace open.codecs.netpbm
{
  /// <summary>
  /// Write options for ASCII Bitmap file
  /// </summary>
  public class PmWriterAsciiOptions
  {
    /// <summary>
    /// Common scanline break at image width. Zero for no break.
    /// </summary>
    public int WidthBreak = 0;
    /// <summary>
    /// Common line break at 72'nd character. Zero for no break.
    /// </summary>
    public int LineBreak = 72;
    /// <summary>
    /// Writes SPACE (0x20) separator after each pixel
    /// </summary>
    public bool WriteSeparator = false;
  }

  /// <summary>
  /// A <see cref="PmFile.Write(string, PmWriterOptions)"/> or <see cref="PmFile.Write(Stream, PmWriterOptions)"/> options.
  /// </summary>
  public class PmWriterOptions
  {
    /// <summary>
    /// True to leave the stream open after was written; Otherwise, false to close it.
    /// (Default: false)
    /// </summary>
    public bool LeaveStreamOpen;

    /// <summary>
    /// True to overwrite existing file; Otherwise, false to return write error <see cref="EPmWriteStatus.ERROR_FILE_ALREADY_EXSTS"/>.
    /// (Default: false)
    /// </summary>
    public bool OverwriteExistingFile;

    /// <summary>
    /// A float MaxVal format
    /// (Default: "F6")
    /// </summary>
    public string FloatMaxValueFormat = "F6";

    /// <summary>
    /// Ascii BitMap write options
    /// </summary>
    public PmWriterAsciiOptions AsciiOptions = new PmWriterAsciiOptions();
  }
}
