﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace open.codecs.netpbm
{
  /// <summary>
  /// A type that stores 48-bit unsigned integer
  /// </summary>
  [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SizeOf)]
#pragma warning disable IDE1006 // Naming Styles
  public struct UInt48: IComparable, IFormattable, IConvertible, IComparable<UInt48>, IEquatable<UInt48>
  {
    /// <summary>
    /// The number of bytes this type will take.
    /// </summary>
    public const int SizeOf = 6;

    /// <summary>
    /// The maximum value of this type represented as ulong.
    /// </summary>
    public const ulong MinValue = 0ul;

    /// <summary>
    /// The minimum value of this type.
    /// </summary>
    public static readonly UInt48 UInt48MinValue = MinValue;

    /// <summary>
    /// The maximum value of this type represented as ulong.
    /// </summary>
    public const ulong MaxValue = 0x0000FFFFFFFFFFFFul;

    /// <summary>
    /// The maximum value of this type.
    /// </summary>
    public static readonly UInt48 UInt48MaxValue = MaxValue;

    #region ### union ###

    /// <summary>
    /// <para>BE: Higher 16-bits [bits: 32 - 47]</para>
    /// <para>LE: Lower 16 bits [bits: 0 - 15] </para>
    /// </summary>
    [FieldOffset(0)]
    public ushort v0u16;

    /// <summary>
    /// <para>BE: Lower 32-bits [bits: 0 - 31]</para>
    /// <para>LE: Higher 32 bits [bits: 16 - 47]</para>
    /// </summary>
    [FieldOffset(2)]
    public uint v2u32;

    /// <summary>
    /// <para>BE: Higher 32 bits [bits: 16 - 47]</para>
    /// <para>LE: Lower 32 bits [bits: 0 - 31]</para>
    /// </summary>
    [FieldOffset(0)]
    public uint v0u32;

    /// <summary>
    /// <para>BE: Lower 16 bits [bits: 0 - 15]</para>
    /// <para>LE: Higher 16 bits [bits: 32 - 47]</para>
    /// </summary>
    [FieldOffset(4)]
    public ushort v4u16;

    #endregion

    #region ### operators ###

    // NOTE: should suffice for all assignments/conversions
    //       only it still requires first explicit conversion to ulong

    /* makes type agnostic for big endian/little endian
     *      memory layout of bytes     => lower part as UInt32    , higher part as UInt16
     * BE : { b5, b4, b3, b2, b1, b0 } => v2u32:{ b3, b2, b1, b0 }, v0u16:{ b5, b4 } >> 32
     * LE : { b0, b1, b2, b3, b4, b5 } => v0u32:{ b0, b1, b2, b3 }, v4u16:{ b4, b5 } >> 32
     */

    /// <summary>
    /// implicit conversion from <see cref="UInt48"/> to <see cref="ulong"/>.
    /// </summary>
    /// <param name="r">A value to be converted.</param>
    /// <returns>
    /// A <see cref="ulong"/> number.
    /// </returns>
    [MethodImpl(256)]
    public static implicit operator ulong(UInt48 r)
    {
      if (!PmReader.HWIsLittleEndian)
        return ((ulong)r.v0u16 << 32) | r.v2u32;
      return ((ulong)r.v4u16 << 32) | r.v0u32;
    }

    /// <summary>
    /// implicit conversion from <see cref="ulong"/> to <see cref="UInt48"/>.
    /// </summary>
    /// <param name="r">A value to be converted.</param>
    /// <returns>
    /// A <see cref="UInt48"/> number.
    /// </returns>
    [MethodImpl(256)]
    public static implicit operator UInt48(ulong r)
    {
      if (!PmReader.HWIsLittleEndian)
        return new UInt48() { v2u32 = (uint)r, v0u16 = (ushort)(r >> 32) };
      return new UInt48() { v0u32 = (uint)r, v4u16 = (ushort)(r >> 32) };
    }

    /*
    [MethodImpl(256)]
    public static explicit operator long(UInt48 r) => ((long)r.v2u16 << 32) | r.v1u32;

    [MethodImpl(256)]
    public static explicit operator UInt48(long r) => new UInt48() { v2u16 = (ushort)(r >> 32), v1u32 = (uint)r };

    [MethodImpl(256)]
    public static explicit operator uint(UInt48 r) => r.v1u32;

    [MethodImpl(256)]
    public static explicit operator UInt48(uint r) => new UInt48() { v1u32 = r };

    [MethodImpl(256)]
    public static explicit operator int(UInt48 r) => (int)r.v1u32;

    [MethodImpl(256)]
    public static explicit operator UInt48(int r) => new UInt48() { v1u32 = (uint)r };

    [MethodImpl(256)]
    public static explicit operator ushort(UInt48 r) => r.v1u16;

    [MethodImpl(256)]
    public static explicit operator UInt48(ushort r) => new UInt48() { v1u16 = r };

    [MethodImpl(256)]
    public static explicit operator short(UInt48 r) => (short)r.v1u16;

    [MethodImpl(256)]
    public static explicit operator UInt48(short r) => new UInt48() { v1u16 = (ushort)r };

    [MethodImpl(256)]
    public static explicit operator byte(UInt48 r) => (byte)r.v1u16;

    [MethodImpl(256)]
    public static explicit operator UInt48(byte r) => new UInt48() { v1u16 = r };

    [MethodImpl(256)]
    public static explicit operator sbyte(UInt48 r) => (sbyte)r.v1u16;

    [MethodImpl(256)]
    public static explicit operator UInt48(sbyte r) => new UInt48() { v1u16 = (byte)r };
    */
    /*
    // should suffice all types additions
    [MethodImpl(256)]
    public static ulong operator +(long v1, UInt48 v2) => (ulong)(v1 + (long)v2);
    [MethodImpl(256)]
    public static ulong operator +(ulong v1, UInt48 v2) => (v1 + (ulong)v2);
    [MethodImpl(256)]
    public static ulong operator +(UInt48 v1, long v2) => (ulong)((long)v1 + v2);
    [MethodImpl(256)]
    public static ulong operator +(UInt48 v1, ulong v2) => ((ulong)v1 + v2);
    [MethodImpl(256)]
    public static ulong operator +(UInt48 v1, UInt48 v2) => ((ulong)v1 + v2);

    // should suffice all types subtractions
    [MethodImpl(256)]
    public static ulong operator -(long v1, UInt48 v2) => (ulong)(v1 - (long)v2);
    [MethodImpl(256)]
    public static ulong operator -(ulong v1, UInt48 v2) => (v1 - (ulong)v2);
    [MethodImpl(256)]
    public static ulong operator -(UInt48 v1, long v2) => (ulong)((long)v1 - v2);
    [MethodImpl(256)]
    public static ulong operator -(UInt48 v1, ulong v2) => ((ulong)v1 - v2);
    [MethodImpl(256)]
    public static ulong operator -(UInt48 v1, UInt48 v2) => ((ulong)v1 - v2);

    // should suffice all types multiplications
    [MethodImpl(256)]
    public static ulong operator *(long v1, UInt48 v2) => (ulong)(v1 * (long)v2);
    [MethodImpl(256)]
    public static ulong operator *(ulong v1, UInt48 v2) => (v1 * (ulong)v2);
    [MethodImpl(256)]
    public static ulong operator *(UInt48 v1, long v2) => (ulong)((long)v1 * v2);
    [MethodImpl(256)]
    public static ulong operator *(UInt48 v1, ulong v2) => ((ulong)v1 * v2);
    [MethodImpl(256)]
    public static ulong operator *(UInt48 v1, UInt48 v2) => ((ulong)v1 * v2);

    // should suffice all types divisions
    [MethodImpl(256)]
    public static ulong operator /(long v1, UInt48 v2) => (ulong)(v1 / (long)v2);
    [MethodImpl(256)]
    public static ulong operator /(ulong v1, UInt48 v2) => (v1 / (ulong)v2);
    [MethodImpl(256)]
    public static ulong operator /(UInt48 v1, long v2) => (ulong)((long)v1 / v2);
    [MethodImpl(256)]
    public static ulong operator /(UInt48 v1, ulong v2) => ((ulong)v1 / v2);
    [MethodImpl(256)]
    public static ulong operator /(UInt48 v1, UInt48 v2) => ((ulong)v1 / v2);
    */
    #endregion

    #region ### common members for type ###

    /// <summary>
    /// Returns true if the two values represent the same value.
    /// </summary>
    /// <param name="other">The value to compare to.</param>
    /// <returns>True if the two values represent the same value.</returns>
    [MethodImpl(256)]
    public bool Equals(UInt48 other) => v0u32 == other.v0u32 && v4u16 == other.v4u16;

    /// <summary>
    /// Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <returns>
    /// true if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false.
    /// </returns>
    /// <param name="obj">Another object to compare to. </param><filterpriority>2</filterpriority>
    [MethodImpl(256)]
    public override bool Equals(object obj) => (obj is UInt48) && Equals((UInt48)obj);

    /// <summary>
    /// Returns true if the two values represent the same value.
    /// </summary>
    /// <param name="value1">The first value to compare.</param>
    /// <param name="value2">The second value to compare.</param>
    /// <returns>True if the two values represent the same value.</returns>
    [MethodImpl(256)]
    public static bool operator ==(UInt48 value1, UInt48 value2) => value1.Equals(value2);

    /// <summary>
    /// Returns true if the two values represent different values.
    /// </summary>
    /// <param name="value1">The first value to compare.</param>
    /// <param name="value2">The second value to compare.</param>
    /// <returns>True if the two values represent different values.</returns>
    [MethodImpl(256)]
    public static bool operator !=(UInt48 value1, UInt48 value2) => !(value1 == value2);

    /// <summary>
    /// Returns the hash code for this instance.
    /// </summary>
    /// <returns>
    /// A 32-bit signed integer that is the hash code for this instance.
    /// </returns>
    [MethodImpl(256)]
    public override int GetHashCode() => ((ulong)this).GetHashCode();

    /// <summary>
    /// Returns a string representing the value using a default format and an invariant culture format provider.
    /// </summary>
    /// <returns>
    /// The string representation of the value of this instance, consisting of a sequence of digits ranging from 0 to 9, without a sign or leading zeros.
    /// </returns>
    [MethodImpl(256)]
    public override string ToString() => ((ulong)this).ToString(CultureInfo.InvariantCulture);

    /// <summary>
    /// Returns a string representing the value using the given string format and the given format provider.
    /// </summary>
    /// <param name="format">A numeric format string.</param>
    /// <param name="provider">An object that supplies culture-specific formatting information about this instance.</param>
    /// <returns>
    /// The string representation of the value of this instance as specified by format and provider.
    /// </returns>
    [MethodImpl(256)]
    public string ToString(string format, IFormatProvider provider) => ((ulong)this).ToString(format, provider);

    /// <summary>
    /// Tries to convert the string representation of a number to its 48-bit unsigned
    /// integer equivalent. A return value indicates whether the conversion succeeded
    /// or failed.
    /// </summary>
    /// <param name="s">
    /// A string that represents the number to convert.
    /// </param>
    /// <param name="value">
    /// When this method returns, contains the 48-bit unsigned integer value that is
    /// equivalent to the number contained in s, if the conversion succeeded, or zero
    /// if the conversion failed. The conversion fails if the s parameter is null or
    /// System.String.Empty, is not of the correct format, or represents a number less
    /// than System.UInt48.MinValue or greater than System.UInt48.MaxValue. This parameter
    /// is passed uninitialized; any value originally supplied in result will be overwritten.
    /// </param>
    /// <returns>
    /// Returns true if s was converted successfully; otherwise, false.
    /// </returns>
    [MethodImpl(256)]
    public static bool TryParse(string s, out UInt48 value)
    {
      if (!ulong.TryParse(s, out ulong u64))
      {
        value = UInt48MinValue;
        return false;
      }
      value = u64;
      return true;
    }
    #endregion

    #region ### IComparable ###

    /// <summary>
    /// Compares this instance with specified <see cref="UInt48"/> and returns indication of their relative values.
    /// </summary>
    /// <param name="obj">A instance of <see cref="UInt48"/> to be compared with.</param>
    /// <returns>
    /// A signed number indicating the relative values of this instance and value. Return
    /// Value Description Less than zero This instance is less than value. Zero This
    /// instance is equal to value. Greater than zero This instance is greater than value.
    /// -or- value is null.
    /// </returns>
    public int CompareTo(UInt48 obj)
    {
      // TODO: BE | LE support
      if (obj.v4u16 == v4u16)
        return v0u32.CompareTo(obj.v0u32);
      return v4u16.CompareTo(obj.v4u16);
    }

    /// <summary>
    /// Compares this instance with specified object and returns indication of their relative values.
    /// </summary>
    /// <param name="obj">A instance of object to be compared with.</param>
    /// <returns>
    /// A signed number indicating the relative values of this instance and value. Return
    /// Value Description Less than zero This instance is less than value. Zero This
    /// instance is equal to value. Greater than zero This instance is greater than value.
    /// -or- value is null.
    /// </returns>
    public int CompareTo(object obj)
    {
      return ((ulong)this).CompareTo(obj);
    }

    #endregion

    #region ### IConvertible ###

    /// <summary>
    /// Returns the System.TypeCode for this instance.
    /// </summary>
    /// <returns>The enumerated constant that is the System.TypeCode of the class or value type that implements this interface.</returns>
    TypeCode IConvertible.GetTypeCode()
    {
      // TODO: what struct should return when there is no size of backup fields ?
      //       and when this call is used and by whom ?
      return TypeCode.Object;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent Boolean value using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>A Boolean value equivalent to the value of this instance.</returns>
    bool IConvertible.ToBoolean(IFormatProvider provider)
    {
      return (UInt48MinValue != this);
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent Unicode character using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>A Unicode character equivalent to the value of this instance.</returns>
    char IConvertible.ToChar(IFormatProvider provider)
    {
      return (char)v0u32;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent 8-bit signed integer using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An 8-bit signed integer equivalent to the value of this instance.</returns>
    sbyte IConvertible.ToSByte(IFormatProvider provider)
    {
      return unchecked((sbyte)(ulong)this);
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent 8-bit unsigned integer using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An 8-bit unsigned integer equivalent to the value of this instance.</returns>
    byte IConvertible.ToByte(IFormatProvider provider)
    {
      return (byte)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent 16-bit signed integer using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An 16-bit signed integer equivalent to the value of this instance.</returns>
    short IConvertible.ToInt16(IFormatProvider provider)
    {
      return (short)(ulong)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent 16-bit unsigned integer using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An 16-bit unsigned integer equivalent to the value of this instance.</returns>
    ushort IConvertible.ToUInt16(IFormatProvider provider)
    {
      return (ushort)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent 32-bit signed integer using the specified culture-specific formatting information. 
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An 32-bit signed integer equivalent to the value of this instance.</returns>
    int IConvertible.ToInt32(IFormatProvider provider)
    {
      return (int)(ulong)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent 32-bit unsigned integer using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An 32-bit unsigned integer equivalent to the value of this instance.</returns>
    uint IConvertible.ToUInt32(IFormatProvider provider)
    {
      return (uint)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent 64-bit signed integer using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An 64-bit signed integer equivalent to the value of this instance.</returns>
    long IConvertible.ToInt64(IFormatProvider provider)
    {
      return (long)(ulong)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent 64-bit unsigned integer using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An 64-bit unsigned integer equivalent to the value of this instance.</returns>
    ulong IConvertible.ToUInt64(IFormatProvider provider)
    {
      return (ulong)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent single-precision floating-point number using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>A single-precision floating-point number equivalent to the value of this instance.</returns>
    float IConvertible.ToSingle(IFormatProvider provider)
    {
      return (uint)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent double-precision floating-point number using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>A double-precision floating-point number equivalent to the value of this instance.</returns>
    double IConvertible.ToDouble(IFormatProvider provider)
    {
      return (ulong)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent System.Decimal number using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>A System.Decimal number equivalent to the value of this instance.</returns>
    decimal IConvertible.ToDecimal(IFormatProvider provider)
    {
      return (ulong)this;
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent System.DateTime using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>A System.DateTime instance equivalent to the value of this instance.</returns>
    DateTime IConvertible.ToDateTime(IFormatProvider provider)
    {
      throw new NotSupportedException();
    }

    /// <summary>
    /// Converts the value of this instance to an equivalent System.String using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>A System.String instance equivalent to the value of this instance.</returns>
    string IConvertible.ToString(IFormatProvider provider)
    {
      return ToString("", provider);
    }

    /// <summary>
    /// Converts the value of this instance to an System.Object of the specified System.Type that has an equivalent value, using the specified culture-specific formatting information.
    /// </summary>
    /// <param name="conversionType">The System.Type to which the value of this instance is converted.</param>
    /// <param name="provider">An System.IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
    /// <returns>An System.Object instance of type conversionType whose value is equivalent to the value of this instance.</returns>
    object IConvertible.ToType(Type conversionType, IFormatProvider provider)
    {
      return ((IConvertible)((ulong)this)).ToType(conversionType, provider);
    }

    #endregion
  }
}
