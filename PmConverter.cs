﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using open.codecs.netpbm.types;
using open.codecs.netpbm.wic;


namespace open.codecs.netpbm
{

  /// <summary>
  /// Portable Map Converter class allows conversion from <see cref="PmImage"/> object to <see cref="BitmapSource"/> object.
  /// </summary>
  unsafe public partial class PmConverter
  {

    internal static class LUTS
    {
      internal static T_PIXEL_BGRA_4xU8[] LUT_YA_TO_BGRA_4xU8;

      static LUTS()
      {
        LUT_YA_TO_BGRA_4xU8 = new T_PIXEL_BGRA_4xU8[65536];
        for (int y = 0; y <= 255; y++)
        {
          for (int a = 0; a <= 255; a++)
          {
            LUT_YA_TO_BGRA_4xU8[(a << 8 | y)] = new T_PIXEL_BGRA_4xU8() { B = (byte)y, G = (byte)y, R = (byte)y, A = (byte)a };
          }
        }
      }
    }

    // NOTE: rules when coding the converter always first try to use native type pointers,
    //       if native type not possible then use user structure pointers.
    //       also avoid any conversions to or from float values 
    //       integer shift is factor of 8 times faster from conversion to or from float.
    //       not sure why is this for .NET, in C++ is much quicker (.NET has wrong implementation?)

    // NOTE: some conversions use mapping higher bits directly in structure which might not work for big endian platforms?
    //       new OSes: OsX == little endian, android == little endian, Windows == little endian;
    //       SPARC and some other, less used, platforms are big endian (less used for common users).
    //       We are safe to assume target is little endian.

    // NOTE: in effort to keep the speed HW endian versions should be written trough delegate
    //       and not through type conversion. So any pixel type should be both BE and LE, and 
    //       two functions should be written to covert both cases (BE and LE). And depending
    //       on HW endian one of those functions should be mapped to static delegate method
    //       that shall be called when such conversion is required/requested.

    private static bool s_IsLittleEndian = BitConverter.IsLittleEndian;

    private delegate bool ConvertDelegate(PmImage sourceImage, out PmConverterImageData targetImage);

#pragma warning disable IDE1006 // Naming Styles keep name without 's_'
#pragma warning disable IDE0004 // Cast is redundant

    private static readonly ConvertDelegate Convert_Y_1u32_to_Y_1u16 = s_IsLittleEndian
      ? (ConvertDelegate)Convert_Y_1u32_to_Y_1u16_le      // little endian version
      : (ConvertDelegate)Convert_Y_1u32_to_Y_1u16_be;     // big endian version

    private static readonly ConvertDelegate Convert_Y_1u48_to_Y_1u16 = s_IsLittleEndian
      ? (ConvertDelegate)Convert_Y_1u48_to_Y_1u16_le      // little endian version
      : (ConvertDelegate)Convert_Y_1u48_to_Y_1u16_be;     // big endian version

    private static readonly ConvertDelegate Convert_Y_1u64_to_Y_1u16 = s_IsLittleEndian
      ? (ConvertDelegate)Convert_Y_1u64_to_Y_1u16_le      // little endian version
      : (ConvertDelegate)Convert_Y_1u64_to_Y_1u16_be;     // big endian version

    private static readonly ConvertDelegate Convert_YA_2u16_to_BGRA_4u8 = s_IsLittleEndian
      ? (ConvertDelegate)Convert_YA_2u16_to_BGRA_4u8_le   // little endian version
      : (ConvertDelegate)Convert_YA_2u16_to_BGRA_4u8_be;  // big endian version

    private static readonly ConvertDelegate Convert_RGB_3u32_to_RGB_3u16 = s_IsLittleEndian
      ? (ConvertDelegate)Convert_RGB_3u32_to_RGB_3u16_le  // little endian version
      : (ConvertDelegate)Convert_RGB_3u32_to_RGB_3u16_be; // big endian version

    private static readonly ConvertDelegate Convert_RGB_3u64_to_RGB_3u16 = s_IsLittleEndian
      ? (ConvertDelegate)Convert_RGB_3u64_to_RGB_3u16_le  // little endian version
      : (ConvertDelegate)Convert_RGB_3u64_to_RGB_3u16_be; // big endian version

#pragma warning restore IDE1006 // Naming Styles
#pragma warning restore IDE0004 // Cast is redundant

    /// <summary>
    /// Creates the clone <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with specified pixel format 
    /// <paramref name="targetPixelFormat"/> from <see cref="PmImage"/> <paramref name="sourceImage"/>.
    /// This method is bi-endian (endian agnostic).
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted output image.</param>
    /// <param name="targetPixelFormat">A target pixel format to be set.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_CopyClone(PmImage sourceImage, out PmConverterImageData targetImage, PixelFormat targetPixelFormat)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        targetPixelFormat
        );
      // NOTE: NetPBM and WIC/BitmapSouce/Image are both top-down just memory copy 1:1
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var memorySize = sourceImage.ImageHeight * targetImage.Stride;
        Buffer.MemoryCopy((void*)(pbSource), (void*)(targetImage.Data), memorySize, memorySize);
      }
      return true;
    }

    #region ### Y_1xU64 to Y_1xU16 (LE+BE) ###

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.Y_64bits_1u64"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Gray16"/>,
    /// using little-endian (LE) memory layout.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted output image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_Y_1u64_to_Y_1u16_le(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Gray16
        );
      fixed (byte* pSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_Y_1xU64_h16_le*)pSource;
        var pTargetStride = (ushort*)targetImage.Data;
        if (sourceImage.ImageMaxVal == ulong.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = (ushort)(multiplier * pSourceStride[x].Y);
          }
          pSourceStride = (T_PIXEL_Y_1xU64_h16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = pSourceStride[x].Yh16;
          }
          pSourceStride = (T_PIXEL_Y_1xU64_h16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.Y_64bits_1u64"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Gray16"/>,
    /// using big-endian (BE) memory layout.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted output image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_Y_1u64_to_Y_1u16_be(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      // create output target image
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Gray16
        );
      fixed (byte* pSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_Y_1xU64_h16_be*)pSource;
        var pTargetStride = (ushort*)targetImage.Data;
        if (sourceImage.ImageMaxVal == ulong.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = (ushort)(multiplier * pSourceStride[x].Y);
          }
          pSourceStride = (T_PIXEL_Y_1xU64_h16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = pSourceStride[x].Yh16;
          }
          pSourceStride = (T_PIXEL_Y_1xU64_h16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }

    #endregion

    #region ### Y_1xU48 to Y_1xU16 (LE+BE) ###

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.Y_48bits_1u48"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Gray16"/>,
    /// using little-endian (LE) memory layout.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_Y_1u48_to_Y_1u16_le(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Gray16
        );
      fixed (byte* pSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_Y_1xU48_h16_le*)pSource;
        var pTargetStride = (ushort*)targetImage.Data;
        if (sourceImage.ImageMaxVal == UInt48.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = (ushort)(multiplier * (((long)pSourceStride[x].Yh16 << 32) | pSourceStride[x].Yl32));
          }
          pSourceStride = (T_PIXEL_Y_1xU48_h16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = pSourceStride[x].Yh16;
          }
          pSourceStride = (T_PIXEL_Y_1xU48_h16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }
    
    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.Y_48bits_1u48"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Gray16"/>, 
    /// using big-endian (BE) memory layout.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_Y_1u48_to_Y_1u16_be(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Gray16
        );
      fixed (byte* pSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_Y_1xU48_h16_be*)pSource;
        var pTargetStride = (ushort*)targetImage.Data;
        if (sourceImage.ImageMaxVal == UInt48.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {            
            pTargetStride[x] = (ushort)(multiplier * (((long)pSourceStride[x].Yh16 << 32) | pSourceStride[x].Yl32));
          }
          pSourceStride = (T_PIXEL_Y_1xU48_h16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = pSourceStride[x].Yh16;
          }
          pSourceStride = (T_PIXEL_Y_1xU48_h16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }

    #endregion

    #region ### Y_1xU32 to Y_1xU16 (LE+BE) ###

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.Y_32bits_1u32"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Gray16"/>, 
    /// using little-endian (LE) memory layout.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_Y_1u32_to_Y_1u16_le(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Gray16
        );
      
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_Y_1xU32_h16_le*)pbSource;
        var pTargetStride = (ushort*)targetImage.Data;
        if (sourceImage.ImageMaxVal == uint.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = (ushort)(multiplier * pSourceStride[x].Y);
          }
          pSourceStride = (T_PIXEL_Y_1xU32_h16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) - iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = pSourceStride[x].Yh16;
          }
          pSourceStride = (T_PIXEL_Y_1xU32_h16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.Y_32bits_1u32"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Gray16"/>, 
    /// using big-endian (BE) memory layout.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_Y_1u32_to_Y_1u16_be(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Gray16
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_Y_1xU32_h16_be*)pbSource;
        var pTargetStride = (ushort*)targetImage.Data;
        if (sourceImage.ImageMaxVal == uint.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = (ushort)(multiplier * pSourceStride[x].Y);
          }
          pSourceStride = (T_PIXEL_Y_1xU32_h16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) - iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = pSourceStride[x].Yh16;
          }
          pSourceStride = (T_PIXEL_Y_1xU32_h16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (ushort*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }

    #endregion
    
    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.YA_64bits_2f32"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Rgba64"/>.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_YA_2f32_to_RGBA_4u16(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Rgba64
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = ushort.MaxValue / (sourceImage.ImageMaxValFloat == 0 ? 1f : sourceImage.ImageMaxValFloat);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_YA_2xF32*)pbSource;
        var pTargetStride = (T_PIXEL_RGBA_4xU16*)targetImage.Data;
        if (sourceImage.ImageMaxValFloat == 1f)
        {
          goto ConvertMaxValue;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = pTargetStride[x].G = pTargetStride[x].B = (ushort)(multiplier * pSourceStride[x].Y);
            pTargetStride[x].A = (ushort)(multiplier * pSourceStride[x].A);
          }
          pSourceStride = (T_PIXEL_YA_2xF32*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGBA_4xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
ConvertMaxValue:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = pTargetStride[x].G = pTargetStride[x].B = (ushort)(pSourceStride[x].Y * 255);
            pTargetStride[x].A = (ushort)(pSourceStride[x].A * 255);
          }
          pSourceStride = (T_PIXEL_YA_2xF32*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGBA_4xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
      }
    }

    #region ### RGB_3xU32 to RGB_3xU16 (LE+BE) ### 
    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.RGB_96bits_3u32"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Rgb48"/> in little-endian.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_RGB_3u32_to_RGB_3u16_le(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Rgb48
        );
      // stride has conversion so we must convert each pixel value,

      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        // TODO : optimize for 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, ... multiplier if necessary
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_RGB_3xU32_h16_le*)pbSource;
        var pTargetStride = (T_PIXEL_RGB_3xU16*)targetImage.Data;
        if (sourceImage.ImageMaxVal == uint.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = (ushort)(multiplier * pSourceStride[x].R);
            pTargetStride[x].G = (ushort)(multiplier * pSourceStride[x].G);
            pTargetStride[x].B = (ushort)(multiplier * pSourceStride[x].B);
            // A:N/A
          }
          pSourceStride = (T_PIXEL_RGB_3xU32_h16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = pSourceStride[x].Rh16;
            pTargetStride[x].G = pSourceStride[x].Gh16;
            pTargetStride[x].B = pSourceStride[x].Bh16;
            // A:N/A
          }
          pSourceStride = (T_PIXEL_RGB_3xU32_h16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.RGB_96bits_3u32"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Rgb48"/> in big-endian.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_RGB_3u32_to_RGB_3u16_be(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Rgb48
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_RGB_3xU32_h16_be*)pbSource;
        var pTargetStride = (T_PIXEL_RGB_3xU16*)targetImage.Data;
        if (sourceImage.ImageMaxVal == uint.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = (ushort)(multiplier * pSourceStride[x].R);
            pTargetStride[x].G = (ushort)(multiplier * pSourceStride[x].G);
            pTargetStride[x].B = (ushort)(multiplier * pSourceStride[x].B);
            // A:N/A
          }
          pSourceStride = (T_PIXEL_RGB_3xU32_h16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = pSourceStride[x].Rh16;
            pTargetStride[x].G = pSourceStride[x].Gh16;
            pTargetStride[x].B = pSourceStride[x].Bh16;
            // A:N/A
          }
          pSourceStride = (T_PIXEL_RGB_3xU32_h16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }
    #endregion

    #region ### RGB_3xU64 to RGB_3xU16 (LE+BE) ###

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.RGB_192bits_3u64"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Rgb48"/> in little-endian.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_RGB_3u64_to_RGB_3u16_le(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Rgb48
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_RGB_3xU64_ou16_le*)pbSource;
        var pTargetStride = (T_PIXEL_RGB_3xU16*)targetImage.Data;
        if (sourceImage.ImageMaxVal == ulong.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = (ushort)(multiplier * pSourceStride[x].R);
            pTargetStride[x].G = (ushort)(multiplier * pSourceStride[x].G);
            pTargetStride[x].B = (ushort)(multiplier * pSourceStride[x].B);
            // A:N/A
          }
          pSourceStride = (T_PIXEL_RGB_3xU64_ou16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = pSourceStride[x].Rh16;
            pTargetStride[x].G = pSourceStride[x].Gh16;
            pTargetStride[x].B = pSourceStride[x].Bh16;
            // A:N/A
          }
          pSourceStride = (T_PIXEL_RGB_3xU64_ou16_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
      }
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.RGB_192bits_3u64"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Rgb48"/> in big-endian.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_RGB_3u64_to_RGB_3u16_be(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Rgb48
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 16);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_RGB_3xU64_ou16_be*)pbSource;
        var pTargetStride = (T_PIXEL_RGB_3xU16*)targetImage.Data;
        if (sourceImage.ImageMaxVal == ulong.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = (ushort)(multiplier * pSourceStride[x].R);
            pTargetStride[x].G = (ushort)(multiplier * pSourceStride[x].G);
            pTargetStride[x].B = (ushort)(multiplier * pSourceStride[x].B);
            // A:N/A
          }
          pSourceStride = (T_PIXEL_RGB_3xU64_ou16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = pSourceStride[x].Rh16;
            pTargetStride[x].G = pSourceStride[x].Gh16;
            pTargetStride[x].B = pSourceStride[x].Bh16;
            // A:N/A
          }
          pSourceStride = (T_PIXEL_RGB_3xU64_ou16_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU16*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
      }
    }

    #endregion

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.WA_16bits_2u8"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Bgra32"/>.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_WA_2u8_to_BGRA_4u8(PmImage sourceImage, out PmConverterImageData targetImage)
    {      
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Bgra32
        );
      // WA = {0..1,0..1}
      // YA = {0..255,0..255}
      // YA = {0..MaxVal,0..MaxVal}
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 8);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_YA_2xU8*)pbSource;
        var pTargetStride = (T_PIXEL_BGRA_4xU8*)targetImage.Data;
        if (sourceImage.ImageMaxVal == 1)
        {
          goto ConvertMaxValue1;
        }
        else if (sourceImage.ImageMaxVal == byte.MaxValue)
        {
          goto ConvertMaxValue255;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B =
            pTargetStride[x].G =
            pTargetStride[x].R = (byte)(multiplier * pSourceStride[x].Y);
            pTargetStride[x].A = (byte)(multiplier * pSourceStride[x].A);
          }
          pSourceStride = (T_PIXEL_YA_2xU8*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
ConvertMaxValue1:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B =
            pTargetStride[x].G =
            pTargetStride[x].R = (byte)(pSourceStride[x].Y << 7); // (0 || 1) * 255
            pTargetStride[x].A = (byte)(pSourceStride[x].A << 7); // (0 || 1) * 255
          }
          pSourceStride = (T_PIXEL_YA_2xU8*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
ConvertMaxValue255:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {            
            pTargetStride[x].B =
            pTargetStride[x].G =
            pTargetStride[x].R = pSourceStride[x].Y;
            pTargetStride[x].A = pSourceStride[x].A;
          }
          pSourceStride = (T_PIXEL_YA_2xU8*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
      }
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.YA_16bits_2u8"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Bgra32"/>.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_YA_2u8_to_BGRA_4u8(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Bgra32
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 8);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_YA_2xU8*)pbSource;
        var pTargetStride = (T_PIXEL_BGRA_4xU8*)targetImage.Data;
        if (sourceImage.ImageMaxVal == byte.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B =
            pTargetStride[x].G =
            pTargetStride[x].R = (byte)(multiplier * pSourceStride[x].Y);
            pTargetStride[x].A = (byte)(multiplier * pSourceStride[x].A);
          }
          pSourceStride = (T_PIXEL_YA_2xU8*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x] = LUTS.LUT_YA_TO_BGRA_4xU8[pSourceStride[x].A << 8 | pSourceStride[x].Y];
          }
          pSourceStride = (T_PIXEL_YA_2xU8*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }        
        return true;
      }
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.YA_32bits_2u16"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Bgra32"/>.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_YA_2u16_to_BGRA_4u8_le(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Bgra32
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 8);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_YA_2xU16_ou8_le*)pbSource;
        var pTargetStride = (T_PIXEL_BGRA_4xU8*)targetImage.Data;
        if (sourceImage.ImageMaxVal == ushort.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B =
            pTargetStride[x].G =
            pTargetStride[x].R = (byte)(multiplier * pSourceStride[x].Y);
            pTargetStride[x].A = (byte)(multiplier * pSourceStride[x].A);
          }
          pSourceStride = (T_PIXEL_YA_2xU16_ou8_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B =
            pTargetStride[x].G =
            pTargetStride[x].R = (byte)(pSourceStride[x].Yh8);
            pTargetStride[x].A = (byte)(pSourceStride[x].Ah8);
          }
          pSourceStride = (T_PIXEL_YA_2xU16_ou8_le*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.YA_32bits_2u16"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Bgra32"/>.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_YA_2u16_to_BGRA_4u8_be(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Bgra32
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 8);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_YA_2xU16_ou8_be*)pbSource;
        var pTargetStride = (T_PIXEL_BGRA_4xU8*)targetImage.Data;
        if (sourceImage.ImageMaxVal == ushort.MaxValue)
        {
          goto MaxValueConverter;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B =
            pTargetStride[x].G =
            pTargetStride[x].R = (byte)(multiplier * pSourceStride[x].Y);
            pTargetStride[x].A = (byte)(multiplier * pSourceStride[x].A);
          }
          pSourceStride = (T_PIXEL_YA_2xU16_ou8_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
MaxValueConverter:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B =
            pTargetStride[x].G =
            pTargetStride[x].R = (byte)(pSourceStride[x].Yh8);
            pTargetStride[x].A = (byte)(pSourceStride[x].Ah8);
          }
          pSourceStride = (T_PIXEL_YA_2xU16_ou8_be*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
      }
      return true;
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.RGBA_32bits_4u8"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Bgra32"/>.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>    
    private static bool Convert_RGBA_4xU8_to_BGRA_4xU8(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Bgra32
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 8);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_RGBA_4xU8*)pbSource;
        var pTargetStride = (T_PIXEL_BGRA_4xU8*)targetImage.Data;
        if (sourceImage.ImageMaxVal == 255)
        {
          goto ConvertMaxValue;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B = (byte)(multiplier * pSourceStride[x].B);
            pTargetStride[x].G = (byte)(multiplier * pSourceStride[x].G);
            pTargetStride[x].R = (byte)(multiplier * pSourceStride[x].R);
            pTargetStride[x].A = (byte)(multiplier * pSourceStride[x].A);
          }
          pSourceStride = (T_PIXEL_RGBA_4xU8*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
ConvertMaxValue:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].B = pSourceStride[x].B;
            pTargetStride[x].G = pSourceStride[x].G;
            pTargetStride[x].R = pSourceStride[x].R;
            pTargetStride[x].A = pSourceStride[x].A;
          }
          pSourceStride = (T_PIXEL_RGBA_4xU8*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_BGRA_4xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
      }
    }

    /// <summary>
    /// Converts from <see cref="PmImage"/> <paramref name="sourceImage"/> with pixel format <see cref="EPmPixelFormat.RGB_96bits_3f32"/>
    /// to <see cref="PmConverterImageData"/> <paramref name="targetImage"/> with pixel format <see cref="PixelFormats.Rgb24"/>.
    /// </summary>
    /// <param name="sourceImage">A source image to be converted.</param>
    /// <param name="targetImage">A converted target image.</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    private static bool Convert_RGB_3xF32_to_BGR_3xU8(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      targetImage = new PmConverterImageData(
        sourceImage.ImageWidth,
        sourceImage.ImageHeight,
        PixelFormats.Rgb24
        );
      fixed (byte* pbSource = sourceImage.ImagePixelData)
      {
        var multiplier = sourceImage.GetMaxValMultiplier(targetChannelBits: 8);
        var iSourceStride = sourceImage.Stride;
        var iTargetStride = targetImage.Stride;
        var pSourceStride = (T_PIXEL_RGB_3xF32*)pbSource;
        var pTargetStride = (T_PIXEL_RGB_3xU8*)targetImage.Data;
        if (sourceImage.ImageMaxValFloat == 1f)
        {
          goto ConvertMaxValue;
        }
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = (byte)((uint)(multiplier * pSourceStride[x].R));
            pTargetStride[x].G = (byte)((uint)(multiplier * pSourceStride[x].G));
            pTargetStride[x].B = (byte)((uint)(multiplier * pSourceStride[x].B));
            //pTargetStride[x].A = pSourceStride[x].A;
          }
          pSourceStride = (T_PIXEL_RGB_3xF32*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
ConvertMaxValue:
        for (int y = 0; y < sourceImage.ImageHeight; y++)
        {
          for (int x = 0; x < targetImage.ImageWidth; x++)
          {
            pTargetStride[x].R = (byte)((uint)(pSourceStride[x].R * 255));
            pTargetStride[x].G = (byte)((uint)(pSourceStride[x].G * 255));
            pTargetStride[x].B = (byte)((uint)(pSourceStride[x].B * 255));
            //pTargetStride[x].A = pSourceStride[x].A;
          }
          pSourceStride = (T_PIXEL_RGB_3xF32*)(((byte*)pSourceStride) + iSourceStride);
          pTargetStride = (T_PIXEL_RGB_3xU8*)(((byte*)pTargetStride) + iTargetStride);
        }
        return true;
      }
    }

    static string Dump(byte b)
    {
      char[] s = new char[8];
      for(int i = 0; i < 8; i++)
      {
        s[7 - i] = (0 == (b & (1 << i))) ? '0' : '1';
      }
      string sz = new string(s);
      return sz;
    }

    static string Dump(byte b, int count)
    {
      char[] s = new char[count];
      for (int i = 0; i < count; i++)
      {
        s[(count - 1) - i] = (0 == (b & (1 << i))) ? '0' : '1';
      }
      string sz = new string(s);
      return sz;
    }

    // NOTE: stripped down version of JPEG-LS bit reader

    /// <summary>
    /// <para>Reads the maximum number of bits into <see cref="_tmpBits"/> and advances the buffer for specified number of bits.</para>
    /// <para>■ Note: all reads from Data buffer are byte aligned to the highest byte bit, padding the lowest bits with zero (0).</para>
    /// </summary>
    private class BitsReaderByteAlignHBB
    {
      /// <summary>
      /// Reference to readonly byte array to read from
      /// </summary>
      public readonly byte[] Data;
      /// <summary>
      /// Current byte read position in the <see cref="Data"/> member.
      /// </summary>
      public int Position;
      /// <summary>
      /// temporary read variable size in bits
      /// </summary>
      const int VARSIZE = 64;
      /// <summary>
      /// temporary read variable
      /// </summary>
      ulong _tmpBits;
      /// <summary>
      /// temporary read variable free bits
      /// </summary>
      int _tmpBitsFree = VARSIZE;

      /// <summary>
      /// Creates new instance of <see cref="BitsReaderByteAlignHBB"/> class with specified <paramref name="data"/> byte array.
      /// </summary>
      /// <param name="data"></param>
      [DebuggerHidden]
      [MethodImpl(256)]
      public BitsReaderByteAlignHBB(byte[] data)
      {
        Data = data;
      }

      /// <summary>
      /// Returns byte aligned to highest byte bit with specified number of bits, where padding the lowest bits with zero (0).
      /// </summary>
      /// <param name="bitCount">The number of bits to read (max 8).</param>
      /// <returns>Returns a byte aligned to highest byte bit value.</returns>
      [DebuggerHidden]
      [MethodImpl(256)]
      public byte ReadBits(int bitCount)
      {
        // read bits if we do not have enough already read.
        while (bitCount > VARSIZE - _tmpBitsFree)
        {
          // NOTE: 'readByte' must be ulong (shift)
          ulong readByte; 
readByte:
          readByte = Data[Position++];
          _tmpBitsFree -= 8;
          _tmpBits |= (readByte << _tmpBitsFree);
          if (_tmpBitsFree >= 8 && Position < Data.Length - 1)
            goto readByte;
        }
        // Shift left the requested number of bits and set output value
        uint output = (uint)(_tmpBits >> (VARSIZE - bitCount));
        _tmpBitsFree += bitCount;
        _tmpBits <<= bitCount;
        // bit align to highest bit and pad with zero (0) bits (shift left inserts zeros).
        return (byte)((output << (8 - bitCount)) ^ 0xFF); // reverse all bits
      }
    }

    private static bool Convert_W_1xU1_to_W_1xU1_wpad(PmImage sourceImage, out PmConverterImageData targetImage)
    {

       return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.BlackWhite);      
    }

    /// <summary>
    /// Converts <see cref="PmImage"/> <paramref name="sourceImage"/> to <see cref="PmConverterImageData"/> <paramref name="targetImage"/>.
    /// </summary>
    /// <param name="sourceImage">A PBM image source to be converted from.</param>
    /// <param name="targetImage">A converted image data (WIC ready).</param>
    /// <returns>Returns true if conversion was successful; Otherwise, returns false.</returns>
    public static bool ConvertSource(PmImage sourceImage, out PmConverterImageData targetImage)
    {
      //EPbmPixelFormat pf = sourceImage.GetPixelFormat();
      switch (sourceImage.PixelFormat)
      {

        // BLACK AND WHITE UINT formats

        case EPmPixelFormat.W_1bit_1u1:
          // bit [0,1] 0-white, 1-black (oddly do we must XOR bits?)
          // native support
          //return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.BlackWhite);
          return Convert_W_1xU1_to_W_1xU1_wpad(sourceImage, out targetImage);

        // GRAYSCALE UINT formats

        case EPmPixelFormat.Y_8bits_1u8:
          // native support
          return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.Gray8);

        case EPmPixelFormat.Y_16bits_1u16:
          // native support
          return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.Gray16);

        case EPmPixelFormat.Y_32bits_1u32:
          // No native support, we must convert it.
          return Convert_Y_1u32_to_Y_1u16(sourceImage, out targetImage);

        case EPmPixelFormat.Y_48bits_1u48:
          // No native support, we must convert it.          
          return Convert_Y_1u48_to_Y_1u16(sourceImage, out targetImage);

        case EPmPixelFormat.Y_64bits_1u64:
          // No native support, we must convert it.
          return Convert_Y_1u64_to_Y_1u16(sourceImage, out targetImage);

        // RGB UINT formats

        case EPmPixelFormat.RGB_24bits_3u8:
          // native support
          return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.Rgb24);

        case EPmPixelFormat.RGB_48bits_3u16:
          // native support
          return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.Rgb48);

        case EPmPixelFormat.RGB_96bits_3u32:
          // No native support, we must convert it.
          return Convert_RGB_3u32_to_RGB_3u16(sourceImage, out targetImage);

        case EPmPixelFormat.RGB_192bits_3u64:
          // No native support, we must convert it.
          return Convert_RGB_3u64_to_RGB_3u16(sourceImage, out targetImage);

        // BLACK AND WHITE WITH ALPHA UINT formats

        case EPmPixelFormat.WA_16bits_2u8:
          // 2 bytes with [0..1], [0..1] TODO indexed palette ?
          // No native support, we must convert it.
          return Convert_WA_2u8_to_BGRA_4u8(sourceImage, out targetImage);

        // GRAYSCALE WITH ALPHA UINT formats 

        case EPmPixelFormat.YA_16bits_2u8:
          // No native support, we must convert it.
          return Convert_YA_2u8_to_BGRA_4u8(sourceImage, out targetImage);

        case EPmPixelFormat.YA_32bits_2u16:
          // No native support, we must convert it.
          return Convert_YA_2u16_to_BGRA_4u8(sourceImage, out targetImage);

        // RGBA WITH ALPHA UINT FORMATS

        case EPmPixelFormat.RGBA_32bits_4u8:
          // native support
          return Convert_CopyClone(sourceImage, out targetImage, WicPixelFormats.PixelFormat32bppRGBA);

        case EPmPixelFormat.RGBA_64bits_4u16:
          // native support
          return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.Rgba64);

        // Non-Standard (FLOAT extension) 

        case EPmPixelFormat.Y_32bits_1f32:
          // native support
          return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.Gray32Float);

        case EPmPixelFormat.YA_64bits_2f32:
          // No native support, we must convert it.
          return Convert_YA_2f32_to_RGBA_4u16(sourceImage, out targetImage);

        case EPmPixelFormat.RGB_96bits_3f32:
          // native support
          return Convert_CopyClone(sourceImage, out targetImage, WicPixelFormats.PixelFormat96bppRGBFloat);

        case EPmPixelFormat.RGBA_128bits_4f32:
          // native support
          // return Convert_CopyClone(sourceImage, out targetImage, PixelFormats.Rgba128Float);
          return Convert_CopyClone(sourceImage, out targetImage, WicPixelFormats.PixelFormat128bppRGBAFloat);
        
        // unsupported

        case EPmPixelFormat.Unknown:
        default:
          // not supported atm
          break;
      }
      targetImage = null;
      return false;
    }


    /// <summary>
    /// Creates new instance of <see cref="BitmapSource"/> object from specified <see cref="PmImage"/> <paramref name="sourceImage"/> object.
    /// </summary>
    /// <param name="sourceImage">A <see cref="PmImage"/> source image object/</param>
    /// <param name="dpiX">The horizontal dots per inch (DPI) of the bitmap.</param>
    /// <param name="dpiY">The vertical dots per inch (DPI) of the bitmap.</param>
    /// <returns>Returns new instance of <see cref="BitmapSource"/> object.</returns>
    public static BitmapSource CreateBitmapSource(PmImage sourceImage, double dpiX = 96d, double dpiY = 96d)
    {
      // NOTE: there was small issue not all pixel values are mappable to WIC pixel native
      //       formats, added conversion PM -> WIC for unsupported pixel native formats.
      //       For full list of WIC support pixel formats see: wic.WicPixelFormats.
      //
      if (!ConvertSource(sourceImage, out PmConverterImageData targetImage))
      {
        throw (new NotSupportedException("Conversion not supported!"));
      }

      BitmapSource bitmapSource = BitmapSource.Create(
        targetImage.ImageWidth,
        targetImage.ImageHeight,
        dpiX,
        dpiY,
        targetImage.PixelFormat,
        null,
        targetImage.Data,
        targetImage.DataSize, // NOTE: Create() does not validate this member
        targetImage.Stride);

      return bitmapSource;
    }
  }
}
